using Framework;

namespace GameShared.ModelLoader
{
	public class EmptyModelLoader<TModelRoot> : IModelLoader<TModelRoot> where TModelRoot : new()
	{
		public void Load(IContainer container, TModelRoot rootModel)
		{
		}

		public TModelRoot Save(IContainer container)
		{
			return new TModelRoot();
		}
	}
}