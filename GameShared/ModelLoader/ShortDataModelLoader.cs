using System;
using Framework;
using Framework.Collections;
using Framework.Model.Localization;
using Framework.Reactive;
using SAS.Model;
using SAS.Model.History;
using SAS.Model.Time;
using SAS.Model.User;

namespace GameShared.ModelLoader
{
	[Bind]
	public class ShortDataModelLoader : IModelLoader<ShortDataRoot>
	{
		public void Load(IContainer container, ShortDataRoot rootModel)
		{
			UserCharacters userCharacters = container.Resolve<UserCharacters>();
			UserMonsters userMonsters = container.Resolve<UserMonsters>();
			LoadCharacters(userCharacters, rootModel);
			LoadMonsters(userMonsters, rootModel);
			LoadChest(container, rootModel);
			LoadHistory(container, rootModel);
			LoadSettings(container, rootModel);
		}

		private void LoadHistory(IContainer container, ShortDataRoot rootModel)
		{
			UserHistory userHistory = container.Resolve<UserHistory>();
			userHistory.Deserialize(rootModel.History);
		}

		private void LoadSettings(IContainer container, ShortDataRoot rootModel)
		{
			UserSettings userSettings = container.Resolve<UserSettings>();
			Language languageToLoad = rootModel.Language == Language.None ? Language.English : rootModel.Language;
			userSettings.Language.Value = languageToLoad;
			userSettings.SoundsVolume.Value = rootModel.SoundsVolume;
			userSettings.SkipBattleAnimation.Value = rootModel.SkipBattleAnimation;
		}

		private void LoadMonsters(UserMonsters userMonsters, ShortDataRoot rootModel)
		{
			if (userMonsters.Monsters == null)
			{
				userMonsters.Monsters = new ListField<Monster>();
			}

			userMonsters.Monsters.Clear();
			foreach (MonsterDto monsterDto in rootModel.Monsters)
			{
				Monster monster = new Monster(monsterDto.StaticId, monsterDto.Level);
				userMonsters.Monsters.Add(monster);
			}
		}

		private void LoadChest(IContainer container, ShortDataRoot rootModel)
		{
			UserChest userChest = container.Resolve<UserChest>();
			userChest.Deserialize(rootModel.Chest);
		}

		private void LoadCharacters(UserCharacters userCharacters, ShortDataRoot rootModel)
		{
			if (userCharacters.Characters == null)
			{
				userCharacters.Characters = new ListField<Character>();
			}
			foreach (CharacterDto dto in rootModel.Characters)
			{
				Character character = userCharacters.TryGet(dto.Id);
				if (character != null)
				{
					character.CurrentExperience.Value = dto.Experience;
					character.Level.Value = dto.Level;
					character.AvailableFightsCount.Value = dto.AvailableFightsCount;
					character.NextFightSeries.Value = TimePoint.CreateBySeconds(dto.NextFightSeriesSeconds);
					character.Availability.Value = dto.Availability;
				}
				else
				{
					character = new Character(dto.StaticId, dto.Id, new Guid(dto.Guid), dto.Experience, dto.Level,
							dto.Rarity, dto.Element, dto.AvailableFightsCount,
							TimePoint.CreateBySeconds(dto.NextFightSeriesSeconds), dto.Availability);
					userCharacters.Characters.Add(character);
				}
			}
		}

		public ShortDataRoot Save(IContainer container)
		{
			ShortDataRoot result = new ShortDataRoot();
			SaveCharacters(container, ref result);
			SaveMonsters(container, ref result);
			SaveChest(container, ref result);
			SaveHistory(container, ref result);
			SaveSettings(container, ref result);
			return result;
		}

		private void SaveMonsters(IContainer container, ref ShortDataRoot result)
		{
			UserMonsters userMonsters = container.Resolve<UserMonsters>();
			if (userMonsters.Monsters == null || userMonsters.Monsters.Count == 0)
			{
				return;
			}

			MonsterDto[] monsterDtos = new MonsterDto[userMonsters.Monsters.Count];
			for (int i = 0; i < monsterDtos.Length; i++)
			{
				monsterDtos[i] = userMonsters.Monsters[i].Serialize();
			}

			result.Monsters = new ConstArray<MonsterDto>(monsterDtos);
		}

		private void SaveCharacters(IContainer container, ref ShortDataRoot rootModel)
		{
			UserCharacters userCharacters = container.Resolve<UserCharacters>();
			if (userCharacters.Characters == null || userCharacters.Characters.Count == 0)
			{
				return;
			}
			CharacterDto[] characterDtos = new CharacterDto[userCharacters.Characters.Count];
			int next = 0;
			foreach (Character character in userCharacters.Characters)
			{
				CharacterDto dto = character.Serialize();
				characterDtos[next++] = dto;
			}
			rootModel.Characters = new ConstArray<CharacterDto>(characterDtos);
		}

		private void SaveChest(IContainer container, ref ShortDataRoot rootModel)
		{
			UserChest userChest = container.Resolve<UserChest>();
			rootModel.Chest = userChest.Serialize();
		}

		private void SaveHistory(IContainer container, ref ShortDataRoot rootModel)
		{
			UserHistory userHistory = container.Resolve<UserHistory>();
			rootModel.History = userHistory.Serialize();
		}

		private void SaveSettings(IContainer container, ref ShortDataRoot rootModel)
		{
			UserSettings userSettings = container.Resolve<UserSettings>();
			rootModel.Language = userSettings.Language.Value;
			rootModel.SoundsVolume = userSettings.SoundsVolume.Value;
			rootModel.SkipBattleAnimation = userSettings.SkipBattleAnimation.Value;
		}
	}
}