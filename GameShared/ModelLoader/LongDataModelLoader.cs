using Framework;
using Framework.Collections;
using SAS.Model;
using SAS.Model.StarterPack;

namespace GameShared.ModelLoader
{
	[Bind]
	public class LongDataModelLoader : IModelLoader<LongDataRoot>
	{
		public void Load(IContainer container, LongDataRoot rootModel)
		{
			LoadItems(container, rootModel);
		}

		private void LoadItems(IContainer container, LongDataRoot root)
		{
			if (root.Items.Count > 0)
			{
				UserItems userItems = container.Resolve<UserItems>();
				foreach (ItemPair itemPair in root.Items)
				{
					userItems.GetItem((ItemType)itemPair.Id).Count.Value = itemPair.Count;
				}
			}
			container.Resolve<UserStarterPack>().CountPurchased.Value = root.StarterPacksPurchased;
		}

		public LongDataRoot Save(IContainer container)
		{
			LongDataRoot longDataRoot = new LongDataRoot();
			SaveItems(container, ref longDataRoot);
			return longDataRoot;
		}

		private void SaveItems(IContainer container, ref LongDataRoot longDataRoot)
		{
			UserItems userItems = container.Resolve<UserItems>();

			ItemPair[] items = new ItemPair[userItems.ItemsCount];
			for (int i = 0; i < items.Length; i++)
			{
				Item item = userItems.GetItem(i + 1);
				items[i] = new ItemPair()
				{
						Id = item.Static.Id,
						Count = item.Count.Value
				};
			}

			longDataRoot.Items = new ConstArray<ItemPair>(items);
			longDataRoot.StarterPacksPurchased = container.Resolve<UserStarterPack>().CountPurchased;
		}
	}
}