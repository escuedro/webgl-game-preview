#if NOT_UNITY3D
using Framework;
using Framework.Model;
using Framework.Model.IdentityGenerator;
using SAS.Model;
using SAS.Model.User;

namespace GameShared.ModelLoader
{
	[Bind]
	public class UserInfoModelLoader : IModelLoader<UserInfoRoot>
	{
		public void Load(IContainer container, UserInfoRoot rootModel)
		{
			TestModel testModel = container.Resolve<TestModel>();
			testModel.Desctiption = rootModel.Description;
			testModel.TestModelVariable = rootModel.TestNumber;
			LoadIdentities(container, rootModel);
		}

		private void LoadIdentities(IContainer container, UserInfoRoot root)
		{
			if (root.Identities.Count > 0)
			{
				LocalIdentityGenerator localIdentityGenerator = container.Resolve<LocalIdentityGenerator>();
				foreach (LocalIdentityRange identityRange in root.Identities)
				{
					localIdentityGenerator.Add(identityRange);
				}
			}
		}

		public UserInfoRoot Save(IContainer container)
		{
			TestModel testModel = container.Resolve<TestModel>();
			UserSettings userSettings = container.Resolve<UserSettings>();
			UserInfoRoot userInfoRoot = new UserInfoRoot
			{
					Description = testModel.Desctiption, TestNumber = testModel.TestModelVariable
			};
			SaveIdentities(container, ref userInfoRoot);
			return userInfoRoot;
		}

		private void SaveIdentities(IContainer container, ref UserInfoRoot root)
		{
			LocalIdentityGenerator localIdentityGenerator = container.Resolve<LocalIdentityGenerator>();
			root.Identities = localIdentityGenerator.Serialize();
		}
	}
}
#endif