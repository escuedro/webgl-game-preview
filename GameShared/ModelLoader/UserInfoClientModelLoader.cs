#if !NOT_UNITY3D
using Framework;
using SAS.Model;
using SAS.Model.User;

namespace GameShared.ModelLoader
{
	[Bind]
	public class UserInfoClientModelLoader
	{
		public void Load(IContainer container, UserInfoClient rootModel)
		{
			TestModel testModel = container.Resolve<TestModel>();
			testModel.Desctiption = rootModel.Description;
			testModel.TestModelVariable = rootModel.TestNumber;
		}
	}
}
#endif