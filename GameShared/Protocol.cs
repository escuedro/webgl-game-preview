using Framework.Model.Localization;
using Framework.Collections;
using Framework.Serialization;
using SAS.Model;
using SAS.Model.Battle;
using SAS.Model.Time;

namespace GameShared
{
	[RPC]
	public abstract class EmptyRpc
	{
		public class Request
		{
			public static Request Instance = new Request();
		}

		public class Response
		{
			public static Response Instance = new Response();
		}
	}

	[RPC]
	public abstract class Hello
	{
		public class Request
		{
			public int Number;
		}

		public class Response
		{
			public string Message;
		}
	}

	[RPC]
	public abstract class CreateCharacter
	{
		public class Response
		{
			public CharacterDto CharacterDto;
		}
	}

	[RPC]
	public abstract class GetBosses
	{

		public class Response
		{
			public ConstArray<BossDto> BossDtos;
		}
	}
	
	[RPC]
	public abstract class GetBossesDamage
	{
		public class Response
		{
			public ConstArray<BossDamageDto> BossDamageDtos;
		}
	}

	[RPC]
	public abstract class GetBossRatings
	{
		public class Response
		{
			public ConstArray<BossUserRating> TopHundred;
			public BossPlayerRating UserRating;
		}
		
		public class Request
		{
			public long BossId;
		}
	}

	[RPC]
	public abstract class AttackBoss
	{
		public class Request
		{
			public ConstArray<long> AttackerCharacterIds;
			public long BossIdToAttack;
		}

		public class Response
		{
			public ConstArray<long> AttackersDamage;
			public long BossStartHp;
			public long BossEndHp;
			public int BossStaticId;
		}
	}

	[RPC]
	public abstract class LevelUpCharacter
	{
		public class Request
		{
			public long Id;
		}
	}

	[RPC]
	public abstract class GenerateMonsters
	{
		public class Request
		{
			public int MonstersAmount;
		}

		public class Response
		{
			public ConstArray<MonsterDto> MonstersDto;
		}
	}

	[RPC]
	public abstract class GetDynamic
	{
		public class Response
		{
			public UserInfoClient UserInfo;
			public ShortDataRoot ShortData;
			public LongDataRoot LongData;
			public ulong ServerNow;
			public string UserGuid;
		}
	}

	[RPC]
	public abstract class AttackMonster
	{
		public class Request
		{
			public long AttackerCharacterId;
			public MonsterDto MonsterToAttack;
		}

		public class Response
		{
			public uint FightTime;
			public BattleResult BattleResult;
			public uint Essence;
			public long Experience;
			public ConstArray<ItemPair> ItemReward;
		}
	}

	[RPC]
	public abstract class SetSoundSettings
	{
		public class Request
		{
			public Language Language;
			public int SoundsVolume;
		}
	}

	[RPC]
	public abstract class SetBattleSettings
	{
		public class Request
		{
			public bool SkipBattleAnimation;
		}
	}

	[RPC]
	public abstract class GetTokens
	{
		public class Request
		{
			public ChestRewardTime ChestRewardTime;
		}

		public class Response
		{
			public ChestRewardTime ChestRewardTime;
		}
	}

	[RPC]
	public abstract class Transmutate
	{
		public class Request
		{
			public bool UseElixir;
			public long CharacterId1;
			public long CharacterId2;
		}

		public class Response
		{
			public bool Success;
			public bool RemoveCharacter1;
			public bool RemoveCharacter2;
			public bool RemoveElixir;
			public CharacterDto Character;
		}
	}

	[RPC]
	public abstract class DebugAddTokens
	{
		public class Request
		{
			public long Amount;
		}
	}

	[RPC]
	public abstract class BuyShopPosition
	{
		public class Request
		{
			public int ShopPositionId;
			public uint PositionsAmount;
		}

		public class Response
		{
			public int ShopPositionId;
			public uint PositionsAmount;
		}
	}

	[RPC]
	public abstract class BuyStarterPack
	{
		public class Response
		{
			public ConstArray<ItemPair> Reward;
		}
	}

	[RPC]
	public abstract class GetUserChest
	{
		public class Response
		{
			public ChestDto Chest;
		}
	}

	[RPC]
	public abstract class TranslationData
	{
		public class Request
		{
			public TranslationDto TranslationDto;
		}

		public class Response
		{
			public TranslationDto TranslationDto;
		}
	}
}