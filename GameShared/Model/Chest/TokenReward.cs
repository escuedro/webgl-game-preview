﻿using Framework.FixedMath;
using Framework.Reactive;

namespace SAS.Model
{
	public class TokenReward
	{
		public readonly ChestRewardTime ChestRewardTime;
		public readonly Field<Fix64> Count;

		public TokenReward(ChestRewardTime chestRewardTime, Fix64 count)
		{
			ChestRewardTime = chestRewardTime;
			Count = new Field<Fix64>(count);
		}
	}
}