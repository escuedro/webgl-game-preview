﻿using Framework;
using Framework.Hasher;
using Framework.Serialization;
using SAS.Model.Time;

namespace SAS.Model
{
	[GenerateSerialize]
	[GenerateHash]
	public struct ChestRewardTime
	{
		public static TimeDuration RewardDuration = TimeDuration.Hour;

		private bool Equals(ChestRewardTime other)
		{
			return _id == other._id;
		}

		public override bool Equals(object obj)
		{
			return obj is ChestRewardTime other && Equals(other);
		}

		internal uint _id;

		[Pure]
		public uint ToUint() => _id;

		[Pure]
		public TimePoint ToTimePoint() => TimePoint.CreateBySeconds(_id);

		public ChestRewardTime(uint id)
		{
			_id = id;
		}

		public ChestRewardTime(TimePoint timePoint)
		{
			uint periodSeconds = RewardDuration.Seconds;
			_id = timePoint.Seconds / periodSeconds * periodSeconds;
		}

		public override int GetHashCode()
		{
			return (int)_id;
		}

		public static bool operator ==(ChestRewardTime a, ChestRewardTime b)
		{
			return a._id == b._id;
		}

		public static bool operator <=(ChestRewardTime a, ChestRewardTime b)
		{
			return a._id <= b._id;
		}

		public static bool operator >=(ChestRewardTime a, ChestRewardTime b)
		{
			return a._id >= b._id;
		}

		public static bool operator !=(ChestRewardTime a, ChestRewardTime b)
		{
			return a._id != b._id;
		}
	}
}