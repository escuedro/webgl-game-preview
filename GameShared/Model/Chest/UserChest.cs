﻿using System.Collections.Generic;
using Framework;
using Framework.Collections;
using Framework.FixedMath;
using Framework.Reactive;
using SAS.Model.Time;

namespace SAS.Model
{
	[Bind]
	public class UserChest
	{
		public readonly Field<Fix64> TokensAvailable = new Field<Fix64>();
		public readonly ListField<EssenceReward> EssenceRewards = new ListField<EssenceReward>();
		public readonly ListField<TokenReward> TokenRewards = new ListField<TokenReward>();

		internal void AddEssence(ChestRewardTime rewardTime, uint count)
		{
			foreach (EssenceReward essenceReward in EssenceRewards)
			{
				if (essenceReward.ChestRewardTime == rewardTime)
				{
					essenceReward.Count.Value += count;
					return;
				}
			}
			EssenceReward newReward = new EssenceReward(rewardTime, count);
			EssenceRewards.Add(newReward);
		}

		[Pure]
		public uint GetEssenceInTime(ChestRewardTime rewardTime)
		{
			foreach (EssenceReward essenceReward in EssenceRewards)
			{
				if (essenceReward.ChestRewardTime == rewardTime)
				{
					return essenceReward.Count.Value;
				}
			}
			return 0;
		}

		public EssenceReward[] GetEssenceRewardsInPastTime(ChestRewardTime rewardTime)
		{
			List<EssenceReward> essenceRewards = new List<EssenceReward>();
			foreach (EssenceReward essenceReward in EssenceRewards)
			{
				if (essenceReward.ChestRewardTime <= rewardTime)
				{
					essenceRewards.Add(essenceReward);
				}
			}
			return essenceRewards.ToArray();
		}

		public bool TryGetLastEssenceReward(out EssenceReward essenceReward)
		{
			if (EssenceRewards.Count == 0)
			{
				essenceReward = null;
				return false;
			}
			essenceReward = EssenceRewards[EssenceRewards.Count - 1];
			return true;
		}

		public bool TryGetLastTokenReward(out TokenReward tokenReward)
		{
			if (TokenRewards.Count == 0)
			{
				tokenReward = null;
				return false;
			}
			tokenReward = TokenRewards[TokenRewards.Count - 1];
			return true;
		}

		[Pure]
		public Fix64 GetTokensInTime(ChestRewardTime rewardTime)
		{
			foreach (TokenReward tokenReward in TokenRewards)
			{
				if (tokenReward.ChestRewardTime == rewardTime)
				{
					return tokenReward.Count.Value;
				}
			}
			return Fix64.Zero;
		}

		[Pure]
		public Fix64 GetAllTokens()
		{
			Fix64 count = Fix64.Zero;
			foreach (TokenReward tokenReward in TokenRewards)
			{
				count += tokenReward.Count.Value;
			}
			return count;
		}

		public void ConvertEssenceInTime(ChestRewardTime rewardTime, Fix64 essenceToTokens)
		{
			for (int i = EssenceRewards.Count - 1; i >= 0; i--)
			{
				EssenceReward essenceReward = EssenceRewards[i];
				if (essenceReward.ChestRewardTime == rewardTime)
				{
					TokenRewards.Add(new TokenReward(rewardTime, (Fix64)essenceReward.Count.Value * essenceToTokens));
					EssenceRewards.Remove(essenceReward);
					return;
				}
			}
			Log.Error("trying to convert 0 essence");
		}

		public void ApplyTokenRewardInTime(ChestRewardTime rewardTime)
		{
			for (int i = TokenRewards.Count - 1; i >= 0; i--)
			{
				TokenReward tokenReward = TokenRewards[i];
				if (tokenReward.ChestRewardTime == rewardTime)
				{
					TokensAvailable.Value += tokenReward.Count;
					TokenRewards.Remove(tokenReward);
					return;
				}
			}
		}

		public void ApplyAllTokenRewards()
		{
			for (int i = TokenRewards.Count - 1; i >= 0; i--)
			{
				TokenReward tokenReward = TokenRewards[i];
				TokensAvailable.Value += tokenReward.Count;
				TokenRewards.Remove(tokenReward);
			}
		}

		[Pure]
		internal ChestDto Serialize()
		{
			ConstArray<ChestRewardEssence> essences = default;
			if (EssenceRewards.Count > 0)
			{
				ChestRewardEssence[] essencesArray = new ChestRewardEssence[EssenceRewards.Count];
				for (int i = 0; i < EssenceRewards.Count; i++)
				{
					EssenceReward essenceReward = EssenceRewards[i];
					essencesArray[i] = new ChestRewardEssence()
					{
							ChestRewardTime = essenceReward.ChestRewardTime.ToUint(),
							Count = essenceReward.Count
					};
				}
				essences = new ConstArray<ChestRewardEssence>(essencesArray);
			}

			ConstArray<ChestRewardToken> tokens = default;
			if (TokenRewards.Count > 0)
			{
				ChestRewardToken[] tokensArray = new ChestRewardToken[TokenRewards.Count];
				for (int i = 0; i < TokenRewards.Count; i++)
				{
					TokenReward tokenReward = TokenRewards[i];
					tokensArray[i] = new ChestRewardToken()
					{
							ChestRewardTime = tokenReward.ChestRewardTime.ToUint(),
							Count = tokenReward.Count.Value
					};
				}
				tokens = new ConstArray<ChestRewardToken>(tokensArray);
			}

			return new ChestDto() { Essences = essences, Tokens = tokens, TokensAvailable = TokensAvailable };
		}

		internal void Deserialize(ChestDto chest)
		{
			TokensAvailable.Value = chest.TokensAvailable;
			foreach (ChestRewardEssence chestEssence in chest.Essences)
			{
				SetRewardEssence(chestEssence);
			}

			for (int i = EssenceRewards.Count - 1; i >= 0; i--)
			{
				EssenceReward essenceReward = EssenceRewards[i];
				bool existInNew = false;
				foreach (ChestRewardEssence chestEssence in chest.Essences)
				{
					if (essenceReward.ChestRewardTime.ToUint() == chestEssence.ChestRewardTime)
					{
						existInNew = true;
						break;
					}
				}
				if (!existInNew)
				{
					EssenceRewards.RemoveAt(i);
				}
			}
			foreach (ChestRewardToken chestToken in chest.Tokens)
			{
				SetRewardToken(chestToken);
			}
			for (int i = TokenRewards.Count - 1; i >= 0; i--)
			{
				TokenReward tokenReward = TokenRewards[i];
				bool existInNew = false;
				foreach (ChestRewardToken chestToken in chest.Tokens)
				{
					if (tokenReward.ChestRewardTime.ToUint() == chestToken.ChestRewardTime)
					{
						existInNew = true;
						break;
					}
				}
				if (!existInNew)
				{
					TokenRewards.RemoveAt(i);
				}
			}
		}

		private void SetRewardToken(ChestRewardToken chestToken)
		{
			foreach (TokenReward tokenReward in TokenRewards)
			{
				if (tokenReward.ChestRewardTime.ToUint() == chestToken.ChestRewardTime)
				{
					tokenReward.Count.Value = chestToken.Count;
					return;
				}
			}
			TokenReward reward =
					new TokenReward(new ChestRewardTime(chestToken.ChestRewardTime), (Fix64)chestToken.Count);
			TokenRewards.Add(reward);
		}

		private void SetRewardEssence(ChestRewardEssence rewardEssence)
		{
			foreach (EssenceReward essenceReward in EssenceRewards)
			{
				if (essenceReward.ChestRewardTime.ToUint() == rewardEssence.ChestRewardTime)
				{
					essenceReward.Count.Value = rewardEssence.Count;
					return;
				}
			}
			AddEssence(new ChestRewardTime(rewardEssence.ChestRewardTime), rewardEssence.Count);
		}
	}
}