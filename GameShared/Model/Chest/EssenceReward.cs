﻿using Framework.Reactive;

namespace SAS.Model
{
	public class EssenceReward
	{
		public readonly ChestRewardTime ChestRewardTime;
		public readonly Field<uint> Count;

		public EssenceReward(ChestRewardTime chestRewardTime, uint count)
		{
			ChestRewardTime = chestRewardTime;
			Count = new Field<uint>(count);
		}
		
	}
}