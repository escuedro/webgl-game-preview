﻿using Framework.Collections;
using Framework.FixedMath;
using Framework.Hasher;
using Framework.Serialization;

namespace SAS.Model
{
	[GenerateHash]
	[GenerateSerialize]
	public struct ChestDto
	{
		public Fix64 TokensAvailable;
		public ConstArray<ChestRewardEssence> Essences;
		public ConstArray<ChestRewardToken> Tokens;
	}

	[GenerateHash]
	[GenerateSerialize]
	public struct ChestRewardEssence
	{
		public uint ChestRewardTime;
		public uint Count;
	}

	[GenerateHash]
	[GenerateSerialize]
	public struct ChestRewardToken
	{
		public uint ChestRewardTime;
		public Fix64 Count;
	}
}