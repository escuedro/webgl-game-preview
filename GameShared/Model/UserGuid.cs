using Framework;
using Framework.Reactive;

namespace SAS.Model
{
	[Bind]
	public class UserGuid
	{
		// TODO refactor, not used in server
		public readonly Field<string> Guid = new Field<string>();
	}
}