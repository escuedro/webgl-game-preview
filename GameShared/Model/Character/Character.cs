using System;
using Framework;
using Framework.Model;
using Framework.Reactive;
using GameShared;
using SAS.Model.Time;

namespace SAS.Model
{
	public class Character
	{
		public const string IdentityName = "character";
		public static readonly int RarityLevels = Enum.GetNames(typeof(CharacterRarity)).Length - 1;
		public static readonly int ElementTypes = Enum.GetNames(typeof(Element)).Length - 1;

		public readonly Guid Guid;
		public readonly long Id;
		public readonly CharacterStatic Static;
		public readonly Field<long> CurrentExperience;
		public readonly Field<int> Level;
		public readonly CharacterRarity Rarity;
		public readonly Element Element;
		public readonly Field<int> AvailableFightsCount;
		public readonly Field<TimePoint> NextFightSeries;
		public readonly Field<CharacterAvailability> Availability;
		public float LuckMultiplier =>
				Statics.GetSingle<CharacterSharedStatic>().LuckMultipliers[Static.Luck - 1] / 100f;
		public string CardVisual => Static.CardVisual[GetVisualIndex()];

		public TranslationKey TranslationKey => new TranslationKey(Static.TranslationKey);
		public TranslationKey DescriptionTranslationKey => new TranslationKey(Static.DescriptionTranslationKey);

		public CharacterLevelSharedStatic LevelStatic =>
				Statics.GetSingle<CharacterSharedStatic>().Rarity[(int)Rarity - 1].Levels[Level.Value - 1];

		public bool IsMaximumLevel => Level.Value == Statics.GetSingle<CharacterSharedStatic>().MaximumLevel;
		public bool IsMaximumRarity => (int)Rarity == RarityLevels;

		public bool IsAvailable => AvailableFightsCount.Value > 0 || NextFightSeries <= TimePoint.Now;

		public Character(int staticId,
				long id,
				Guid guid,
				long currentExperience,
				int level,
				CharacterRarity rarity,
				Element element,
				int availableFightsCount,
				TimePoint nextFightSeries,
				CharacterAvailability availability)
		{
			Static = Statics.Get<CharacterStatic>(staticId);
			Id = id;
			Guid = guid;
			CurrentExperience = new Field<long>(currentExperience);
			Level = new Field<int>(level);
			Rarity = rarity;
			Element = element;
			AvailableFightsCount = new Field<int>(availableFightsCount);
			NextFightSeries = new Field<TimePoint>(nextFightSeries);
			Availability = new Field<CharacterAvailability>(availability);
		}

		public void AddExperience(long experience)
		{
			long experienceToNextLevel = LevelStatic.ExperienceToNextLevel;
			long currentExperience = CurrentExperience.Value;
			long experienceToAdd = experience;
			if (currentExperience + experience > experienceToNextLevel)
			{
				experienceToAdd = experienceToNextLevel - currentExperience;
			}
			if (experienceToAdd > 0)
			{
				CurrentExperience.Value += experienceToAdd;
			}
		}

		public CharacterDto Serialize()
		{
			Character character = this;
			return new CharacterDto
			{
					Id = character.Id,
					Guid = Guid.ToString(),
					StaticId = character.Static.Id,
					Experience = character.CurrentExperience,
					Level = character.Level,
					Rarity = character.Rarity,
					Element = character.Element,
					AvailableFightsCount = character.AvailableFightsCount,
					NextFightSeriesSeconds = character.NextFightSeries.Value.Seconds,
					Availability = character.Availability
			};
		}

		private int GetVisualIndex()
		{
			int visualIndex = 0;

			if (Level.Value >= 1 && Level.Value <= 3)
			{
				visualIndex = 0;
			}
			if (Level.Value >= 4 && Level.Value <= 6)
			{
				visualIndex = 1;
			}
			if (Level.Value >= 7 && Level.Value <= 9)
			{
				visualIndex = 2;
			}
			return visualIndex;
		}
	}
}