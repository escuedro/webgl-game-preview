namespace SAS.Model
{
	public enum CharacterAvailability
	{
		Undefined = 0,
		Available = 1,
		InStocks = 2
	}
}