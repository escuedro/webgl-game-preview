namespace SAS.Model
{
	public enum CharacterRarity
	{
		Undefined = 0,
		Common = 1,
		Uncommon = 2,
		Rare = 3,
		Epic = 4,
		Legendary = 5,
		Mythical = 6
	}
}