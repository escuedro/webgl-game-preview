using Framework;
using Framework.Collections;
using Framework.Reactive;

namespace SAS.Model
{
	[Bind]
	public class UserCharacters
	{
		public ListField<Character> Characters = new ListField<Character>();

		[CanBeNull]
		[MustUseReturnValue]
		public Character TryGet(long id)
		{
			if (Characters != null)
			{
				foreach (Character character in Characters)
				{
					if (character.Id == id)
					{
						return character;
					}
				}
			}
			return null;
		}

		[CanBeNull]
		public Character[] GetMultiple(long[] ids)
		{
			if (ids.Length == 0)
			{
				return null;
			}
			Character[] characters = new Character[ids.Length];
			int next = 0;

			foreach (Character character in Characters)
			{
				if (ids.ContainsValue(character.Id))
				{
					characters[next++] = character;
				}
			}
			if (next != ids.Length)
			{
				return null;
			}
			return characters;
		}

		public bool HasPair(Character characterToCompare)
		{
			foreach (Character character in Characters)
			{
				if (character != characterToCompare)
				{
					if (character.Static.Id == characterToCompare.Static.Id &&
							character.Rarity == characterToCompare.Rarity)
					{
						return true;
					}
				}
			}
			return false;
		}
	}
}