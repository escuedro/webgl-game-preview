﻿using Framework.Model;
using Framework.RandomUtils;

namespace SAS.Model
{
	[System.Serializable]
	public class CharacterGenerationChance : StaticObject, IWeightedElement
	{
		public int GenerationWeight;
		public int Weight => GenerationWeight;
	}
}