using System;
using Framework.Model;

namespace SAS.Model
{
	[Serializable]
	public class CharacterStatic : StaticObject
	{
		public string TranslationKey;
		public string DescriptionTranslationKey;
		public Element Element;
		public uint Defence;
		public uint Attack;
		public uint Luck;
		public string[] CardVisual;
		public string PrefabResourcePath;
		public string PortraitResourcePath;
	}

	[Serializable]
	public class CharacterLevelStatic
	{
		public string Visual;
	}

	[Serializable]
	public class CharacterLevelSharedStatic
	{
		public long ExperienceToNextLevel;
		public long TokensToNexLevel;
	}

	[Serializable]
	public class CharacterRaritySharedStatic
	{
		public CharacterLevelSharedStatic[] Levels;
	}

	[Serializable]
	public class CharacterSharedStatic
	{
		public uint MaximumLevel;
		public int MaximumRarity;
		public uint[] Power;
		public uint[] AvailableFightsCount;
		public CharacterRaritySharedStatic[] Rarity;
		public string[] FrameVisualByRarity;
		public string[] TitleVisualByRarity;
		public string[] DecorRarityByVisual;
		public string[] FlagVisualByElement;
		public string[] BackgroundByRarity;
		public uint[] LuckMultipliers; // divide by 100. count equals to max luck -1
	}
}