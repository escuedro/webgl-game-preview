﻿using Framework.Model;
using Framework.RandomUtils;

namespace SAS.Model
{
	[System.Serializable]
	public class CharacterRarityGenerationChance : StaticObject, IWeightedElement
	{
		public CharacterRarity Rarity;
		public int GenerationWeight;
		public int Weight => GenerationWeight;
	}
}