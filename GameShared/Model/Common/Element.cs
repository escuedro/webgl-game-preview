using System;

namespace SAS.Model
{
	public enum Element
	{
		Undefined = 0,
		Earth = 1,
		Fire = 2,
		Air = 3,
		Water = 4
	}

	public class ElementUtils
	{
		public static int CompareElement(Element left, Element right)
		{
			switch (left)
			{
				case Element.Fire:
					switch (right)
					{
						case Element.Water: return -1;
						case Element.Earth: return 1;
						default: return 0;
					}
				case Element.Earth:
					switch (right)
					{
						case Element.Fire: return -1;
						case Element.Air: return 1;
						default: return 0;
					}
				case Element.Air:
					switch (right)
					{
						case Element.Earth: return -1;
						case Element.Water: return 1;
						default: return 0;
					}
				case Element.Water:
					switch (right)
					{
						case Element.Air: return -1;
						case Element.Fire: return 1;
						default: return 0;
					}

				default: return 0;
			}
		}

		public static Element GetOppositeElement(Element element)
		{
			switch (element)
			{
				case Element.Undefined:
					return element;
				case Element.Earth:
					return Element.Fire;
				case Element.Fire:
					return Element.Water;
				case Element.Air:
					return Element.Earth;
				case Element.Water:
					return Element.Air;
				default:
					throw new ArgumentOutOfRangeException(nameof(element), element, null);
			}
		}
	}
}