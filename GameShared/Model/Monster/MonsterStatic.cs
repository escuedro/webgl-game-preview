﻿using System;
using Framework.Model;

namespace SAS.Model
{
	[Serializable]
	public class MonsterStatic : StaticObject
	{
		public uint[] ChanceByLevel;
		public long[] ExperienceForWin;
		public long[] ExperienceForLose;
		public uint[] EssenceForWin;
		public uint[] TokenRewardChance;
		
		public string TranslationKey;

		public string PrefabResourcePath;
		public string PortraitResourcePath;
		public string CardVisualPath;
	}
}