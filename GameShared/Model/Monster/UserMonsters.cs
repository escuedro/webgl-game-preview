﻿using Framework;
using Framework.Reactive;
using GameShared;

namespace SAS.Model
{
    [Bind]
    public class UserMonsters
    {
        public ListField<Monster> Monsters = new ListField<Monster>();

        public void Remove(MonsterDto monsterDto)
        {
            Monster monsterToRemove = null;
            foreach (Monster monster in Monsters)
            {
                if (monster.Level == monsterDto.Level && monster.Static.Id == monsterDto.StaticId)
                {
                    monsterToRemove = monster;
                    break;
                }
            }

            if (monsterToRemove != null)
            {
                Monsters.Remove(monsterToRemove);
            }
        }
    }
}