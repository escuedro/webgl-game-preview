﻿using Framework;
using Framework.Model;
using Framework.Reactive;
using GameShared;

namespace SAS.Model
{
	public class Monster
	{
		public readonly MonsterStatic Static;
		public readonly int Level;

		public uint WinChance => Static.ChanceByLevel[Level - 1];
		public uint TicketRewardChance => Static.TokenRewardChance[Level - 1];
		public long ExperienceForWin => Static.ExperienceForWin[Level - 1];
		public long ExperienceForLose => Static.ExperienceForLose[Level - 1];
		public uint EssenceForWin => Static.EssenceForWin[Level - 1];

		public TranslationKey TranslationKey => new TranslationKey(Static.TranslationKey);

		public Monster(int staticID,
				int level)
		{
			Static = Statics.Get<MonsterStatic>(staticID);
			Level = new Field<int>(level);
		}

		public MonsterDto Serialize()
		{
			return new MonsterDto()
			{
					Level = Level,
					StaticId = Static.Id
			};
		}
	}
}