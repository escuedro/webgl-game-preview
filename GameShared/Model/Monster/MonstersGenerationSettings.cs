﻿using System;

namespace SAS.Model
{
    [Serializable]
    public class MonstersGenerationSettings
    {
        public uint MaxMonstersAmount;
        public uint MaxMonstersLevel;
    }
}