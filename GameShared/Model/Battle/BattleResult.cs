﻿using Framework.Hasher;
using Framework.Serialization;
using GameShared;

namespace SAS.Model.Battle
{
	[GenerateSerialize]
	[GenerateHash]
	public struct BattleResult
	{
		public BattleResultType ResultType;
		public long AttackerCharacterId;
		public MonsterDto AttackedMonster;
	}

	public enum BattleResultType
	{
		Undefined = 0,
		Win = 1,
		Lose = 2
	}
}