﻿using System;

namespace SAS.Model.Battle
{
	[Serializable]
	public class TransmutationSettings
	{
		public uint SuccessTransmutateChance;
		public uint CharacterDeathChance;
	}
}