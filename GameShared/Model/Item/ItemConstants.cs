using Framework.Model;

namespace SAS.Model
{
	public class ItemConstants
	{
		public static ItemStatic Contract => Statics.Get<ItemStatic>((int)ItemType.Contract);
		public static ItemStatic Folliant => Statics.Get<ItemStatic>((int)ItemType.Folliant);
		public static ItemStatic Elixir => Statics.Get<ItemStatic>((int)ItemType.Elixir);
		public static ItemStatic Ticket => Statics.Get<ItemStatic>((int)ItemType.Ticket);
	}
}