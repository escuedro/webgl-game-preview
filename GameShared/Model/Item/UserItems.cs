using System;
using Framework;
using Framework.Model;

namespace SAS.Model
{
	[Bind]
	public class UserItems
	{
		public readonly int ItemsCount;
		private readonly Item[] _items;

		public UserItems()
		{
			ItemsCount = Enum.GetValues(typeof(ItemType)).Length;
			_items = new Item[ItemsCount];
			for (int i = 0; i < _items.Length; i++)
			{
				_items[i] = new Item(Statics.Get<ItemStatic>(i + 1), 0);
			}
		}

		[Pure]
		public Item GetItem(ItemType itemType)
		{
			return _items[(int)itemType - 1];
		}

		[Pure]
		public Item GetItem(int itemId)
		{
			return _items[itemId - 1];
		}

		[Pure]
		public bool HasPrice(ItemPair price)
		{
			Item item = GetItem(price.Id);
			return item.Count >= price.Count;
		}

		internal void AddReward(ItemPair reward)
		{
			GetItem(reward.Id).Count.Value += reward.Count;
		}

		internal void RemoveItem(ItemType itemType, uint count)
		{
			GetItem(itemType).Count.Value -= count;
		}
	}
}