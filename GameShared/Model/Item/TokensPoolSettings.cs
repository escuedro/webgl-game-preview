﻿using System;

namespace SAS.Model
{
	[Serializable]
	public class TokensPoolSettings
	{
		public uint PoolSize;
	}
}