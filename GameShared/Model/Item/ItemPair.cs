using System;
using Framework.Hasher;
using Framework.Model;
using Framework.Serialization;

namespace SAS.Model
{
	[Serializable]
	[GenerateSerialize]
	[GenerateHash]
	public struct ItemPair
	{
#if NOT_UNITY3D
		[System.Text.Json.Serialization.JsonIgnore]
#endif
		public ItemStatic ItemStatic => Statics.Get<ItemStatic>(Id);
		public int Id;
		public uint Count;

		public ItemPair(int id, uint count)
		{
			Id = id;
			Count = count;
		}

		public ItemPair(ItemType id, uint count)
		{
			Id = (int)id;
			Count = count;
		}
	}
}