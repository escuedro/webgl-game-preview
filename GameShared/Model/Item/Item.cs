using Framework.Reactive;

namespace SAS.Model
{
	public class Item
	{
		public readonly ItemStatic Static;
		public readonly Field<uint> Count;

		public Item(ItemStatic itemStatic, uint count)
		{
			Static = itemStatic;
			Count = new Field<uint>(count);
		}
	}
}