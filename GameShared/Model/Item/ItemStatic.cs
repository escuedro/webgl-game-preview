using System;
using Framework.Model;

namespace SAS.Model
{
	[Serializable]
	public class ItemStatic : StaticObject
	{
		public string Name;
		public string DescriptionTranslationKey;
		public string IconVisual;
		public string BackgroundVisual;
	}
}