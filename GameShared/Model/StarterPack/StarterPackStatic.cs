using System;

namespace SAS.Model.StarterPack
{
	[Serializable]
	public class StarterPackStatic
	{
		public uint PriceInTokens;
		public ItemPair[] FirstBuy;
		public ItemPair[] SecondBuy;
	}
}