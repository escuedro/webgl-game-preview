using Framework;
using Framework.Reactive;

namespace SAS.Model.StarterPack
{
	[Bind]
	public class UserStarterPack
	{
		public readonly Field<uint> CountPurchased = new Field<uint>();
	}
}