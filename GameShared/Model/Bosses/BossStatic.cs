﻿using System;
using Framework.Model;

namespace SAS.Model
{
	[Serializable]
	public class BossStatic : StaticObject
	{
		public string TranslationKey;
		public bool IsSuperBoss;
		public long HealthPoints;
		public int LifetimeSeconds;
		public int WeekRewardPool;
		public Element Element;
		public int MinCharactersInGroupCriterion;
		public CharacterRarity CharactersRarity;
		public uint TicketsCost;

		public string PrefabResourcePath;
		public string PortraitResourcePath;
		public string CardVisualPath;
	}
}