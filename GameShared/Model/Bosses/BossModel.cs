#if NOT_UNITY3D
namespace SAS.Model
{
	public class BossModel
	{
		public long Id { get; set; }

		public int StaticId { get; set; }

		public long HealthPoints { get; set; }

		public int DeathSeconds { get; set; }

		public int Element { get; set; }

		public bool Processed { get; set; }
	}
}
#endif