#if NOT_UNITY3D
using System.Collections.Generic;
using SAS.Model;

namespace GameShared
{
	public interface IBossStorage
	{
		List<BossModel> GetAliveBosses();
		List<BossModel> GetUnprocessedBosses();

		void InsertBosses(IEnumerable<BossModel> bosses);
		void UpdateBoss(long bossId, int bossStaticId, long userId, string guid, long damage);

		void ClearBosses();

		void UpdateBossProcessed(long bossId);

		List<BossAttackerModel> GetBossAtackers(long bossId);
		List<BossAttackerModel> GetUserAttackedBosses(long userId);
		List<BossUserRating> GetBossRating(BossModel bossModel, int maxCount);
		BossPlayerRating GetBossPlayerRating(BossModel bossModel, long userId, string userGuid);
		BossModel GetBoss(long bossId);
	}
}
#endif