#if !NOT_UNITY3D
using Framework.Model;
using Framework.Reactive;
using GameShared;
using SAS.Model.Time;

namespace SAS.Model
{
	public class ClientBoss
	{
		public readonly long Id;
		public readonly BossStatic Static;
		public readonly Field<long> HealthPoints;
		public readonly TimePoint DeathTime;
		public Element Element;

		public ClientBoss(BossDto bossDto)
		{
			Id = bossDto.BossId;
			Static = Statics.Get<BossStatic>(bossDto.StaticId);
			HealthPoints = new Field<long>(bossDto.HealthPoints);
			DeathTime = TimePoint.CreateBySeconds(bossDto.DeathTime);
			Element = bossDto.Element;
		}

		public void Deserialize(BossDto bossDto)
		{
			HealthPoints.Value = bossDto.HealthPoints;
		}
	}
}
#endif