#if !NOT_UNITY3D
using Framework;
using Framework.Collections;
using Framework.Reactive;
using GameShared;

namespace SAS.Model
{
	[Bind]
	public class ClientBosses
	{
		public readonly ListField<ClientBoss> Bosses = new ListField<ClientBoss>();

		public void Deserialize(ConstArray<BossDto> dynamic)
		{
			foreach (BossDto bossDto in dynamic)
			{
				LoadBoss(bossDto);
			}

			for (int i = Bosses.Count - 1; i >= 0; i--)
			{
				ClientBoss clientBoss = Bosses[i];
				bool existInNew = false;
				foreach (BossDto bossDto in dynamic)
				{
					if (bossDto.BossId == clientBoss.Id)
					{
						existInNew = true;
						break;
					}
				}
				if (!existInNew)
				{
					Bosses.RemoveAt(i);
				}
			}
		}

		private void LoadBoss(BossDto bossDto)
		{
			foreach (ClientBoss boss in Bosses)
			{
				if (boss.Id == bossDto.BossId)
				{
					boss.Deserialize(bossDto);
					return;
				}
			}
			Bosses.Add(new ClientBoss(bossDto));
		}

		public bool TryGetBoss(int staticId, out ClientBoss clientBoss)
		{
			foreach (ClientBoss boss in Bosses)
			{
				if (boss.Static.Id == staticId)
				{
					clientBoss = boss;
					return true;
				}
			}
			clientBoss = null;
			return false;
		}
	}
}
#endif