#if NOT_UNITY3D

namespace SAS.Model
{
	public class BossAttackerModel
	{
		public long BossId { get; set; }

		public int StaticId { get; set; }
		public long UserId { get; set; }
		public string UserGuid { get; set; }

		public long Damage { get; set; }
	}
}
#endif