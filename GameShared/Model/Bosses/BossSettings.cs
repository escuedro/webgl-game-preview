using System;
using Framework.Model;

namespace SAS.Model
{
	[Serializable]
	public class BossSettings : StaticObject
	{
		public uint[] UniteDamageMultipliers;
		public int[] RarityBonuses;
		public uint VictoryReward;
		public uint TimeoutReward;
		public uint BossElementModificator;
		public uint MinimumCharacterToBossDamage;
	}
}