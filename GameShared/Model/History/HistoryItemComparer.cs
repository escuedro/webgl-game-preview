﻿using System.Collections.Generic;

namespace SAS.Model.History
{
	public class HistoryItemComparer : IComparer<HistoryItem>
	{
		public int Compare(HistoryItem x, HistoryItem y)
		{
			if (x == null)
			{
				return -1;
			}
			if (y == null)
			{
				return 1;
			}
			return y.HistoryData.FightTime.CompareTo(x.HistoryData.FightTime);
		}
	}
}