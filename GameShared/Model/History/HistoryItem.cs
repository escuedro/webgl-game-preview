﻿namespace SAS.Model.History
{
	public class HistoryItem
	{
		public readonly HistoryData HistoryData;

		public HistoryItem(HistoryData historyData)
		{
			HistoryData = historyData;
		}
	}
}