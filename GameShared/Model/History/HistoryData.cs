﻿using Framework.Hasher;
using Framework.Serialization;
using SAS.Model.Battle;

namespace SAS.Model.History
{
	[GenerateHash]
	[GenerateSerialize]
	public struct HistoryData
	{
		public long CharacterId;
		public int Level;
		public BattleResultType Result;
		public uint FightTime;
		public long Experience;
		public uint Essence;
	}
}