﻿using Framework;
using Framework.Collections;
using Framework.Reactive;
using GameShared;
using SAS.Model.Battle;
using SAS.Model.Time;

namespace SAS.Model.History
{
	[Bind]
	public class UserHistory{
		private const int HistorySize = 10;

		//order is old is first, new is last
		public readonly ListField<HistoryItem> History = new ListField<HistoryItem>();

		public void AddRecord(Character character, BattleResultType result, TimePoint fightTime, uint essence, long experience)
		{
			HistoryData historyData = new HistoryData
			{
					CharacterId = character.Id,
					Level = character.Level,
					FightTime = fightTime.Seconds,
					Essence = essence,
					Experience = experience,
					Result = result
			};
			History.Add(new HistoryItem(historyData));
			while (History.Count > HistorySize)
			{
				History.RemoveFirst();
			}
		}

		public HistoryDto Serialize()
		{
			HistoryData[] result = new HistoryData[History.Count];
			for (int i = 0; i < History.Count; i++)
			{
				result[i] = History[i].HistoryData;
			}
			return new HistoryDto {FightHistory = new ConstArray<HistoryData>(result)};
		}

		public void Deserialize(HistoryDto historyDto)
		{
			History.Clear();
			foreach (HistoryData historyData in historyDto.FightHistory)
			{
				History.Add(new HistoryItem(historyData));
			}
		}
	}
}