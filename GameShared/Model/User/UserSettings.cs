﻿using Framework;
using Framework.Model.Localization;
using Framework.Reactive;

namespace SAS.Model.User
{
	[Bind]
	public class UserSettings
	{
		public Field<Language> Language = new Field<Language>();
		public Field<int> SoundsVolume = new Field<int>();
		public Field<bool> SkipBattleAnimation = new Field<bool>();
	}
}