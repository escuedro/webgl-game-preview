﻿using System;
using Framework.Model;

namespace SAS.Model.Shop
{
	[Serializable]
	public class ShopPositionStatic : StaticObject
	{
		public ItemPair Item;
		public uint PriceInTokens;
	}
}