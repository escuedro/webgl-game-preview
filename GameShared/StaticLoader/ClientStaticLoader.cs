﻿#if !NOT_UNITY3D
using System.Text;
using Framework;
using UnityEngine;

namespace GameShared.Static
{
	public class ClientStaticLoader : StaticLoader
	{
		public void Load(ResourcePath staticsPath)
		{
			TextAsset staticAsset = Resources.Load<TextAsset>(staticsPath.Value);
			string staticJson = Encoding.Default.GetString(staticAsset.bytes);
			StaticRoot staticRoot = JsonUtility.FromJson<StaticRoot>(staticJson);
			Load(staticRoot);
		}
	}
}
#endif