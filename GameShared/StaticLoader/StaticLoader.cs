using Framework.Model;

namespace GameShared.Static
{
    public abstract class StaticLoader
    {
        protected void Load(StaticRoot root)
        {
            var statics = Statics.Init();
            statics.RegisterStatic(root.Characters);
            statics.RegisterStatic(root.Items);
            statics.RegisterStatic(root.Monsters);
            statics.RegisterStatic(root.Bosses);
            statics.RegisterStatic(root.ShopPositions);
            statics.RegisterStatic(root.CharacterGenerationChances);
            statics.RegisterStatic(root.CharacterRarityGenerationChances);
            statics.RegisterSingle(root.CharacterShared);
            statics.RegisterSingle(root.MonstersGenerationSettings);
            statics.RegisterSingle(root.CharactersFightSeriesSettings);
            statics.RegisterSingle(root.TransmutationSettings);
            statics.RegisterSingle(root.TokensPoolSettings);
            statics.RegisterSingle(root.StarterPack);
            statics.RegisterSingle(root.BossSettings);
        }
    }
}