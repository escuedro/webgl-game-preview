﻿#if NOT_UNITY3D
using System.IO;
using System.Text.Json;

namespace GameShared.Static
{
	public class ServerStaticLoader : StaticLoader
	{
		private readonly JsonSerializerOptions _jsonOptions = new JsonSerializerOptions
		{
				IncludeFields = true
		};

		public void Load(string filePath)
		{
			string staticJson = File.ReadAllText(filePath);
			StaticRoot staticRoot = JsonSerializer.Deserialize<StaticRoot>(staticJson, _jsonOptions);
			Load(staticRoot);
		}
	}
}
#endif