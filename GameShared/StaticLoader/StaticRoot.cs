﻿using System;
using SAS.Model;
using SAS.Model.Battle;
using SAS.Model.Shop;
using SAS.Model.StarterPack;

namespace GameShared.Static
{
    [Serializable]
    public class StaticRoot
    {
        public CharacterStatic[] Characters;
        public CharacterSharedStatic CharacterShared;
        public ItemStatic[] Items;
        public MonsterStatic[] Monsters;
        public BossStatic[] Bosses;
        public ShopPositionStatic[] ShopPositions;
        public CharacterGenerationChance[] CharacterGenerationChances;
        public CharacterRarityGenerationChance[] CharacterRarityGenerationChances;
        public MonstersGenerationSettings MonstersGenerationSettings;
        public CharactersFightSeriesSettings CharactersFightSeriesSettings;
        public TransmutationSettings TransmutationSettings;
        public TokensPoolSettings TokensPoolSettings;
        public StarterPackStatic StarterPack;
        public BossSettings BossSettings;
    }
}