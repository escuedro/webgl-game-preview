﻿using Framework.Client.Translations;
using Framework.Model.Localization;
using Framework.Collections;
using Framework.Hasher;
using Framework.Model;
using Framework.Serialization;
using SAS.Model;
using SAS.Model.History;

namespace GameShared
{
#if NOT_UNITY3D
	[GenerateSerialize]
	[GenerateHash]
	public struct UserInfoRoot
	{
		public string Description;
		public int TestNumber;
		public ConstArray<LocalIdentityRange> Identities;
	}
#endif
	[GenerateSerialize]
	public struct UserInfoClient
	{
		public string Description;
		public int TestNumber;
	}


	[GenerateSerialize]
	[GenerateHash]
	public struct ShortDataRoot
	{
		public ConstArray<CharacterDto> Characters;
		public ConstArray<MonsterDto> Monsters;
		public HistoryDto History;
		public ChestDto Chest;
		public Language Language;
		public int SoundsVolume;
		public bool SkipBattleAnimation;
	}

	[GenerateSerialize]
	[GenerateHash]
	public struct LongDataRoot
	{
		public ConstArray<ItemPair> Items;
		public uint StarterPacksPurchased;
	}

	[GenerateSerialize]
	[GenerateHash]
	public struct CharacterDto
	{
		public string Guid;
		public long Id;
		public int StaticId;
		public long Experience;
		public int Level;
		public CharacterRarity Rarity;
		public Element Element;
		public int AvailableFightsCount;
		public uint NextFightSeriesSeconds;
		public CharacterAvailability Availability;
	}

	[GenerateSerialize]
	[GenerateHash]
	public struct HistoryDto
	{
		public ConstArray<HistoryData> FightHistory;
	}

	[GenerateSerialize]
	[GenerateHash]
	public struct MonsterDto
	{
		public int StaticId;
		public int Level;
	}

	[GenerateSerialize]
	[GenerateHash]
	public struct BossDto
	{
		public int StaticId;

		public long BossId;

		public long HealthPoints;

		public uint DeathTime;

		public Element Element;

		public int MinCharactersInGroupCriterion;

		public CharacterRarity CharactersRarity;
	}

	[GenerateSerialize]
	[GenerateHash]
	public struct BossDamageDto
	{
		public long BossId;
		public int StaticId;
		public long Damage;
	}

	[GenerateSerialize]
	[GenerateHash]
	public struct BossUserRating
	{
		public string UserGuid;
		public long Damage;
		public uint ExpectedEssence;
	}

	[GenerateSerialize]
	[GenerateHash]
	public struct BossPlayerRating
	{
		public bool IsInRating;
		public string UserGuid;
		public uint Place;
		public long Damage;
		public uint ExpectedEssence;
	}

	[GenerateSerialize]
	[GenerateHash]
	public struct TranslationDto
	{
		public ConstArray<KeyTranslationPair> Translations;
	}
}