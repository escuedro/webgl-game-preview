﻿using Framework.RPC;
using SAS.Model;

#if NOT_UNITY3D
using System.Collections.Generic;
using Framework.Collections;
using Framework.Model;
#endif

namespace GameShared
{
	public class GetBossesCommand : RpcCommand<EmptyRpc.Request, GetBosses.Response>
	{
#if NOT_UNITY3D
		protected override GetBosses.Response Execute(EmptyRpc.Request request)
		{
			IBossStorage bossStorage = Get<IBossStorage>();
			List<BossModel> bossModels = bossStorage.GetAliveBosses();

			BossDto[] bossDtos = new BossDto[bossModels.Count];

			for (int i = 0; i < bossModels.Count; ++i)
			{
				BossModel bossModel = bossModels[i];
				BossStatic bossStatic = Statics.Get<BossStatic>(bossModel.StaticId);

				bossDtos[i] = new BossDto
				{
						StaticId = bossModel.StaticId,
						BossId = bossModel.Id,
						HealthPoints = bossModel.HealthPoints,
						DeathTime = (uint)bossModel.DeathSeconds,
						Element = (Element)bossModel.Element,
						MinCharactersInGroupCriterion = bossStatic.MinCharactersInGroupCriterion,
						CharactersRarity = bossStatic.CharactersRarity
				};
			}

			return new GetBosses.Response
			{
					BossDtos = new ConstArray<BossDto>(bossDtos)
			};
		}
#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(GetBosses.Response response)
		{
			Get<ClientBosses>().Deserialize(response.BossDtos);
		}
#endif
	}
}