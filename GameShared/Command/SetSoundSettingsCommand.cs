﻿using Framework.Model.Localization;
using Framework.RPC;
using SAS.Model.User;

namespace GameShared
{
	public class SetSoundSettingsCommand : RpcCommand<SetSoundSettings.Request, EmptyRpc.Response>
	{
#if NOT_UNITY3D
		protected override EmptyRpc.Response Execute(SetSoundSettings.Request request)
		{
			SetSettings(request.Language, request.SoundsVolume);

			return EmptyRpc.Response.Instance;
		}

#else
		private readonly Language _language;
		private readonly int _soundsVolume;

		public SetSoundSettingsCommand(Language language, int soundsVolume)
		{
			_language = language;
			_soundsVolume = soundsVolume;
		}

		protected override SetSoundSettings.Request GetRequest()
		{
			return new SetSoundSettings.Request()
			{
					Language = _language,
					SoundsVolume = _soundsVolume
			};
		}

		protected override void OnResponse(EmptyRpc.Response response)
		{
			SetSettings(_language, _soundsVolume);
		}
#endif

		private void SetSettings(Language language, int soundsVolume)
		{
			UserSettings userSettings = Get<UserSettings>();
			userSettings.Language.Value = language;
			userSettings.SoundsVolume.Value = soundsVolume;
		}
	}
}