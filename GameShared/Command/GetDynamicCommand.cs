using Framework.RPC;
#if NOT_UNITY3D
using GameShared.ModelLoader;
using SAS.Model.Time;

#else
using SAS.Model;
using Framework;
using Framework.Model;
using GameShared.ModelLoader;
using SAS.Model.Time;
#endif

namespace GameShared
{
	public class GetDynamicCommand : RpcCommand<EmptyRpc.Request, GetDynamic.Response>
	{
#if NOT_UNITY3D
		protected override GetDynamic.Response Execute(EmptyRpc.Request request)
		{
			UserInfoRoot userInfoRoot = Get<UserInfoModelLoader>().Save(UserContext.Container);

			UserInfoClient userInfoClient = new UserInfoClient()
					{ Description = userInfoRoot.Description, TestNumber = userInfoRoot.TestNumber };
			ShortDataRoot shortDataRoot = Get<ShortDataModelLoader>().Save(UserContext.Container);
			LongDataRoot longDataRoot = Get<LongDataModelLoader>().Save(UserContext.Container);
			TimePoint now = TimePoint.Now;

			return new GetDynamic.Response()
			{
					UserInfo = userInfoClient,
					ShortData = shortDataRoot,
					LongData = longDataRoot,
					ServerNow = now.Milliseconds,
					UserGuid = UserContext.Guid
			};
		}
#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(GetDynamic.Response response)
		{
			IContainer container = Get<IContainer>();
			Get<LongDataModelLoader>().Load(container, response.LongData);
			Get<ShortDataModelLoader>().Load(container, response.ShortData);
			Get<UserInfoClientModelLoader>().Load(container, response.UserInfo);
			Date.GetInstance().SetTime(TimePoint.CreateByMilliseconds(response.ServerNow));
			Get<UserGuid>().Guid.Value = response.UserGuid;
		}
#endif
	}
}