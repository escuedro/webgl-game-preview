using Framework.RPC;
using SAS.Model;
using SAS.Model.Time;

namespace GameShared
{
	public class DebugResetCharacterCooldownsCommand : RpcCommand<EmptyRpc.Request, EmptyRpc.Response>
	{
#if NOT_UNITY3D
		protected override EmptyRpc.Response Execute(EmptyRpc.Request request)
		{
			ResetCooldowns();
			return EmptyRpc.Response.Instance;
		}
#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(EmptyRpc.Response response)
		{
			ResetCooldowns();
		}
#endif

		private void ResetCooldowns()
		{
			UserCharacters userCharacters = Get<UserCharacters>();
			foreach (Character character in userCharacters.Characters)
			{
				character.NextFightSeries.Value = TimePoint.Now - TimeDuration.Second;
			}
		}
	}
}