using Framework.RPC;
using SAS.Model;

namespace GameShared
{
	public class GetUserChestCommand : RpcCommand<EmptyRpc.Request, GetUserChest.Response>
	{
#if NOT_UNITY3D
		protected override GetUserChest.Response Execute(EmptyRpc.Request request)
		{
			ChestDto chest = Get<UserChest>().Serialize();
			return new GetUserChest.Response()
			{
					Chest = chest
			};
		}
#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(GetUserChest.Response response)
		{
			Get<UserChest>().Deserialize(response.Chest);
		}
#endif
	}
}