﻿using Framework.RPC;
using SAS.Model;

namespace GameShared
{
	public class DebugClearCharactersCommand : RpcCommand<EmptyRpc.Request, EmptyRpc.Response>
	{
		#if NOT_UNITY3D
		protected override EmptyRpc.Response Execute(EmptyRpc.Request request)
		{
			ClearCharacters();
			return EmptyRpc.Response.Instance;
		}
		
		#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(EmptyRpc.Response response)
		{
			ClearCharacters();
		}

		#endif
		
		private void ClearCharacters()
		{
			UserCharacters userCharacters = Get<UserCharacters>();
			userCharacters.Characters.Clear();
		}
	}
}