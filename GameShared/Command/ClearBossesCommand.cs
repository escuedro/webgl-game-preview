﻿using Framework.RPC;
using SAS.Model;

namespace GameShared
{
	public class ClearBossesCommand : RpcCommand<EmptyRpc.Request, EmptyRpc.Response>
	{
#if NOT_UNITY3D

		protected override EmptyRpc.Response Execute(EmptyRpc.Request request)
		{
			Get<IBossStorage>().ClearBosses();
			return EmptyRpc.Response.Instance;
		}

#else
		
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(EmptyRpc.Response response)
		{
			Get<ClientBosses>().Bosses.Clear();
		}

#endif
	}
}