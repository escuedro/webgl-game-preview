﻿using System.Collections.Generic;
using Framework;
using Framework.Collections;
using Framework.RPC;
using SAS.Model;

#if NOT_UNITY3D
using System;
using Framework.Model;
using SAS.Model.Time;
#endif

namespace GameShared
{
	public class GenerateMonstersCommand : RpcCommand<GenerateMonsters.Request, GenerateMonsters.Response>
	{
#if NOT_UNITY3D
		protected override GenerateMonsters.Response Execute(GenerateMonsters.Request request)
		{
			Random random = new Random((int)TimePoint.Now.Seconds);
			MonsterStatic[] monsterStatics = Statics.GetAll<MonsterStatic>();
			MonstersGenerationSettings generationSettings = Statics.GetSingle<MonstersGenerationSettings>();
			UserMonsters userMonsters = Get<UserMonsters>();
			int monstersAmount = request.MonstersAmount;
			if (monstersAmount > generationSettings.MaxMonstersAmount)
			{
				throw new ValidateException("Monsters amount bigger than max monsters amount");
			}

			userMonsters.Monsters.Clear();

			List<MonsterStatic> staticsToGenerate = GetRandomMonsterStatics(monstersAmount, monsterStatics, random);

			Monster[] generatedMonsters = new Monster[monstersAmount];

			for (int i = 0; i < monstersAmount; i++)
			{
				MonsterStatic monsterStatic = staticsToGenerate[i];
				int randomLevel = random.Next(1, (int)(generationSettings.MaxMonstersLevel + 1));
				Monster generatedMonster = new Monster(monsterStatic.Id, randomLevel);
				userMonsters.Monsters.Add(generatedMonster);

				generatedMonsters[i] = generatedMonster;
			}

			MonsterDto[] monsterDtos = new MonsterDto[monstersAmount];
			for (int i = 0; i < monstersAmount; i++)
			{
				monsterDtos[i] = generatedMonsters[i].Serialize();
			}
			ConstArray<MonsterDto> constArrayDtos = new ConstArray<MonsterDto>(monsterDtos);

			return new GenerateMonsters.Response()
			{
					MonstersDto = constArrayDtos
			};
		}

		private List<MonsterStatic> GetRandomMonsterStatics(int monstersAmount,
				MonsterStatic[] allStatics,
				Random random)
		{
			List<MonsterStatic> monsterStatics = new List<MonsterStatic>(monstersAmount);

			while (monsterStatics.Count < monstersAmount)
			{
				MonsterStatic monsterStatic = allStatics.GetRandom(random);
				if (monsterStatics.Contains(monsterStatic))
				{
					continue;
				}
				monsterStatics.Add(monsterStatic);
			}

			return monsterStatics;
		}

#else
        private int _monstersAmount;

        public GenerateMonstersCommand(int monstersAmount)
        {
            _monstersAmount = monstersAmount;
        }

        protected override GenerateMonsters.Request GetRequest()
        {
            return new GenerateMonsters.Request()
            {
                MonstersAmount = _monstersAmount
            };
        }

        protected override void OnResponse(GenerateMonsters.Response response)
        {
            ConstArray<MonsterDto> monsterDtos = response.MonstersDto;
            UserMonsters userMonsters = Get<UserMonsters>();
            userMonsters.Monsters.Clear();
            foreach (MonsterDto monsterDto in monsterDtos)
            {
                Log.Info($"Create monster {monsterDto.StaticId}");
                userMonsters.Monsters.Add(new Monster(monsterDto.StaticId, monsterDto.Level));
            }
        }

#endif
	}
}