﻿using Framework.RPC;

#if NOT_UNITY3D
using SAS.Model;
using System.Collections.Generic;
using Framework.Collections;
#endif


namespace GameShared
{
	public class GetBossRatingCommand : RpcCommand<GetBossRatings.Request, GetBossRatings.Response>
	{
#if NOT_UNITY3D
		protected override GetBossRatings.Response Execute(GetBossRatings.Request request)
		{
			IBossStorage bossStorage = Get<IBossStorage>();
			const int maxCount = 100;
			BossModel bossModel = bossStorage.GetBoss(request.BossId);
			List<BossUserRating> bossModels = bossStorage.GetBossRating(bossModel, maxCount);

			string userGuid = UserContext.Guid;
			BossPlayerRating bossPlayerRating = new BossPlayerRating();
			for (int i = 0; i < bossModels.Count; i++)
			{
				BossUserRating bossUserRating = bossModels[i];
				if (bossUserRating.UserGuid == userGuid)
				{
					bossPlayerRating = new BossPlayerRating
					{
							Place = (uint)i,
							UserGuid = userGuid,
							Damage = bossUserRating.Damage,
							ExpectedEssence = bossUserRating.ExpectedEssence,
							IsInRating = true
					};
				}
			}
			if (!bossPlayerRating.IsInRating)
			{
				bossPlayerRating = bossStorage.GetBossPlayerRating(bossModel, (long)UserContext.Id, userGuid);
			}

			return new GetBossRatings.Response
			{
					TopHundred = new ConstArray<BossUserRating>(bossModels.ToArray()),
					UserRating = bossPlayerRating
			};
		}
#else
		private uint _bossId;

		public GetBossRatingCommand(uint bossId)
		{
			_bossId = bossId;
		}

		protected override GetBossRatings.Request GetRequest()
		{
			return new GetBossRatings.Request { BossId = _bossId };
		}

		protected override void OnResponse(GetBossRatings.Response response)
		{
		}
#endif
	}
}