using Framework.FixedMath;
using Framework.Reactive;
using Framework.RPC;
using SAS.Model;

namespace GameShared
{
	public class DebugAddTokensCommand : RpcCommand<DebugAddTokens.Request, EmptyRpc.Response>
	{
#if !NOT_UNITY3D
		private readonly long Amount;

		public DebugAddTokensCommand(long amount)
		{
			Amount = amount;
		}

		protected override DebugAddTokens.Request GetRequest()
		{
			return new DebugAddTokens.Request()
			{
					Amount = Amount
			};
		}

		protected override void OnResponse(EmptyRpc.Response response)
		{
			Apply((Fix64)Amount);
		}
#else
		protected override EmptyRpc.Response Execute(DebugAddTokens.Request request)
		{
			Apply((Fix64)request.Amount);
			return EmptyRpc.Response.Instance;
		}
#endif

		private void Apply(Fix64 addOrRemove)
		{
			Field<Fix64> tokensAvailable = Get<UserChest>().TokensAvailable;
			Fix64 requestAmount = addOrRemove;
			if (addOrRemove < Fix64.Zero)
			{
				if (tokensAvailable.Value + requestAmount >= Fix64.Zero)
				{
					tokensAvailable.Value += requestAmount;
				}
				else
				{
					tokensAvailable.Value = Fix64.Zero;
				}
			}
			else
			{
				tokensAvailable.Value += requestAmount;
			}
		}
	}
}