﻿using System.Linq;
using Framework;
using Framework.Collections;
using Framework.Model;
using Framework.RPC;
using SAS.Model;
using SAS.Model.Time;
using SAS.Model.Battle;

namespace GameShared
{
#if NOT_UNITY3D
	public partial class AttackBossCommand
	{
		private readonly object _lock = new object();

		public AttackBossCommand()
		{
		}

		private static void ValidateCharactersCanAttack(Character[] characters)
		{
			foreach (Character character in characters)
			{
				if (character == null)
				{
					throw new ValidateException("Character to attack is null");
				}

				if (character.Availability.Value == CharacterAvailability.InStocks)
				{
					throw new ValidateException("Character to attack is unavailable");
				}

				TimePoint fightTime = TimePoint.Now;
				if (character.NextFightSeries.Value > fightTime && character.AvailableFightsCount.Value == 0)
				{
					throw new ValidateException("Character fight count in current cooldown equals zero");
				}
			}
		}

		private void ValidateCanAttackBoss(Character[] characters, BossModel bossModel, BossStatic bossStatic)
		{
			UserItems userItems = Get<UserItems>();

			Item ticket = userItems.GetItem(ItemType.Ticket);
			if (ticket.Count < bossStatic.TicketsCost)
			{
				throw new ValidateException("No ticket!");
			}

			ValidateCharactersCanAttack(characters);

			Element bossElement = (Element)bossModel.Element;
			if (bossElement != Element.Undefined)
			{
				if (characters.FirstOrDefault(x => ElementUtils.GetOppositeElement(bossElement) == x.Element) == null)
				{
					throw new ValidateException($"No character with element '{bossElement}'");
				}
			}

			if (bossStatic.MinCharactersInGroupCriterion > 0)
			{
				if (characters.Length < bossStatic.MinCharactersInGroupCriterion)
				{
					throw new ValidateException($"Not enough characters in group");
				}
			}

			if (bossStatic.CharactersRarity != CharacterRarity.Undefined)
			{
				if (characters.FirstOrDefault(character => character.Rarity >= bossStatic.CharactersRarity) == null)
				{
					throw new ValidateException($"No character with rarity '{bossStatic.CharactersRarity}'");
				}
			}
		}

		private float GetGroupSynergyMultiplier(Character[] characters, BossSettings bossSettings)
		{
			int maxGroup = 1;

			for (Element element = Element.Air; element <= Element.Water; element++)
			{
				int currentGroup = 0;
				foreach (Character character in characters)
				{
					if (character.Element == element)
					{
						currentGroup++;
					}
				}
				if (currentGroup > maxGroup)
				{
					maxGroup = currentGroup;
				}
			}
			return bossSettings.UniteDamageMultipliers[maxGroup - 1];
		}

		private float GetGroupLuckMultiplier(Character[] characters)
		{
			float maxLuck = 0;

			foreach (Character character in characters)
			{
				if (character.LuckMultiplier > maxLuck)
				{
					maxLuck = character.LuckMultiplier;
				}
			}
			return maxLuck;
		}

		private long[] AttackBoss(Character[] characters, BossModel bossModel, BossStatic bossStatic)
		{
			var bossSettings = Statics.GetSingle<BossSettings>();

			float groupSynergyMultiplier = GetGroupSynergyMultiplier(characters, bossSettings) / 1000f;
			float groupLuckMultiplier = GetGroupLuckMultiplier(characters) / 1000f;

			long groupDamage = 0;
			long[] attackersDamage = new long[characters.Length];
			int nextIndex = 0;
			foreach (Character character in characters)
			{
				float rarityBonus = bossSettings.RarityBonuses[(int)character.Rarity - 1] / 1000f;
				float battleBonus = character.Static.Attack + character.Static.Defence / 100f;

				float elementMultiplier = 1.0f +
						ElementUtils.CompareElement(character.Element, (Element)bossModel.Element) *
						(bossSettings.BossElementModificator / 1000f);

				long characterDamage =
						(long)((character.Level + bossSettings.MinimumCharacterToBossDamage) *
								(rarityBonus + battleBonus) * groupLuckMultiplier * elementMultiplier);
				attackersDamage[nextIndex++] = characterDamage;

				Log.Info($"Character {character.Id} damage = {characterDamage}");

				groupDamage += characterDamage;
			}

			Log.Info($"Group damage = {groupDamage}");

			long finalGroupDamage = (long)(groupDamage * groupSynergyMultiplier);

			for (int i = 0; i < attackersDamage.Length; i++)
			{
				attackersDamage[i] = (long)(attackersDamage[i] * groupSynergyMultiplier);
			}
			Get<IBossStorage>().UpdateBoss(bossModel.Id, bossStatic.Id, (long)UserContext.Id, UserContext.Guid, finalGroupDamage);

			Log.Info($"Final group damage = {finalGroupDamage}");
			bossModel.HealthPoints -= finalGroupDamage;
			return attackersDamage;
		}

		protected override AttackBoss.Response Execute(AttackBoss.Request request)
		{
			UserCharacters userCharacters = Get<UserCharacters>();
			Character[] characters = userCharacters.GetMultiple(request.AttackerCharacterIds.ToArray());
			if (characters == null)
			{
				throw new ValidateException("Characters to attack is null");
			}

			IBossStorage bossStorage = Get<IBossStorage>();
			lock (_lock)
			{
				BossModel boss = bossStorage.GetAliveBosses().FirstOrDefault(x => x.Id == request.BossIdToAttack);

				if (boss == null)
				{
					throw new ValidateException("Boss to attack is null");
				}

				BossStatic bossStatic = Statics.Get<BossStatic>(boss.StaticId);

				if (!Get<UserItems>().HasPrice(new ItemPair(ItemType.Ticket, bossStatic.TicketsCost)))
				{
					throw new ValidateException("User have no tickets");
				}

				ValidateCanAttackBoss(characters, boss, bossStatic);
				long bossStartHp = boss.HealthPoints;
				long[] attackersDamage = AttackBoss(characters, boss, bossStatic);
				long bossEndHp = boss.HealthPoints;
				FightBattle(characters, bossStatic.Id);

				return new AttackBoss.Response
				{
						AttackersDamage = new ConstArray<long>(attackersDamage),
						BossEndHp = bossEndHp,
						BossStartHp = bossStartHp,
						BossStaticId = boss.StaticId
				};
			}
		}
	}
#endif

	public partial class AttackBossCommand : RpcCommand<AttackBoss.Request, AttackBoss.Response>
	{
#if !NOT_UNITY3D
		private long[] _attackerCharacters;
		private long _bossId;

		public AttackBossCommand(Character[] attackerCharacters, ClientBoss bossToAttack)
		{
			_attackerCharacters = attackerCharacters.Select(x => x.Id).ToArray();
			_bossId = bossToAttack.Id;
		}

		protected override AttackBoss.Request GetRequest()
		{
			return new AttackBoss.Request()
			{
					AttackerCharacterIds = new ConstArray<long>(_attackerCharacters),
					BossIdToAttack = _bossId
			};
		}

		protected override void OnResponse(AttackBoss.Response response)
		{
			UserCharacters userCharacters = Get<UserCharacters>();
			FightBattle(userCharacters.GetMultiple(_attackerCharacters), response.BossStaticId);
		}

#endif

		private void FightBattle(Character[] attackerCharacters, int bossStaticId)
		{
			TimePoint fightTime = TimePoint.Now;

			CharacterSharedStatic sharedStatic = Statics.GetSingle<CharacterSharedStatic>();

			foreach (var attackerCharacter in attackerCharacters)
			{
				if (attackerCharacter.NextFightSeries.Value <= fightTime)
				{
					CharactersFightSeriesSettings fightSeriesSettings =
							Statics.GetSingle<CharactersFightSeriesSettings>();
					uint fightCooldown = fightSeriesSettings.Cooldown;
					attackerCharacter.NextFightSeries.Value = fightTime + TimeDuration.CreateBySeconds(fightCooldown);
					attackerCharacter.AvailableFightsCount.Value = (int)sharedStatic.AvailableFightsCount[(int)attackerCharacter.Rarity - 1];
				}
				attackerCharacter.AvailableFightsCount.Value -= 1;
			}
			BossStatic bossStatic = Statics.Get<BossStatic>(bossStaticId);
			Get<UserItems>().RemoveItem(ItemType.Ticket, bossStatic.TicketsCost);
		}
	}
}