﻿using System;
using Framework;
using Framework.FixedMath;
using Framework.Model;
using Framework.RPC;
using SAS.Model;
using SAS.Model.Shop;

namespace GameShared
{
	public class BuyShopPositionCommand : RpcCommand<BuyShopPosition.Request, BuyShopPosition.Response>
	{
		
#if NOT_UNITY3D
		protected override BuyShopPosition.Response Execute(BuyShopPosition.Request request)
		{
			ShopPositionStatic positionStatic = Statics.Get<ShopPositionStatic>(request.ShopPositionId);
			uint priceInTokens = positionStatic.PriceInTokens * request.PositionsAmount;
			UserChest userChest = Get<UserChest>();
			if (userChest.TokensAvailable.Value < (Fix64)priceInTokens)
			{
				throw new ValidateException("User tokens amount less than price of shop position");
			}
			BuyShopPosition(request.ShopPositionId, request.PositionsAmount);
			return new BuyShopPosition.Response()
			{
					ShopPositionId = request.ShopPositionId,
					PositionsAmount = request.PositionsAmount
			};
		}

#else
		private int _shopPositionsId;

		private uint _positionsAmount;

		public BuyShopPositionCommand(int shopPositionsId, uint positionsAmount)
		{
			_shopPositionsId = shopPositionsId;
			_positionsAmount = positionsAmount;
		}

		protected override BuyShopPosition.Request GetRequest()
		{
			return new BuyShopPosition.Request()
			{
					ShopPositionId = _shopPositionsId,
					PositionsAmount = _positionsAmount
			};
		}

		protected override void OnResponse(BuyShopPosition.Response response)
		{
			BuyShopPosition(response.ShopPositionId, response.PositionsAmount);
		}

#endif

		private void BuyShopPosition(int shopPositionId, uint positionsAmount)
		{
			UserItems userItems = Get<UserItems>();
			ShopPositionStatic positionStatic = Statics.Get<ShopPositionStatic>(shopPositionId);
			uint priceInTokens = positionStatic.PriceInTokens * positionsAmount;
			UserChest userChest = Get<UserChest>();
			userChest.TokensAvailable.Value -= (Fix64)priceInTokens;
			userItems.AddReward(new ItemPair(positionStatic.Item.Id, positionStatic.Item.Count * positionsAmount));
		}
	}
}