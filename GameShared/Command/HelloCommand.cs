#if NOT_UNITY3D
using Framework.Model;
#endif
using Framework.RPC;
using SAS.Model;

namespace GameShared
{
	public class HelloCommand : RpcCommand<Hello.Request, Hello.Response>
	{
#if NOT_UNITY3D

		protected override Hello.Response Execute(Hello.Request request)
		{
			TestModel testModel = Get<TestModel>();
			Hello.Response response = new Hello.Response
			{
					Message = $"Hello user № {UserContext.Id} name '{UserContext.Container.Resolve<string>()}' description '{testModel.Desctiption}' storedNumber = {testModel.TestModelVariable} msg = {request.Number}!"
			};
			testModel.TestModelVariable = request.Number;
			return response;
		}
#else
		private readonly int _number;

		public HelloCommand(int number)
		{
			_number = number;
		}

		protected override Hello.Request GetRequest()
		{
			return new Hello.Request()
			{
					Number = _number
			};
		}

		protected override void OnResponse(Hello.Response response)
		{
			Framework.Log.Info(response.Message);
		}

#endif
	}
}