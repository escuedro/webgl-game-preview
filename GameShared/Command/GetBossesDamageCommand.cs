using Framework.RPC;
using SAS.Model;

#if NOT_UNITY3D
using System.Collections.Generic;
using Framework.Collections;
#endif


namespace GameShared
{
	public class GetBossesDamageCommand : RpcCommand<EmptyRpc.Request, GetBossesDamage.Response>
	{
#if NOT_UNITY3D
		protected override GetBossesDamage.Response Execute(EmptyRpc.Request request)
		{
			IBossStorage bossStorage = Get<IBossStorage>();
			List<BossAttackerModel> bossModels = bossStorage.GetUserAttackedBosses((long)UserContext.Id);

			BossDamageDto[] bossDtos = new BossDamageDto[bossModels.Count];

			for (int i = 0; i < bossModels.Count; ++i)
			{
				BossAttackerModel bossModel = bossModels[i];
				bossDtos[i] = new BossDamageDto
				{
						BossId = bossModel.BossId,
						StaticId = bossModel.StaticId,
						Damage = bossModel.Damage
				};
			}

			return new GetBossesDamage.Response
			{
					BossDamageDtos = new ConstArray<BossDamageDto>(bossDtos)
			};
		}
#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(GetBossesDamage.Response response)
		{
		}
#endif
	}
}