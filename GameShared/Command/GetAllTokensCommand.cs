﻿using Framework;
using Framework.FixedMath;
using Framework.RPC;
using SAS.Model;

namespace GameShared
{
	public class GetAllTokensCommand : RpcCommand<EmptyRpc.Request, EmptyRpc.Response>
	{
#if NOT_UNITY3D
		protected override EmptyRpc.Response Execute(EmptyRpc.Request request)
		{
			UserChest userChest = Get<UserChest>();
			if (userChest.GetAllTokens() == Fix64.Zero)
			{
				throw new ValidateException("Tokens are not exist");
			}

			ConsumeAllTokens();
			return EmptyRpc.Response.Instance;
		}
#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(EmptyRpc.Response response)
		{
			ConsumeAllTokens();
		}
#endif
		private void ConsumeAllTokens()
		{
			UserChest userChest = Get<UserChest>();
			userChest.ApplyAllTokenRewards();
		}
	}
}