﻿using System;
using Framework;
using Framework.Model;
using Framework.RandomUtils;
#if NOT_UNITY3D
using Framework.Model.IdentityGenerator;
using Framework.Server.Marketplace;
#endif
using Framework.RPC;
using SAS.Model;
using SAS.Model.Battle;
using SAS.Model.Time;

namespace GameShared
{
	public class TransmutateCommand : RpcCommand<Transmutate.Request, Transmutate.Response>
	{
#if NOT_UNITY3D
		protected override Transmutate.Response Execute(Transmutate.Request request)
		{
			if (request.CharacterId1 == request.CharacterId2)
			{
				throw new ValidateException("Character ids are same!");
			}

			UserCharacters userCharacters = Get<UserCharacters>();
			Character character1 = userCharacters.TryGet(request.CharacterId1);
			if (character1 == null)
			{
				throw new ValidateException("Character1 is not exists!");
			}
			Character character2 = userCharacters.TryGet(request.CharacterId2);
			if (character2 == null)
			{
				throw new ValidateException("Character2 is not exists!");
			}
			if (character1.Static.Id != character2.Static.Id)
			{
				throw new ValidateException("Statics are not same!");
			}
			if (character1.Rarity != character2.Rarity)
			{
				throw new ValidateException("Rarity are not same!");
			}
			if (character1.IsMaximumRarity || character2.IsMaximumRarity)
			{
				throw new ValidateException("One of selected characters rarity is max!");
			}
			if (character1.Availability.Value == CharacterAvailability.InStocks ||
					character2.Availability.Value == CharacterAvailability.InStocks)
			{
				throw new ValidateException("One of selected characters is in stocks!");
			}

			UserItems userItems = Get<UserItems>();
			Item folliant = userItems.GetItem(ItemType.Folliant);
			if (folliant.Count <= 0)
			{
				throw new ValidateException("No folliant!");
			}

			if (request.UseElixir)
			{
				Item elixir = userItems.GetItem(ItemType.Elixir);
				if (elixir.Count <= 0)
				{
					throw new ValidateException("No elixir!");
				}
			}

			TransmutationSettings transmutationSettings = Statics.GetSingle<TransmutationSettings>();
			Random random = new Random((int)TimePoint.Now.Seconds);

			bool success;
			bool removeCharacter1;
			bool removeCharacter2;
			Character newChar = null;

			if (request.UseElixir)
			{
				success = true;
			}
			else
			{
				success = random.TrySuccess(transmutationSettings.SuccessTransmutateChance);
			}


			if (success)
			{
				const string identityName = "character";
				ulong id = Get<LocalIdentityGenerator>().GetIdentity(identityName);
				CharacterRarity rarity = character1.Rarity + 1;
				Element element = character1.Static.Element;
				Guid characterGuid = Get<IMarketplaceGuidResolver>().CreateGuid(id, identityName, UserContext.Id);
				newChar = new Character(character1.Static.Id, (long)id, characterGuid,
						0, 1, rarity, element, (int)rarity, TimePoint.Now,
						CharacterAvailability.Available);
				removeCharacter1 = true;
				removeCharacter2 = true;
			}
			else
			{
				//TODO: if death chance < 50% - character from second position died more times and vise versa. ask gd
				removeCharacter1 = random.TrySuccess(transmutationSettings.CharacterDeathChance);
				removeCharacter2 = random.TrySuccess(transmutationSettings.CharacterDeathChance);
			}

			Transmutate(request.UseElixir, newChar, new RemoveCharacterData(removeCharacter1 ? character1.Id : 0),
					new RemoveCharacterData(removeCharacter2 ? character2.Id : 0));

			return new Transmutate.Response
			{
					RemoveElixir = request.UseElixir,
					Success = success, RemoveCharacter1 = removeCharacter1, RemoveCharacter2 = removeCharacter2,
					Character = newChar?.Serialize() ?? default
			};
		}

#else
		private bool _useElixir;
		private long _characterId1;
		private long _characterId2;

		public TransmutateCommand(bool useElixir, long characterId1, long characterId2)
		{
			_useElixir = useElixir;
			_characterId1 = characterId1;
			_characterId2 = characterId2;
		}

		protected override Transmutate.Request GetRequest()
		{
			return new Transmutate.Request
					{UseElixir = _useElixir, CharacterId1 = _characterId1, CharacterId2 = _characterId2};
		}

		protected override void OnResponse(Transmutate.Response response)
		{
			Character character = null;
			if (response.Success)
			{
				CharacterDto dto = response.Character;
				character = new Character(dto.StaticId, dto.Id, Guid.Parse(dto.Guid),  dto.Experience, dto.Level,
						dto.Rarity, dto.Element, dto.AvailableFightsCount,
						TimePoint.CreateBySeconds(dto.NextFightSeriesSeconds), dto.Availability);
			}

			Transmutate(response.RemoveElixir, character,
					new RemoveCharacterData(response.RemoveCharacter1 ? _characterId1 : 0),
					new RemoveCharacterData(response.RemoveCharacter2 ? _characterId2 : 0));
		}
#endif
		private void Transmutate(bool removeElixir,
				[CanBeNull] Character newCharacter,
				RemoveCharacterData removeCharacter1,
				RemoveCharacterData removeCharacter2)
		{
			UserItems userItems = Get<UserItems>();
			Item folliant = userItems.GetItem(ItemType.Folliant);
			folliant.Count.Value -= 1;
			if (removeElixir)
			{
				Item elixir = userItems.GetItem(ItemType.Elixir);
				elixir.Count.Value -= 1;
			}

			UserCharacters userCharacters = Get<UserCharacters>();
			if (removeCharacter1.CharacterId != 0)
			{
				Character character1 = userCharacters.TryGet(removeCharacter1.CharacterId);
				userCharacters.Characters.Remove(character1);
			}
			if (removeCharacter2.CharacterId != 0)
			{
				Character character2 = userCharacters.TryGet(removeCharacter2.CharacterId);
				userCharacters.Characters.Remove(character2);
			}
			if (newCharacter != null)
			{
				userCharacters.Characters.Add(newCharacter);
			}
		}

		struct RemoveCharacterData
		{
			/// <param name="characterId">If not 0 - character with specified id should be removed</param>
			public RemoveCharacterData(long characterId)
			{
				CharacterId = characterId;
			}

			public long CharacterId;
		}
	}
}