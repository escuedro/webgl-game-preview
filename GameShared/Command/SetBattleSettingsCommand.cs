﻿using Framework.RPC;
using SAS.Model.User;

namespace GameShared
{
	public class SetBattleSettingsCommand : RpcCommand<SetBattleSettings.Request, EmptyRpc.Response>
	{
#if NOT_UNITY3D
		protected override EmptyRpc.Response Execute(SetBattleSettings.Request request)
		{
			SetSettings(request.SkipBattleAnimation);

			return EmptyRpc.Response.Instance;
		}

#else
		private bool _skipBattleAnimation;

		public SetBattleSettingsCommand(bool skipBattleAnimation)
		{
			_skipBattleAnimation = skipBattleAnimation;
		}

		protected override SetBattleSettings.Request GetRequest()
		{
			return new SetBattleSettings.Request()
			{
					SkipBattleAnimation = _skipBattleAnimation
			};
		}

		protected override void OnResponse(EmptyRpc.Response response)
		{
			SetSettings(_skipBattleAnimation);
		}
#endif
		private void SetSettings(bool skipBattleAnimation)
		{
			Get<UserSettings>().SkipBattleAnimation.Value = skipBattleAnimation;
		}
	}
}