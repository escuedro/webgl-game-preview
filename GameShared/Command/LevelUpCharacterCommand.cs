#if NOT_UNITY3D
using Framework;
#endif
using Framework.FixedMath;
using Framework.RPC;
using SAS.Model;

namespace GameShared
{
	public class LevelUpCharacterCommand : RpcCommand<LevelUpCharacter.Request, EmptyRpc.Response>
	{
#if NOT_UNITY3D
		protected override EmptyRpc.Response Execute(LevelUpCharacter.Request request)
		{
			Character character = Get<UserCharacters>().TryGet(request.Id);
			if (character == null)
			{
				throw new ValidateException($"User have no character {request.Id}");
			}
			if (character.Availability == CharacterAvailability.InStocks)
			{
				throw new ValidateException($"Character {request.Id} is in stocks");
			}
			if (character.IsMaximumLevel)
			{
				throw new ValidateException($"Character {request.Id} has maximum level");
			}
			CharacterLevelSharedStatic levelStatic = character.LevelStatic;

			if (levelStatic.ExperienceToNextLevel > character.CurrentExperience)
			{
				throw new ValidateException($"Character {request.Id} experience is not enough");
			}

			UserChest userChest = Get<UserChest>();
			if (userChest.TokensAvailable < (Fix64)levelStatic.TokensToNexLevel)
			{
				throw new ValidateException("Not enough tokens");
			}

			Apply(userChest, character);
			return EmptyRpc.Response.Instance;
		}
#else
		private readonly Character _character;

		public LevelUpCharacterCommand(Character character)
		{
			_character = character;
		}

		protected override LevelUpCharacter.Request GetRequest()
		{
			return new LevelUpCharacter.Request { Id = _character.Id };
		}

		protected override void OnResponse(EmptyRpc.Response response)
		{
			Apply(Get<UserChest>(), _character);
		}
#endif

		private void Apply(UserChest userChest, Character character)
		{
			CharacterLevelSharedStatic levelStatic = character.LevelStatic;
			userChest.TokensAvailable.Value -= (Fix64)levelStatic.TokensToNexLevel;
			character.CurrentExperience.Value = 0;
			character.Level.Value += 1;
		}
	}
}