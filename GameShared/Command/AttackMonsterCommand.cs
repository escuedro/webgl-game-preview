﻿using System;
using Framework;
using Framework.Collections;
using Framework.Model;
using Framework.RandomUtils;
using Framework.RPC;
using SAS.Model;
using SAS.Model.Battle;
using SAS.Model.History;
using SAS.Model.Time;

namespace GameShared
{
	public class AttackMonsterCommand : RpcCommand<AttackMonster.Request, AttackMonster.Response>
	{
#if NOT_UNITY3D
		protected override AttackMonster.Response Execute(AttackMonster.Request request)
		{
			UserCharacters userCharacters = Get<UserCharacters>();
			Character character = userCharacters.TryGet(request.AttackerCharacterId);
			if (character == null)
			{
				throw new ValidateException("Character to attack is null");
			}

			if (character.Availability.Value == CharacterAvailability.InStocks)
			{
				throw new ValidateException("Character to attack is unavailable");
			}

			TimePoint fightTime = TimePoint.Now;
			if (character.NextFightSeries.Value > fightTime && character.AvailableFightsCount.Value == 0)
			{
				throw new ValidateException("Character fight count in current cooldown equals zero");
			}

			Random random = new Random((int)fightTime.Seconds);
			Monster monsterToAttack = new Monster(request.MonsterToAttack.StaticId, request.MonsterToAttack.Level);
			bool win = random.TrySuccess(monsterToAttack.WinChance);

			BattleResult battleResult = new BattleResult()
			{
					ResultType = win ? BattleResultType.Win : BattleResultType.Lose,
					AttackerCharacterId = request.AttackerCharacterId,
					AttackedMonster = request.MonsterToAttack
			};
			long experience = win ? monsterToAttack.ExperienceForWin : monsterToAttack.ExperienceForLose;
			uint essence = win ? monsterToAttack.EssenceForWin : 0;


			bool giveTicket = random.TrySuccess(monsterToAttack.TicketRewardChance);
			ItemPair[] itemReward;
			if (giveTicket && win)
			{
				itemReward = new[]
				{
						new ItemPair {Id = ItemConstants.Ticket.Id, Count = 1}
				};
			}
			else
			{
				itemReward = Array.Empty<ItemPair>();
			}
			FightBattle(character, request.MonsterToAttack, win, experience, essence, itemReward);

			return new AttackMonster.Response
			{
					FightTime = fightTime.Seconds,
					BattleResult = battleResult,
					Essence = essence,
					Experience = experience,
					ItemReward = new ConstArray<ItemPair>(itemReward)
			};
		}


#else
		private long _attackerCharacter;
		private MonsterDto _monsterToAttack;

		public AttackMonsterCommand(Character attackerCharacter, Monster monsterToAttack)
		{
			_attackerCharacter = attackerCharacter.Id;
			_monsterToAttack = monsterToAttack.Serialize();
		}

		protected override AttackMonster.Request GetRequest()
		{
			return new AttackMonster.Request()
			{
					AttackerCharacterId = _attackerCharacter,
					MonsterToAttack = _monsterToAttack
			};
		}

		protected override void OnResponse(AttackMonster.Response response)
		{
			UserCharacters userCharacters = Get<UserCharacters>();
			FightBattle(userCharacters.TryGet(response.BattleResult.AttackerCharacterId),
					response.BattleResult.AttackedMonster,
					response.BattleResult.ResultType == BattleResultType.Win, response.Experience, response.Essence,
					response.ItemReward.ToArray());
		}

#endif

		private void FightBattle(Character attackerCharacter,
				MonsterDto monsterToAttack,
				bool win,
				long experience,
				uint essenceCount,
				ItemPair[] itemReward)
		{
			TimePoint fightTime = TimePoint.Now;
			UserHistory userHistory = Get<UserHistory>();
			userHistory.AddRecord(attackerCharacter,
					win ? BattleResultType.Win : BattleResultType.Lose, fightTime, essenceCount, experience);

			CharacterSharedStatic sharedStatic = Statics.GetSingle<CharacterSharedStatic>();

			if (attackerCharacter.NextFightSeries.Value <= fightTime)
			{
				CharactersFightSeriesSettings fightSeriesSettings = Statics.GetSingle<CharactersFightSeriesSettings>();
				uint fightCooldown = fightSeriesSettings.Cooldown;
				attackerCharacter.NextFightSeries.Value = fightTime + TimeDuration.CreateBySeconds(fightCooldown);
				attackerCharacter.AvailableFightsCount.Value = (int)sharedStatic.AvailableFightsCount[(int)attackerCharacter.Rarity - 1];
			}
			attackerCharacter.AvailableFightsCount.Value -= 1;
			if (win)
			{
				UserMonsters userMonsters = Get<UserMonsters>();
				userMonsters.Remove(monsterToAttack);
			}
			attackerCharacter.AddExperience(experience);
			if (essenceCount > 0)
			{
				Get<UserChest>().AddEssence(new ChestRewardTime(fightTime), essenceCount);
			}
			if (itemReward.Length > 0)
			{
				UserItems userItems = Get<UserItems>();
				foreach (ItemPair itemPair in itemReward)
				{
					userItems.AddReward(itemPair);
				}
			}
		}
	}
}