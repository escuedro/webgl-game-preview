using System;
using Framework;
using Framework.RPC;
using SAS.Model;
using SAS.Model.Time;

#if NOT_UNITY3D
using Framework.Model;
using Framework.Model.IdentityGenerator;
using Framework.RandomUtils;
using Framework.Server.Marketplace;
#endif

namespace GameShared
{
	public class CreateCharacterCommand : RpcCommand<EmptyRpc.Request, CreateCharacter.Response>
	{
#if NOT_UNITY3D
		protected override CreateCharacter.Response Execute(EmptyRpc.Request request)
		{
			//TODO write logic
			//TODO id counter per user

			UserItems userItems = Get<UserItems>();
			if (userItems.GetItem(ItemType.Contract).Count.Value == 0)
			{
				throw new ValidateException("User contract item count is zero");
			}

			CharacterGenerationChance[] characterGenerationChances = Statics.GetAll<CharacterGenerationChance>();
			CharacterRarityGenerationChance[] characterRarityGenerationChances =
					Statics.GetAll<CharacterRarityGenerationChance>();

			const string identityName = "character";
			ulong identity = Get<LocalIdentityGenerator>().GetIdentity(identityName);
			Guid characterGuid = Get<IMarketplaceGuidResolver>().CreateGuid(identity, identityName, UserContext.Id);

			WeightedRandom weightedRandom = new WeightedRandom();
			CharacterGenerationChance characterGenerationChance =
					weightedRandom.GetRandomWeightedElement(characterGenerationChances);
			CharacterRarityGenerationChance characterRarityGenerationChance =
					weightedRandom.GetRandomWeightedElement(characterRarityGenerationChances);

			CharacterStatic characterStatic = Statics.Get<CharacterStatic>(characterGenerationChance.Id);
			CharacterRarity rarity = characterRarityGenerationChance.Rarity;

			Character character = new Character(characterStatic.Id, (long)identity, characterGuid, 0, 1, rarity,
					characterStatic.Element, (int)rarity, TimePoint.Now,
					CharacterAvailability.Available);

			CreateCharacter(character);
			return new CreateCharacter.Response()
			{
					CharacterDto = character.Serialize()
			};
		}

#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(CreateCharacter.Response response)
		{
			CharacterDto dto = response.CharacterDto;
			Log.Info($"Created character {dto.Id}");
			Character character = new Character(dto.StaticId, dto.Id, Guid.Parse(dto.Guid), dto.Experience, dto.Level,
					dto.Rarity, dto.Element, dto.AvailableFightsCount,
					TimePoint.CreateBySeconds(dto.NextFightSeriesSeconds), dto.Availability);
			CreateCharacter(character);
		}
#endif

		private void CreateCharacter(Character character)
		{
			UserItems userItems = Get<UserItems>();
			UserCharacters userCharacters = Get<UserCharacters>();
			userCharacters.Characters.Add(character);
			userItems.GetItem(ItemType.Contract).Count.Value -= 1;
		}
	}
}