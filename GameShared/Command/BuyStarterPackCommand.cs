#if NOT_UNITY3D
using Framework;
#endif
using SAS.Model.StarterPack;
using Framework.Collections;
using Framework.FixedMath;
using Framework.Model;
using Framework.RPC;
using SAS.Model;

namespace GameShared
{
	public class BuyStarterPackCommand : RpcCommand<EmptyRpc.Request, BuyStarterPack.Response>
	{
#if NOT_UNITY3D
		protected override BuyStarterPack.Response Execute(EmptyRpc.Request request)
		{
			UserStarterPack userStarterPack = Get<UserStarterPack>();
			StarterPackStatic starterPackStatic = Statics.GetSingle<StarterPackStatic>();
			UserChest userChest = Get<UserChest>();
			if (userChest.TokensAvailable.Value < (Fix64)starterPackStatic.PriceInTokens)
			{
				throw new ValidateException("User tokens amount less than price of starter pack");
			}

			ItemPair[] reward = userStarterPack.CountPurchased == 0 ? starterPackStatic.FirstBuy :
					starterPackStatic.SecondBuy;

			Apply(Get<UserItems>(), userChest, reward);
			return new BuyStarterPack.Response()
			{
					Reward = new ConstArray<ItemPair>(reward)
			};
		}
#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(BuyStarterPack.Response response)
		{
			Apply(Get<UserItems>(), Get<UserChest>(), response.Reward.ToArray());
		}
#endif

		private void Apply(UserItems userItems, UserChest userChest, ItemPair[] reward)
		{
			foreach (ItemPair itemPair in reward)
			{
				userItems.AddReward(itemPair);
			}
			userChest.TokensAvailable.Value -= (Fix64)Statics.GetSingle<StarterPackStatic>().PriceInTokens;
			Get<UserStarterPack>().CountPurchased.Value += 1;
		}
	}
}