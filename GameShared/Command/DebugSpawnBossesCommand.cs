using Framework.RPC;

#if NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Text.Json;
using Framework;
using Framework.Collections;
using Framework.Model;
using SAS.Model;
using SAS.Model.Time;
#endif

namespace GameShared
{
	public class DebugSpawnBossesCommand : RpcCommand<EmptyRpc.Request, EmptyRpc.Response>
	{
#if NOT_UNITY3D
		protected override EmptyRpc.Response Execute(EmptyRpc.Request request)
		{
			BossStatic[] bossStatics = Statics.GetAll<BossStatic>();

			List<BossModel> bossModels = GetRandomBossModels(1, bossStatics, new Random((int)TimePoint.Now.Seconds));

			foreach (var bossModel in bossModels)
			{
				Log.Info($"Boss generated: {JsonSerializer.Serialize(bossModel)}");
			}

			Get<IBossStorage>().InsertBosses(bossModels);
			return EmptyRpc.Response.Instance;
		}

		//Copy paste because debug only code
		private static List<BossModel> GetRandomBossModels(int bossesAmount, BossStatic[] allStatics, Random random)
		{
			TimePoint now = TimePoint.Now;

			List<BossModel> result = new List<BossModel>(bossesAmount);

			for (int i = 0; i < bossesAmount; ++i)
			{
				BossStatic bossStatic = allStatics.GetRandom(random);

				Element bossElement = bossStatic.Element;
				if (bossStatic.IsSuperBoss)
				{
					bossElement = (Element)random.Next((int)Element.Earth, (int)Element.Water + 1);
				}

				TimePoint deathTime = now + TimeDuration.CreateBySeconds(bossStatic.LifetimeSeconds);
				result.Add(new BossModel
				{
						StaticId = bossStatic.Id,
						HealthPoints = bossStatic.HealthPoints,
						DeathSeconds = (int)deathTime.Seconds,
						Element = (int)bossElement
				});
			}

			return result;
		}
#else
		protected override EmptyRpc.Request GetRequest()
		{
			return EmptyRpc.Request.Instance;
		}

		protected override void OnResponse(EmptyRpc.Response response)
		{

		}
#endif
	}
}