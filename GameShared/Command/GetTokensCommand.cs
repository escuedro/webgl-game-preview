﻿using Framework;
using Framework.FixedMath;
using Framework.RPC;
using SAS.Model;

namespace GameShared
{
	public class GetTokensCommand : RpcCommand<GetTokens.Request, GetTokens.Response>
	{
#if NOT_UNITY3D
		protected override GetTokens.Response Execute(GetTokens.Request request)
		{
			UserChest userChest = Get<UserChest>();
			if (userChest.GetTokensInTime(request.ChestRewardTime) == Fix64.Zero)
			{
				throw new ValidateException("Tokens in time specified is not exist");
			}

			ConsumeTokensInTime(request.ChestRewardTime);
			return new GetTokens.Response
			{
					ChestRewardTime = request.ChestRewardTime
			};
		}
#else
		private ChestRewardTime _chestRewardTime;

		public GetTokensCommand(ChestRewardTime chestRewardTime)
		{
			_chestRewardTime = chestRewardTime;
		}

		protected override GetTokens.Request GetRequest()
		{
			return new GetTokens.Request
			{
					ChestRewardTime = _chestRewardTime
			};
		}

		protected override void OnResponse(GetTokens.Response response)
		{
			ConsumeTokensInTime(response.ChestRewardTime);
		}
#endif
		private void ConsumeTokensInTime(ChestRewardTime chestRewardTime)
		{
			UserChest userChest = Get<UserChest>();
			userChest.ApplyTokenRewardInTime(chestRewardTime);
		}
	}
}