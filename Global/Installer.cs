using System.Collections.Generic;
using Framework;
using Framework.Audio;
using Framework.Generated;
using Framework.Serialization;
using Framework.Service;
using Framework.UI;
using Game.Analytics;
using Game.Service.Visibility;
using Game.View;
using Game.View.Audio;
using Game.View.Battle;
using Game.View.Boss;
using Game.View.Help;
using Game.View.Inventory;
using Game.View.Items;
using Game.View.Logout;
using Game.View.Shop;
using Game.View.StarterPack;
using Game.View.Transmutation;
using Game.Windows;
using GameShared.Static;
using UnityEngine;

namespace Game
{
	public class Installer : MonoBehaviour
	{
		[SerializeField]
		private WorldViewRoot _worldViewRoot;
		[SerializeField]
		private VisibilityListener _visibilityListener;

		private readonly Container _container = new Container();

		private void Awake()
		{
#if UNITY_EDITOR
			Application.targetFrameRate = 60;
#else
			Application.targetFrameRate = -1;
#endif
			new GeneratedBinder().Bind(_container);
			_container.Resolve<ClientLogger>();
			_container.BindInstance((IResolver)_container);
			_container.BindInstance((IInjector)_container);
			_container.BindInstance((IContainer)_container);
			GeneratedSerializer serializer = new GeneratedSerializer();
			_container.BindInstance((BaseSerializer)serializer);

			_container.BindInstance(_worldViewRoot);
			_container.BindInstance((IVisibilityListener)_visibilityListener);

			new ClientStaticLoader().Load(Resource.StaticJson);

#if DEVELOP
			_container.Resolve<Framework.RPC.IClientRpcController>().SetServerUrl("https://office.sofqgames.com:5000/");
#elif PRODUCTION
			_container.Resolve<Framework.RPC.IClientRpcController>().SetServerUrl("https://gameserver.eternalguardians.io:5000/");
#endif

			IResourceBinder binder = _container.Resolve<IResourceBinder>();
			BindUi(binder);
			BindWindows(binder);

			AudioBinder audioBinder = new AudioBinder(binder, _container.Resolve<IAudioManager>());
			audioBinder.BindAudio();

			_container.Resolve<ClientStartup>().Start();
		}

		private void BindWindows(IResourceBinder binder)
		{
			binder.Bind<FightResultsWindow>(Resource.Windows.FightResultsWindowPrefab);
			binder.Bind<CharacterInfoWindow>(Resource.Windows.CharacterInfoWindowPrefab);
			binder.Bind<MessagePopupWindow>(Resource.Windows.MessagePopupPrefab);
		}

		private void BindUi(IResourceBinder binder)
		{
			binder.Bind<FpsView>(Resource.View.Other.FpsPrefab);
			binder.Bind<TokensView>(Resource.View.Other.TokensPrefab);
			binder.Bind<MainUiView>(Resource.View.Root.MainUiCanvasPrefab);
			binder.Bind<LoadingView>(Resource.View.Loading.LoadingViewPrefab);
			binder.Bind<LogoutConfirmWindow>(Resource.Windows.LogoutConfirmWindowPrefab);
			binder.Bind<SettingsWindow>(Resource.Windows.SettingsWindowPrefab);

			binder.Bind<CharacterListItem>(Resource.View.Character.CharacterListItemPrefab);
			binder.Bind<CharacterListView>(Resource.View.Character.CharacterListViewPrefab);

			binder.Bind<WindowManagerRoot>(Resource.Config.WindowManagerRootPrefab);

			binder.Bind<StarterPackScreen>(Resource.Windows.StarterPackScreenPrefab);
			binder.Bind<NotEnoughMoneyWindow>(Resource.Windows.NotEnoughMoneyWindowPrefab);

			binder.Bind<TooltipManagerRoot>(Resource.Config.TooltipRootPrefab);
			binder.Bind<TooltipView>(Resource.View.Tooltip.TooltipViewPrefab);

			binder.Bind<TransmutationScreen>(Resource.Windows.TransmutationScreenPrefab);

			binder.Bind<InventoryScreen>(Resource.Windows.InventoryScreenPrefab);
			binder.Bind<InventoryPopup>(Resource.View.Character.Extra.InventoryPopupPrefab);
			binder.Bind<EssenceDescriptionWindow>(Resource.Windows.ItemDescriptionWindowPrefab);

			binder.Bind<CharacterLevelUpWindow>(Resource.Windows.CharacterLevelUpWindowPrefab);
			binder.Bind<CharacterSelectWindow>(Resource.Windows.SelectCharacterWindowPrefab);
			binder.Bind<SelectCharacterPopup>(Resource.View.Character.Extra.SelectCharacterPopupPrefab);
			binder.Bind<TransmutationConfirmWindow>(Resource.Windows.TransmutationConfirmWindowPrefab);
			binder.Bind<CharacterNextFightSeriesView>(Resource.View.Character.Extra.NextFightSeriesPrefab);

			binder.Bind<HistoryItemView>(Resource.View.History.HistoryItemViewPrefab);
			binder.Bind<HistoryWindow>(Resource.Windows.BattleHistoryWindowPrefab);

			binder.Bind<CheatsWidget>(Resource.View.Cheat.CheatsWidgetPrefab);
			binder.Bind<CheatsWindow>(Resource.View.Cheat.CheatsWindowPrefab);

			binder.Bind<MonsterListItem>(Resource.View.Monster.MonsterListItemPrefab);
			binder.Bind<MonsterListView>(Resource.View.Monster.MonsterListViewPrefab);

			binder.Bind<BattleScreen>(Resource.Windows.BattleScreenPrefab);
			binder.Bind<BattleVictoryView>(Resource.Windows.BattleVictoryViewPrefab);
			binder.Bind<BattleDefeatView>(Resource.Windows.BattleDefeatViewPrefab);
			binder.Bind<BattleCharactersView>(Resource.View.Battle.BattleCharactersViewPrefab);

			binder.Bind<BattleVsMonsterScreen>(Resource.Windows.MonsterBattleScreenPrefab);

			binder.Bind<BossBattleScreen>(Resource.Windows.BossBattleScreenPrefab);
			binder.Bind<BossView>(Resource.View.Boss.BossViewPrefab);
			binder.Bind<BossBattleResultView>(Resource.Windows.BossBattleResultsViewPrefab);
			binder.Bind<BossBattleCharactersView>(Resource.View.Battle.BossBattleCharactersViewPrefab);
			binder.Bind<BossBattleSceneScreen>(Resource.Windows.BossBattleSceneScreenPrefab);

			binder.Bind<BossBattleHistoryWindow>(Resource.Windows.BossBattleHistoryWindowPrefab);
			binder.Bind<BossRatingItemView>(Resource.View.History.BossHistorнItemViewPrefab);

			binder.Bind<WarningWindow>(Resource.View.Boss.BossWarningWindowPrefab);
			binder.Bind<BossWarningWindowElement>(Resource.View.Boss.BossWindowWarningElementPrefab);
			binder.Bind<NeedBuyCharacterWindow>(Resource.Windows.NeedBuyCharacterWindowPrefab);

			binder.Bind<FightButton>(Resource.View.Other.FightButtonPrefab);

			binder.Bind<ShopWindow>(Resource.Windows.ShopWindowPrefab);
			
			binder.Bind<AudioManagerRoot>(Resource.View.Root.AudioManagerRootPrefab);
		}
	}
}