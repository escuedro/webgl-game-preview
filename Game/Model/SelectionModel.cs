﻿using Framework;
using Framework.Reactive;
using SAS.Model;

namespace Game.Model
{
    [Bind]
    public class SelectionModel
    {
        public Field<Monster> CurrentChosenMonster = new Field<Monster>();
        public Field<Character> CurrentChosenCharacter = new Field<Character>();
    }
}