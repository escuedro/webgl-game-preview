﻿using Framework;
using Framework.Reactive;
using SAS.Model;

namespace Game.Model
{
	[Bind]
	public class BossSelectionModel
	{
		public ListField<CharacterIndexPair> Characters = new ListField<CharacterIndexPair>();
	}

	public class CharacterIndexPair
	{
		public Character Character;
		public int Index;
	}
}