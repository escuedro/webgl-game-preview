﻿using Framework;
using Framework.Model.Localization;
using Framework.Reactive;

namespace Game.Model
{
	[Bind]
	public class LocalSettings
	{
		public Field<int> SoundsVolume = new Field<int>();
		public Field<Language> Language = new Field<Language>();
	}
}