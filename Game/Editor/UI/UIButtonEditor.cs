﻿using System;
using System.Collections.Generic;
using Game.Audio;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(UIButton))]
public class UIButtonEditor : Editor
{
	private SerializedProperty _soundTypeProperty;
	private SoundType _soundType;

	private string _soundTypeString;
	private bool _changed;

	private void OnEnable()
	{
		_soundTypeProperty = serializedObject.FindProperty("_clickSoundName");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		EditorGUILayout.PropertyField(_soundTypeProperty);
		EditorGUILayout.BeginVertical();
		if (!NeedToFindVariant(_soundTypeProperty.stringValue, out _soundType))
		{
			_soundTypeProperty.stringValue = _soundType.ToString();
			Repaint();
		}
		else if (!string.IsNullOrEmpty(_soundTypeProperty.stringValue))
		{
			foreach (var soundType in GetSoundTypes(_soundTypeProperty.stringValue, 5))
			{
				if (GUILayout.Button(soundType.ToString()))
				{
					_soundTypeProperty.stringValue = soundType.ToString();
					_soundType = soundType;
					GUI.FocusControl(null);
					Repaint();
				}
			}
		}
		EditorGUILayout.EndVertical();

		serializedObject.ApplyModifiedProperties();
	}

	private bool NeedToFindVariant(string value, out SoundType soundType)
	{
		return !Enum.TryParse(value, out soundType);
	}

	private SoundType[] GetSoundTypes(string value, int count)
	{
		List<SoundType> soundTypes = new List<SoundType>();
		string[] names = Enum.GetNames(typeof(SoundType));
		int counter = 0;
		foreach (string soundName in names)
		{
			if (counter > count)
			{
				break;
			}
			if (soundName.Contains(value))
			{
				if (Enum.TryParse(soundName, out SoundType soundType))
				{
					soundTypes.Add(soundType);
					counter++;
				}
			}
		}
		return soundTypes.ToArray();
	}
}