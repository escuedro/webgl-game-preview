#if UNITY_EDITOR
using System.Collections.Generic;
using Framework.Extension;
using UnityEditor;
using UnityEngine;

namespace Game
{
	public static class EffectCorrector
	{
		[MenuItem("GameObject/FixVFXRenderers")]
		public static void FixVFXRenderers()
		{
			Debug.Log("Start FixVFXRenderers");
			IEnumerable<ParticleSystemRenderer> renderers = Selection.activeGameObject.FindTypeInGameObjectAndChildren<ParticleSystemRenderer>();
			foreach (ParticleSystemRenderer renderer in renderers)
			{
				if (renderer.renderMode == ParticleSystemRenderMode.HorizontalBillboard)
				{
					EditorUtility.SetDirty(renderer.gameObject);
					renderer.renderMode = ParticleSystemRenderMode.VerticalBillboard;
					Debug.Log($"Fixed render type in: {renderer.gameObject.name}");
				}
				else
				{
					Debug.Log($"Render type is correct in: {renderer.gameObject.name}");
				}
			}
			Debug.Log("End FixVFXRenderers");
		}
	}
}
#endif