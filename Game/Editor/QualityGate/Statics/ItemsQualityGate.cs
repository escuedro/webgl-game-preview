﻿#if !NOT_UNITY3D
using Framework.QualityGates;
using SAS.Model;
using UnityEngine;

namespace Game.QualityGates
{
	public class ItemsQualityGate : AllStaticQualityGate<ItemStatic>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(ItemStatic model)
		{
			if (model.Id != 1)
			{
				Asserts.NotEmpty(model.Name);
				Asserts.Resource<Sprite>(model.BackgroundVisual);
				Asserts.Resource<Sprite>(model.IconVisual);
				Asserts.NotEmpty(model.DescriptionTranslationKey);
			}
		}
	}
}
#endif