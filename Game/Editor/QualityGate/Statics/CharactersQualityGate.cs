﻿#if !NOT_UNITY3D
using Framework.QualityGates;
using SAS.Model;
using UnityEngine;

namespace Game.QualityGates
{
	public class CharactersQualityGate : AllStaticQualityGate<CharacterStatic>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(CharacterStatic model)
		{
			Asserts.Translation(model.TranslationKey);
			Asserts.Translation(model.DescriptionTranslationKey);
			Asserts.Enum(model.Element);
			Asserts.Positive(model.Defence);
			Asserts.Positive(model.Attack);
			Asserts.Positive(model.Luck);
			const int visiualLevels = 3;
			Asserts.Equals(model.CardVisual.Length, visiualLevels);
			foreach (string visualPath in model.CardVisual)
			{
				Asserts.Resource<Sprite>(visualPath);
			}
			Asserts.Resource<GameObject>(model.PrefabResourcePath);
			Asserts.Resource<Sprite>(model.PortraitResourcePath);
		}
	}
}
#endif