﻿#if !NOT_UNITY3D
using Framework.Model;
using Framework.QualityGates;
using SAS.Model;
using UnityEngine;

namespace Game.QualityGates
{
	public class MonstersQualityGate : AllStaticQualityGate<MonsterStatic>
	{
		public override bool StartOnPlay => false;

		protected override void CheckAsset(MonsterStatic model)
		{
			uint levelsCount = Statics.GetSingle<MonstersGenerationSettings>().MaxMonstersLevel;
			Asserts.Equals((uint)model.ChanceByLevel.Length, levelsCount);
			foreach (uint winChance in model.ChanceByLevel)
			{
				Asserts.InRange(winChance, 0, 100001);
			}
			Asserts.Equals((uint)model.EssenceForWin.Length, levelsCount);

			Asserts.Equals((uint)model.ExperienceForWin.Length, levelsCount);
			foreach (var value in model.ExperienceForWin)
			{
				Asserts.Positive(value);
			}

			Asserts.Equals((uint)model.ExperienceForLose.Length, levelsCount);
			foreach (var value in model.ExperienceForLose)
			{
				Asserts.Positive(value);
			}
			
			Asserts.Equals((uint)model.ExperienceForLose.Length, levelsCount);
			foreach (var value in model.ExperienceForLose)
			{
				Asserts.Positive(value);
			}

			Asserts.Equals((uint)model.TokenRewardChance.Length, levelsCount);
			foreach (uint tokenRewardChance in model.TokenRewardChance)
			{
				Asserts.InRange(tokenRewardChance, 0, 100001);
			}
			
			Asserts.Translation(model.TranslationKey);
		}
	}
}
#endif