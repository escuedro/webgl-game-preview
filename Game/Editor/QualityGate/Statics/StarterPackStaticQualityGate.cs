using Framework.QualityGates;
using SAS.Model;
using SAS.Model.StarterPack;

namespace Game.QualityGates
{
	public class StarterPackStaticQualityGate : SingleStaticQualityGate<StarterPackStatic>
	{
		public override bool StartOnPlay => false;

		protected override void CheckAsset(StarterPackStatic model)
		{
			Asserts.BiggerThanZero(model.PriceInTokens);
			Asserts.BiggerThanZero(model.FirstBuy.Length);
			foreach (ItemPair rewardItem in model.FirstBuy)
			{
				Asserts.NotNull(rewardItem.ItemStatic);
				Asserts.BiggerThanZero(rewardItem.Count);
			}
			Asserts.BiggerThanZero(model.SecondBuy.Length);
			foreach (ItemPair rewardItem in model.SecondBuy)
			{
				Asserts.NotNull(rewardItem.ItemStatic);
				Asserts.BiggerThanZero(rewardItem.Count);
			}
		}
	}
}