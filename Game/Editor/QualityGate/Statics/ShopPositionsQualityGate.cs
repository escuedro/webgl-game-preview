﻿#if !NOT_UNITY3D
using Framework.QualityGates;
using SAS.Model.Shop;

namespace Game.QualityGates
{
	public class ShopPositionsQualityGate : AllStaticQualityGate<ShopPositionStatic>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(ShopPositionStatic model)
		{
			Asserts.BiggerThanZero(model.PriceInTokens);
			Asserts.NotNull(model.Item.ItemStatic);
			Asserts.NotEmpty(model.Item.ItemStatic.Name);
			Asserts.BiggerThanZero(model.Item.Count);
			
		}
	}
}
#endif