﻿#if !NOT_UNITY3D
using System;
using Framework.Model;
using Framework.QualityGates;
using SAS.Model;

namespace Game.QualityGates
{
	[AutoTestExcepted]
	public class CharacterRarityGenerationChancesQualityGate : QualityGate
	{
		public override bool StartOnPlay => false;

		protected override void Test()
		{
			CharacterRarityGenerationChance[] model = Statics.GetAll<CharacterRarityGenerationChance>();

			try
			{
				Asserts.Equals(model.Length, Character.RarityLevels);
				foreach (var chance in model)
				{
					CheckAsset(chance);
				}
			}
			catch (Exception exception)
			{
				Collect(new Error(new Exception($"asset {model} has error {exception.Message}"), model));
			}
		}

		private void CheckAsset(CharacterRarityGenerationChance model)
		{
			Asserts.BiggerThanZero(model.GenerationWeight);
			Asserts.Enum(model.Rarity);
		}
	}
}
#endif