﻿#if !NOT_UNITY3D
using Framework.QualityGates;
using SAS.Model.Battle;

namespace Game.QualityGates
{
	public class TransmutationSettingsQualityGate : SingleStaticQualityGate<TransmutationSettings>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(TransmutationSettings model)
		{
			Asserts.InRange(model.CharacterDeathChance, 0, 100001);
			Asserts.InRange(model.SuccessTransmutateChance, 0, 100001);
		}
	}
}
#endif