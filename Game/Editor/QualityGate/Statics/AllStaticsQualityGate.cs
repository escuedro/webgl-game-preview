﻿#if !NOT_UNITY3D
using Framework.QualityGates;
using GameShared.Static;

namespace Game.QualityGates
{
	public class AllStaticsQualityGate : QualityGate
	{
		public override bool StartOnPlay => false;

		protected override void Test()
		{
#if UNITY_EDITOR
			new ClientStaticLoader().Load(Resource.StaticJson);
			
			Collect(new CharactersQualityGate().RunTest());
			Collect(new CharacterSharedQualityGate().RunTest());
			Collect(new ItemsQualityGate().RunTest());
			Collect(new MonstersQualityGate().RunTest());
			Collect(new BossesStaticQualityGate().RunTest());
			Collect(new ShopPositionsQualityGate().RunTest());
			Collect(new CharacterGenerationChancesQualityGate().RunTest());
			Collect(new CharacterRarityGenerationChancesQualityGate().RunTest());
			Collect(new MonstersGenerationSettingsQualityGate().RunTest());
			Collect(new CharacterFightSeriesSettingsQualityGate().RunTest());
			Collect(new TransmutationSettingsQualityGate().RunTest());
			Collect(new TokensPoolSettingsQualityGate().RunTest());
			Collect(new StarterPackStaticQualityGate().RunTest());
			Collect(new BossSettingsQualityGate().RunTest());
#endif
		}
	}
}
#endif