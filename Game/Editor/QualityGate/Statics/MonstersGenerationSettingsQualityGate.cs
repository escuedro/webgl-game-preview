﻿#if !NOT_UNITY3D
using Framework.QualityGates;
using SAS.Model;

namespace Game.QualityGates
{
	public class MonstersGenerationSettingsQualityGate : SingleStaticQualityGate<MonstersGenerationSettings>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(MonstersGenerationSettings model)
		{
			Asserts.BiggerThanZero(model.MaxMonstersAmount);
			Asserts.BiggerThanZero(model.MaxMonstersLevel);
		}
	}
}
#endif