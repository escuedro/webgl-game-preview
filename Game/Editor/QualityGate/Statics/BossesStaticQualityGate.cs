﻿using Framework.QualityGates;
using SAS.Model;
using UnityEngine;

namespace Game.QualityGates
{
	public class BossesStaticQualityGate : AllStaticQualityGate<BossStatic>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(BossStatic model)
		{
			Asserts.Translation(model.TranslationKey);
			Asserts.Positive(model.HealthPoints);
			Asserts.Positive(model.LifetimeSeconds);
			Asserts.Positive(model.WeekRewardPool);
			Asserts.Enum(model.Element);
			Asserts.InRange(model.MinCharactersInGroupCriterion, 0, 5);
			Asserts.Enum(model.CharactersRarity);
			Asserts.Positive(model.TicketsCost);
		}
	}
}