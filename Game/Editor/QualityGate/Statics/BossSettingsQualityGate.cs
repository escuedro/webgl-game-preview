﻿using Framework.QualityGates;
using SAS.Model;

namespace Game.QualityGates
{
	public class BossSettingsQualityGate : SingleStaticQualityGate<BossSettings>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(BossSettings model)
		{
			foreach (uint damageMultiplier in model.UniteDamageMultipliers)
			{
				Asserts.Positive(damageMultiplier);
			}
			foreach (int rarityBonus in model.RarityBonuses)
			{
				Asserts.Positive(rarityBonus);
			}
			Asserts.Positive(model.VictoryReward);
			Asserts.Positive(model.TimeoutReward);
			Asserts.Positive(model.BossElementModificator);
			Asserts.Positive(model.MinimumCharacterToBossDamage);
		}
	}
}