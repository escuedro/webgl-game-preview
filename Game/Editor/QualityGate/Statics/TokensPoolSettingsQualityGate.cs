﻿#if !NOT_UNITY3D
using Framework.QualityGates;
using SAS.Model;

namespace Game.QualityGates
{
	public class TokensPoolSettingsQualityGate : SingleStaticQualityGate<TokensPoolSettings>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(TokensPoolSettings model)
		{
			Asserts.BiggerThanZero(model.PoolSize);
		}
	}
}
#endif