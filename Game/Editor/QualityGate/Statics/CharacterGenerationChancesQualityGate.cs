﻿#if !NOT_UNITY3D
using System;
using Framework.Model;
using Framework.QualityGates;
using SAS.Model;

namespace Game.QualityGates
{
	[AutoTestExcepted]
	public class CharacterGenerationChancesQualityGate : QualityGate
	{
		public override bool StartOnPlay => false;

		protected override void Test()
		{
			CharacterGenerationChance[] model = Statics.GetAll<CharacterGenerationChance>();
			CharacterStatic[] characters = Statics.GetAll<CharacterStatic>();
			try
			{
				Asserts.Equals(model.Length, characters.Length);
				foreach (var chance in model)
				{
					CheckAsset(chance);
				}
			}
			catch (Exception exception)
			{
				Collect(new Error(new Exception($"asset {model} has error {exception.Message}"), model));
			}
		}

		private void CheckAsset(CharacterGenerationChance model)
		{
			Asserts.BiggerThanZero(model.GenerationWeight);
			Asserts.StaticId<CharacterStatic>(model.Id);
		}
	}
}
#endif