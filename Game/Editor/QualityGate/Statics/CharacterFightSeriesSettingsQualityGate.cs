﻿#if !NOT_UNITY3D
using Framework.QualityGates;
using SAS.Model.Battle;

namespace Game.QualityGates
{
	public class CharacterFightSeriesSettingsQualityGate : SingleStaticQualityGate<CharactersFightSeriesSettings>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(CharactersFightSeriesSettings model)
		{
			Asserts.BiggerThanZero(model.Cooldown);
		}
	}
}
#endif