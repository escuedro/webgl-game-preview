﻿#if !NOT_UNITY3D
using Framework.QualityGates;
using SAS.Model;
using UnityEngine;

namespace Game.QualityGates
{
	public class CharacterSharedQualityGate : SingleStaticQualityGate<CharacterSharedStatic>
	{
		public override bool StartOnPlay => false;
		protected override void CheckAsset(CharacterSharedStatic model)
		{
			Asserts.BiggerThanZero(model.MaximumLevel);
			Asserts.BiggerThanZero(model.MaximumRarity);
			Asserts.Equals(model.Power.Length, (int)model.MaximumLevel);
			Asserts.Equals(model.Rarity.Length, model.MaximumRarity);
			foreach (var rarity in model.Rarity)
			{
				foreach (var level in rarity.Levels)
				{
					Asserts.Positive(level.ExperienceToNextLevel);
					Asserts.Positive(level.TokensToNexLevel);
				}
			}
			Asserts.Equals(model.FrameVisualByRarity.Length, model.MaximumRarity);
			foreach (string resourcePath in model.FrameVisualByRarity)
			{
				Asserts.Resource<Sprite>(resourcePath);
			}
			Asserts.Equals(model.TitleVisualByRarity.Length, model.MaximumRarity);
			foreach (string resourcePath in model.TitleVisualByRarity)
			{
				Asserts.Resource<Sprite>(resourcePath);
			}
			Asserts.Equals(model.DecorRarityByVisual.Length, model.MaximumRarity);
			foreach (string resourcePath in model.DecorRarityByVisual)
			{
				Asserts.Resource<Sprite>(resourcePath);
			}
			Asserts.Equals(model.FlagVisualByElement.Length, 4);
			foreach (string resourcePath in model.FlagVisualByElement)
			{
				Asserts.Resource<Sprite>(resourcePath);
			}
			Asserts.Equals(model.BackgroundByRarity.Length, model.MaximumRarity);
			foreach (string resourcePath in model.BackgroundByRarity)
			{
				Asserts.Resource<Sprite>(resourcePath);
			}
			foreach (uint luckMultiplier in model.LuckMultipliers)
			{
				Asserts.Positive(luckMultiplier);
			}
		}
	}
}
#endif