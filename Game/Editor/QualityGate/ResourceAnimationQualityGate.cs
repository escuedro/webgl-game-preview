﻿#if !NOT_UNITY3D
using System.Linq;
using Framework.QualityGates;
using Game.View;
using Game.View.Animations;
using Spine.Unity;
using UnityEngine;

namespace Game.QualityGates
{
	public class ResourceAnimationQualityGate : AssetQualityGate<GameObject>
	{
		public override bool StartOnPlay => false;
		protected override string GetAssetFilter() => "t:prefab";
		protected override string[] GetAssetFolders() => new[] {"Assets/Resources/Animations"};

		protected override void CheckAsset(GameObject asset, string assetPath)
		{
			bool hasMecanim = asset.TryGetComponent<SkeletonMecanim>(out var mecanim);
			Asserts.True(hasMecanim);
			Asserts.NotNull(mecanim.SkeletonDataAsset);

			bool hasAnimator = asset.TryGetComponent<Animator>(out var animator);
			Asserts.True(hasAnimator);
			Asserts.NotNull(animator.runtimeAnimatorController);
			Asserts.NotNull(animator.runtimeAnimatorController.animationClips.FirstOrDefault(clip => clip.name == "idle" || clip.name == "Idle"));
			foreach (AnimationClip animationClip in animator.runtimeAnimatorController.animationClips.Where(clip => clip.name == "idle" || clip.name == "Idle"))
			{
				Asserts.True(animationClip.isLooping);
			}
			Asserts.NotNull(animator.runtimeAnimatorController.animationClips.FirstOrDefault(clip => clip.name == "attack"));

			bool hasHolder = asset.TryGetComponent<CharacterBattleEffectHolder>(out var holder);
			Asserts.True(hasHolder);
			Asserts.NotNull(holder.StunVfx);
			Asserts.NotNullTransform(holder.StunVfxPoint);
			Asserts.NotNullTransform(holder.AttackedVfxPoint);
			if (holder.AttackVfx != null)
			{
				Asserts.NotNullTransform(holder.AttackVfxPoint);
			}

			bool hasResource = asset.TryGetComponent<ResourceMecanimAnimation>(out var resource);
			Asserts.True(hasResource);
			Asserts.True(resource.Animator == animator);
			Asserts.True(resource.BattleEffectHolder == holder);
		}
	}
}
#endif