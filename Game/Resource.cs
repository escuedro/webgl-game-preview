using Framework;

namespace Game
{
	public abstract class Resource
	{
		public static readonly ResourcePath StaticJson = "static";
		public static readonly ResourcePath LineBreakingFollowingCharactersTxt = "LineBreaking Following Characters";
		public static readonly ResourcePath LineBreakingLeadingCharactersTxt = "LineBreaking Leading Characters";
		public static readonly ResourcePath TMPSettingsAsset = "TMP Settings";
		public static readonly ResourcePath SpineAssetDatabaseMarkerTxt = "SpineAssetDatabaseMarker";

		public abstract class Animations
		{

			public abstract class Characters
			{

				public abstract class Alchemist
				{
					public static readonly ResourcePath AlchemistPrefab = "Animations/Characters/Alchemist/Alchemist";
					public static readonly ResourcePath AlchemistControllerOverrideController = "Animations/Characters/Alchemist/AlchemistController";

				}
				public abstract class Assassin
				{
					public static readonly ResourcePath AssassinPrefab = "Animations/Characters/Assassin/Assassin";
					public static readonly ResourcePath AssassinControllerOverrideController = "Animations/Characters/Assassin/AssassinController";

				}
				public abstract class Berserk
				{
					public static readonly ResourcePath BerserkPrefab = "Animations/Characters/Berserk/Berserk";
					public static readonly ResourcePath BerserkControllerOverrideController = "Animations/Characters/Berserk/BerserkController";

				}
				public abstract class BladeSummoner
				{
					public static readonly ResourcePath BladeSummonerPrefab = "Animations/Characters/BladeSummoner/BladeSummoner";
					public static readonly ResourcePath BladeSummonerControllerOverrideController = "Animations/Characters/BladeSummoner/BladeSummonerController";

				}
				public abstract class Champion
				{
					public static readonly ResourcePath ChampionPrefab = "Animations/Characters/Champion/Champion";
					public static readonly ResourcePath ChampionControllerOverrideController = "Animations/Characters/Champion/ChampionController";

				}
				public abstract class CloudDragon
				{
					public static readonly ResourcePath CloudDragonPrefab = "Animations/Characters/CloudDragon/CloudDragon";
					public static readonly ResourcePath CloudDragonControllerOverrideController = "Animations/Characters/CloudDragon/CloudDragonController";

				}
				public abstract class DragonKing
				{
					public static readonly ResourcePath DragonKingPrefab = "Animations/Characters/DragonKing/DragonKing";
					public static readonly ResourcePath DragonKingControllerOverrideController = "Animations/Characters/DragonKing/DragonKingController";

				}
				public abstract class Druid
				{
					public static readonly ResourcePath DruidPrefab = "Animations/Characters/Druid/Druid";
					public static readonly ResourcePath DruidControllerOverrideController = "Animations/Characters/Druid/DruidController";

				}
				public abstract class Duelist
				{
					public static readonly ResourcePath DuelistPrefab = "Animations/Characters/Duelist/Duelist";
					public static readonly ResourcePath DuelistControllerOverrideController = "Animations/Characters/Duelist/DuelistController";

				}
				public abstract class EmberDragon
				{
					public static readonly ResourcePath EmberDragonPrefab = "Animations/Characters/EmberDragon/EmberDragon";
					public static readonly ResourcePath EmberDragonControllerOverrideController = "Animations/Characters/EmberDragon/EmberDragonController";

				}
				public abstract class Golem
				{
					public static readonly ResourcePath GolemPrefab = "Animations/Characters/Golem/Golem";
					public static readonly ResourcePath GolemControllerOverrideController = "Animations/Characters/Golem/GolemController";

				}
				public abstract class Guardsman
				{
					public static readonly ResourcePath GuardsmanPrefab = "Animations/Characters/Guardsman/Guardsman";
					public static readonly ResourcePath GuardsmanControllerOverrideController = "Animations/Characters/Guardsman/GuardsmanController";

				}
				public abstract class Huntress
				{
					public static readonly ResourcePath HuntressPrefab = "Animations/Characters/Huntress/Huntress";
					public static readonly ResourcePath HuntressControllerOverrideController = "Animations/Characters/Huntress/HuntressController";

				}
				public abstract class Illusionist
				{
					public static readonly ResourcePath IllusionistPrefab = "Animations/Characters/Illusionist/Illusionist";
					public static readonly ResourcePath IllusionistControllerOverrideController = "Animations/Characters/Illusionist/IllusionistController";

				}
				public abstract class InsaneMage
				{
					public static readonly ResourcePath InsaneMagePrefab = "Animations/Characters/Insane Mage/Insane Mage";
					public static readonly ResourcePath InsaneMageControllerOverrideController = "Animations/Characters/Insane Mage/InsaneMageController";

				}
				public abstract class Knight
				{
					public static readonly ResourcePath KnightPrefab = "Animations/Characters/Knight/Knight";
					public static readonly ResourcePath KnightControllerOverrideController = "Animations/Characters/Knight/KnightController";

				}
				public abstract class Mira
				{
					public static readonly ResourcePath MiraPrefab = "Animations/Characters/Mira/Mira";
					public static readonly ResourcePath MiraControllerOverrideController = "Animations/Characters/Mira/MiraController";

				}
				public abstract class MountainDragon
				{
					public static readonly ResourcePath MountainDragonPrefab = "Animations/Characters/MountainDragon/MountainDragon";
					public static readonly ResourcePath MountainDragonControllerOverrideController = "Animations/Characters/MountainDragon/MountainDragonController";

				}
				public abstract class Reaper
				{
					public static readonly ResourcePath ReaperPrefab = "Animations/Characters/Reaper/Reaper";
					public static readonly ResourcePath ReaperControllerOverrideController = "Animations/Characters/Reaper/ReaperController";

				}
				public abstract class SeaDragon
				{
					public static readonly ResourcePath SeaDragonPrefab = "Animations/Characters/SeaDragon/SeaDragon";
					public static readonly ResourcePath SeaDragonControllerOverrideController = "Animations/Characters/SeaDragon/SeaDragonController";

				}
				public abstract class Sorceress
				{
					public static readonly ResourcePath SorceressPrefab = "Animations/Characters/Sorceress/Sorceress";
					public static readonly ResourcePath SorceressControllerOverrideController = "Animations/Characters/Sorceress/SorceressController";

				}
				public abstract class Specter
				{
					public static readonly ResourcePath SpecterPrefab = "Animations/Characters/Specter/Specter";
					public static readonly ResourcePath SpecterControllerOverrideController = "Animations/Characters/Specter/SpecterController";

				}
				public abstract class Vampire
				{
					public static readonly ResourcePath VampirePrefab = "Animations/Characters/Vampire/Vampire";
					public static readonly ResourcePath VampireControllerOverrideController = "Animations/Characters/Vampire/VampireController";

				}
				public abstract class Warden
				{
					public static readonly ResourcePath WardenPrefab = "Animations/Characters/Warden/Warden";
					public static readonly ResourcePath WardenControllerOverrideController = "Animations/Characters/Warden/WardenController";

				}
				public abstract class Warlock
				{
					public static readonly ResourcePath WarlockPrefab = "Animations/Characters/Warlock/Warlock";
					public static readonly ResourcePath WarlockControllerOverrideController = "Animations/Characters/Warlock/WarlockController";

				}
				public abstract class Werewolf
				{
					public static readonly ResourcePath WerewolfPrefab = "Animations/Characters/Werewolf/Werewolf";
					public static readonly ResourcePath WerewolfControllerOverrideController = "Animations/Characters/Werewolf/WerewolfController";

				}
				public abstract class Zombie
				{
					public static readonly ResourcePath ZombiePrefab = "Animations/Characters/Zombie/Zombie";
					public static readonly ResourcePath ZombieControllerOverrideController = "Animations/Characters/Zombie/ZombieController";

				}
			}
			public abstract class UI
			{

				public abstract class Chest
				{
					public static readonly ResourcePath ChestatlasTxt = "Animations/UI/Chest/Chest.atlas";
					public static readonly ResourcePath ChestJson = "Animations/UI/Chest/Chest";
					public static readonly ResourcePath ChestPng = "Animations/UI/Chest/Chest";
					public static readonly ResourcePath ChestAtlasAsset = "Animations/UI/Chest/Chest_Atlas";
					public static readonly ResourcePath ChestMaterialAdditiveMat = "Animations/UI/Chest/Chest_Material-Additive";
					public static readonly ResourcePath ChestMaterialMat = "Animations/UI/Chest/Chest_Material";
					public static readonly ResourcePath ChestSkeletonDataAsset = "Animations/UI/Chest/Chest_SkeletonData";

					public abstract class ReferenceAssets
					{
						public static readonly ResourcePath IdleAsset = "Animations/UI/Chest/ReferenceAssets/idle";
						public static readonly ResourcePath OpenAsset = "Animations/UI/Chest/ReferenceAssets/open";

					}
				}
				public abstract class Hint
				{
					public static readonly ResourcePath AnimationAnim = "Animations/UI/Hint/Animation";
					public static readonly ResourcePath HintAnimatorControllerController = "Animations/UI/Hint/HintAnimatorController";

				}
			}
		}
		public abstract class Config
		{
			public static readonly ResourcePath TooltipRootPrefab = "Config/TooltipRoot";
			public static readonly ResourcePath WindowManagerRootPrefab = "Config/WindowManagerRoot";

		}
		public abstract class Data
		{

			public abstract class Translation
			{
				public static readonly ResourcePath EnglishBytes = "Data/Translation/English";
				public static readonly ResourcePath EspanolBytes = "Data/Translation/Espanol";
				public static readonly ResourcePath PinoyBytes = "Data/Translation/Pinoy";
				public static readonly ResourcePath PortugueseBytes = "Data/Translation/Portuguese";
				public static readonly ResourcePath RussianBytes = "Data/Translation/Russian";

			}
		}
		public abstract class Sounds
		{
			public static readonly ResourcePath AttackAlchemistAsset = "Sounds/Attack_Alchemist";
			public static readonly ResourcePath AttackAssassinAsset = "Sounds/Attack_Assassin";
			public static readonly ResourcePath AttackBerserkAsset = "Sounds/Attack_Berserk";
			public static readonly ResourcePath AttackCavalierAsset = "Sounds/Attack_Cavalier";
			public static readonly ResourcePath AttackChampionAsset = "Sounds/Attack_Champion";
			public static readonly ResourcePath AttackCloudDragonAsset = "Sounds/Attack_Cloud_Dragon";
			public static readonly ResourcePath AttackDragonKingAsset = "Sounds/Attack_Dragon_King";
			public static readonly ResourcePath AttackDruidAsset = "Sounds/Attack_Druid";
			public static readonly ResourcePath AttackDuelistAsset = "Sounds/Attack_Duelist";
			public static readonly ResourcePath AttackEmberDragonAsset = "Sounds/Attack_Ember_Dragon";
			public static readonly ResourcePath AttackGolemAsset = "Sounds/Attack_Golem";
			public static readonly ResourcePath AttackGuardsmanAsset = "Sounds/Attack_Guardsman";
			public static readonly ResourcePath AttackHuntressAsset = "Sounds/Attack_Huntress";
			public static readonly ResourcePath AttackIllusionistAsset = "Sounds/Attack_Illusionist";
			public static readonly ResourcePath AttackKnightAsset = "Sounds/Attack_Knight";
			public static readonly ResourcePath AttackMageAsset = "Sounds/Attack_Mage";
			public static readonly ResourcePath AttackMountainDragonAsset = "Sounds/Attack_Mountain_Dragon";
			public static readonly ResourcePath AttackReaperAsset = "Sounds/Attack_Reaper";
			public static readonly ResourcePath AttackSeaDragonAsset = "Sounds/Attack_Sea_Dragon";
			public static readonly ResourcePath AttackSorceressAsset = "Sounds/Attack_Sorceress";
			public static readonly ResourcePath AttackSpectreAsset = "Sounds/Attack_Spectre";
			public static readonly ResourcePath AttackSummonerAsset = "Sounds/Attack_Summoner";
			public static readonly ResourcePath AttackVampireAsset = "Sounds/Attack_Vampire";
			public static readonly ResourcePath AttackWardenAsset = "Sounds/Attack_Warden";
			public static readonly ResourcePath AttackWarlockAsset = "Sounds/Attack_Warlock";
			public static readonly ResourcePath AttackWerewolfAsset = "Sounds/Attack_Werewolf";
			public static readonly ResourcePath AttackZombieAsset = "Sounds/Attack_Zombie";
			public static readonly ResourcePath EGAmbientAsset = "Sounds/EG_Ambient";
			public static readonly ResourcePath EGBattleThemeAsset = "Sounds/EG_Battle_Theme";
			public static readonly ResourcePath EGMainThemeAsset = "Sounds/EG_Main_Theme";
			public static readonly ResourcePath SoundBossFightStartAsset = "Sounds/Sound_Boss_Fight_Start";
			public static readonly ResourcePath SoundCharacterSelectionAsset = "Sounds/Sound_Character_Selection";
			public static readonly ResourcePath SoundChestOpenAsset = "Sounds/Sound_Chest_Open";
			public static readonly ResourcePath SoundClickButtonAsset = "Sounds/Sound_Click_Button";
			public static readonly ResourcePath SoundElements1Asset = "Sounds/Sound_Elements_1";
			public static readonly ResourcePath SoundElements2Asset = "Sounds/Sound_Elements_2";
			public static readonly ResourcePath SoundElements3Asset = "Sounds/Sound_Elements_3";
			public static readonly ResourcePath SoundElixirUsedAsset = "Sounds/Sound_Elixir_Used";
			public static readonly ResourcePath SoundFightStartAsset = "Sounds/Sound_Fight_Start";
			public static readonly ResourcePath SoundLoginAsset = "Sounds/Sound_Login";
			public static readonly ResourcePath SoundLoginFailureAsset = "Sounds/Sound_Login_Failure";
			public static readonly ResourcePath SoundLoseAsset = "Sounds/Sound_Lose";
			public static readonly ResourcePath SoundLvlUpAsset = "Sounds/Sound_Lvl_Up";
			public static readonly ResourcePath SoundMergeAsset = "Sounds/Sound_Merge";
			public static readonly ResourcePath SoundMergeFailureAsset = "Sounds/Sound_Merge_Failure";
			public static readonly ResourcePath SoundPurchaseAsset = "Sounds/Sound_Purchase";
			public static readonly ResourcePath SoundRarity1Asset = "Sounds/Sound_Rarity_1";
			public static readonly ResourcePath SoundRarity2Asset = "Sounds/Sound_Rarity_2";
			public static readonly ResourcePath SoundRarity3Asset = "Sounds/Sound_Rarity_3";
			public static readonly ResourcePath SoundRarity4Asset = "Sounds/Sound_Rarity_4";
			public static readonly ResourcePath SoundRarity5Asset = "Sounds/Sound_Rarity_5";
			public static readonly ResourcePath SoundRarity6Asset = "Sounds/Sound_Rarity_6";
			public static readonly ResourcePath SoundRefreshAsset = "Sounds/Sound_Refresh";
			public static readonly ResourcePath SoundRewardAsset = "Sounds/Sound_Reward";
			public static readonly ResourcePath SoundScrollBarAsset = "Sounds/Sound_Scroll_Bar";
			public static readonly ResourcePath SoundStarterPackAsset = "Sounds/Sound_Starter_Pack";
			public static readonly ResourcePath SoundWarningAsset = "Sounds/Sound_Warning";
			public static readonly ResourcePath SoundWinAsset = "Sounds/Sound_Win";

		}
		public abstract class Textures
		{

			public abstract class Characters
			{

				public abstract class HeroesBattlePortraits
				{
					public static readonly ResourcePath AlchemistportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Alchemist_portrait_mini";
					public static readonly ResourcePath AssassinportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Assassin_portrait_mini";
					public static readonly ResourcePath BerserkportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Berserk_portrait_mini";
					public static readonly ResourcePath BladeSummonerportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Blade_Summoner_portrait_mini";
					public static readonly ResourcePath ChampionportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Champion_portrait_mini";
					public static readonly ResourcePath CharactersSpriteAtlasSpriteatlas = "Textures/Characters/HeroesBattlePortraits/Characters Sprite Atlas";
					public static readonly ResourcePath DruidportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Druid_portrait_mini";
					public static readonly ResourcePath DuelistportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Duelist_portrait_mini";
					public static readonly ResourcePath HuntressportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Huntress_portrait_mini";
					public static readonly ResourcePath IllusionistportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Illusionist_portrait_mini";
					public static readonly ResourcePath ImperialGuardsmanportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Imperial_Guardsman_portrait_mini";
					public static readonly ResourcePath KnightportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Knight_portrait_mini";
					public static readonly ResourcePath ReaperportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Reaper_portrait_mini";
					public static readonly ResourcePath RiderportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Rider_portrait_mini";
					public static readonly ResourcePath SorceressportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Sorceress_portrait_mini";
					public static readonly ResourcePath WardenportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Warden_portrait_mini";
					public static readonly ResourcePath WarlockportraitminiPng = "Textures/Characters/HeroesBattlePortraits/Warlock_portrait_mini";

				}
				public abstract class MonstersBattlePortraits
				{
					public static readonly ResourcePath ClouddragonportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Cloud_dragon_portrait_mini";
					public static readonly ResourcePath CrazedmageportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Crazed_mage_portrait_mini";
					public static readonly ResourcePath DragonkingofmarangportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Dragon_king_of_marang_portrait_mini";
					public static readonly ResourcePath EmberdragonportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Ember_dragon_portrait_mini";
					public static readonly ResourcePath GolemportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Golem_portrait_mini";
					public static readonly ResourcePath MonstersSpriteAtlasSpriteatlas = "Textures/Characters/MonstersBattlePortraits/Monsters Sprite Atlas";
					public static readonly ResourcePath MountaindragonportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Mountain_dragon_portrait_mini";
					public static readonly ResourcePath SeadragonportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Sea_dragon_portrait_mini";
					public static readonly ResourcePath SpecterportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Specter_portrait_mini";
					public static readonly ResourcePath VampireportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Vampire_portrait_mini";
					public static readonly ResourcePath WerwolfportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Werwolf_portrait_mini";
					public static readonly ResourcePath ZombieportraitminiPng = "Textures/Characters/MonstersBattlePortraits/Zombie_portrait_mini";

				}
			}
			public abstract class Items
			{

				public abstract class Backgrounds
				{
					public static readonly ResourcePath ElixirBackgroundPng = "Textures/Items/Backgrounds/ElixirBackground";
					public static readonly ResourcePath FoliantBackgroundPng = "Textures/Items/Backgrounds/FoliantBackground";
					public static readonly ResourcePath TicketBackgroundPng = "Textures/Items/Backgrounds/TicketBackground";

				}
				public abstract class Icons
				{
					public static readonly ResourcePath ElixirPng = "Textures/Items/Icons/Elixir";
					public static readonly ResourcePath FoliantPng = "Textures/Items/Icons/Foliant";
					public static readonly ResourcePath TicketPng = "Textures/Items/Icons/Ticket";

				}
			}
		}
		public abstract class UI
		{

			public abstract class Boss
			{
				public static readonly ResourcePath MedalbronzePng = "UI/Boss/medal_bronze";
				public static readonly ResourcePath MedalgoldPng = "UI/Boss/medal_gold";
				public static readonly ResourcePath MedalsilverPng = "UI/Boss/medal_silver";

			}
			public abstract class Card
			{

				public abstract class Backgrounds
				{
					public static readonly ResourcePath CardbgpicforestPng = "UI/Card/Backgrounds/card_bg_pic_forest";
					public static readonly ResourcePath CardbgpicskyPng = "UI/Card/Backgrounds/card_bg_pic_sky";

				}
				public abstract class BossImages
				{
					public static readonly ResourcePath ClouddragoncardpicPng = "UI/Card/BossImages/cloud_dragon_card_pic";
					public static readonly ResourcePath EmberdragoncardpicPng = "UI/Card/BossImages/ember_dragon_card_pic";
					public static readonly ResourcePath MarangdragoncardpicPng = "UI/Card/BossImages/marang_dragon_card_pic";
					public static readonly ResourcePath MonstersCardAtlasSpriteatlas = "UI/Card/BossImages/Monsters Card Atlas";
					public static readonly ResourcePath MountaindragoncardpicPng = "UI/Card/BossImages/mountain_dragon_card_pic";
					public static readonly ResourcePath SeadragoncardpicPng = "UI/Card/BossImages/sea_dragon_card_pic";

				}
				public abstract class CharacterImages
				{
					public static readonly ResourcePath AlchemistPng = "UI/Card/CharacterImages/Alchemist";
					public static readonly ResourcePath AssassinPng = "UI/Card/CharacterImages/Assassin";
					public static readonly ResourcePath BerserkPng = "UI/Card/CharacterImages/Berserk";
					public static readonly ResourcePath CavalierPng = "UI/Card/CharacterImages/Cavalier";
					public static readonly ResourcePath ChampionPng = "UI/Card/CharacterImages/Champion";
					public static readonly ResourcePath CharactersCardAtlasSpriteatlas = "UI/Card/CharacterImages/Characters Card Atlas";
					public static readonly ResourcePath DruidPng = "UI/Card/CharacterImages/Druid";
					public static readonly ResourcePath DuelistPng = "UI/Card/CharacterImages/Duelist";
					public static readonly ResourcePath GuardsmanPng = "UI/Card/CharacterImages/Guardsman";
					public static readonly ResourcePath HuntressPng = "UI/Card/CharacterImages/Huntress";
					public static readonly ResourcePath IllusionistPng = "UI/Card/CharacterImages/Illusionist";
					public static readonly ResourcePath KnightPng = "UI/Card/CharacterImages/Knight";
					public static readonly ResourcePath ReaperPng = "UI/Card/CharacterImages/Reaper";
					public static readonly ResourcePath SorceressPng = "UI/Card/CharacterImages/Sorceress";
					public static readonly ResourcePath SummonerPng = "UI/Card/CharacterImages/Summoner";
					public static readonly ResourcePath WardenPng = "UI/Card/CharacterImages/Warden";
					public static readonly ResourcePath WarlockPng = "UI/Card/CharacterImages/Warlock";

				}
				public abstract class Decor
				{
					public static readonly ResourcePath IconraritycommonPng = "UI/Card/Decor/icon_rarity_common";
					public static readonly ResourcePath IconrarityepicPng = "UI/Card/Decor/icon_rarity_epic";
					public static readonly ResourcePath IconraritylegendaryPng = "UI/Card/Decor/icon_rarity_legendary";
					public static readonly ResourcePath IconraritymysticPng = "UI/Card/Decor/icon_rarity_mystic";
					public static readonly ResourcePath IconrarityrarePng = "UI/Card/Decor/icon_rarity_rare";
					public static readonly ResourcePath IconrarityuncommonPng = "UI/Card/Decor/icon_rarity_uncommon";

				}
				public abstract class Flags
				{
					public static readonly ResourcePath FlagairPng = "UI/Card/Flags/flag_air";
					public static readonly ResourcePath FlagfirePng = "UI/Card/Flags/flag_fire";
					public static readonly ResourcePath FlagnaturePng = "UI/Card/Flags/flag_nature";
					public static readonly ResourcePath FlagwaterPng = "UI/Card/Flags/flag_water";

				}
				public abstract class Frame
				{
					public static readonly ResourcePath FramecommonPng = "UI/Card/Frame/frame_common";
					public static readonly ResourcePath FrameepicPng = "UI/Card/Frame/frame_epic";
					public static readonly ResourcePath FramelegendaryPng = "UI/Card/Frame/frame_legendary";
					public static readonly ResourcePath FramemysticPng = "UI/Card/Frame/frame_mystic";
					public static readonly ResourcePath FramerarePng = "UI/Card/Frame/frame_rare";
					public static readonly ResourcePath FrameuncommonPng = "UI/Card/Frame/frame_uncommon";

				}
				public abstract class MonsterImages
				{
					public static readonly ResourcePath GolemcardpicPng = "UI/Card/MonsterImages/golem_card_pic";
					public static readonly ResourcePath InsanemagecardpicPng = "UI/Card/MonsterImages/insane_mage_card_pic";
					public static readonly ResourcePath SpectercardpicPng = "UI/Card/MonsterImages/specter_card_pic";
					public static readonly ResourcePath VampirecardpicPng = "UI/Card/MonsterImages/vampire_card_pic";
					public static readonly ResourcePath WerewolfcardpicPng = "UI/Card/MonsterImages/werewolf_card_pic";
					public static readonly ResourcePath ZombiecardpicPng = "UI/Card/MonsterImages/zombie_card_pic";

				}
				public abstract class Titles
				{
					public static readonly ResourcePath TitlecommonPng = "UI/Card/Titles/title_common";
					public static readonly ResourcePath TitleepicPng = "UI/Card/Titles/title_epic";
					public static readonly ResourcePath TitlelegendaryPng = "UI/Card/Titles/title_legendary";
					public static readonly ResourcePath TitlemysticPng = "UI/Card/Titles/title_mystic";
					public static readonly ResourcePath TitlerarePng = "UI/Card/Titles/title_rare";
					public static readonly ResourcePath TitleuncommonPng = "UI/Card/Titles/title_uncommon";

				}
			}
			public abstract class Items
			{
				public static readonly ResourcePath ClosedChestPng = "UI/Items/Closed_Chest";
				public static readonly ResourcePath OpenChestPng = "UI/Items/Open_Chest";

			}
		}
		public abstract class VFX
		{
			public static readonly ResourcePath AlchemistDamagePrefab = "VFX/Alchemist_Damage";
			public static readonly ResourcePath AssassinDamagePrefab = "VFX/Assassin_Damage";
			public static readonly ResourcePath BerserkAttackPrefab = "VFX/Berserk_Attack";
			public static readonly ResourcePath BerserkDamagePrefab = "VFX/Berserk_Damage";
			public static readonly ResourcePath BladeSummonerDamagePrefab = "VFX/Blade_Summoner_Damage";
			public static readonly ResourcePath CloudDragonAttackPrefab = "VFX/Cloud_Dragon_Attack";
			public static readonly ResourcePath DerangedMageAttackPrefab = "VFX/Deranged Mage_Attack";
			public static readonly ResourcePath DerangedMageDamagePrefab = "VFX/Deranged Mage_Damage";
			public static readonly ResourcePath DragonKingAttackPrefab = "VFX/Dragon_King_Attack";
			public static readonly ResourcePath DruidAttackPrefab = "VFX/Druid_Attack";
			public static readonly ResourcePath DruidDamagePrefab = "VFX/Druid_Damage";
			public static readonly ResourcePath DuelistDamagePrefab = "VFX/Duelist_Damage";
			public static readonly ResourcePath EmberDragonAttackPrefab = "VFX/Ember_Dragon_Attack";
			public static readonly ResourcePath GolemDamagePrefab = "VFX/Golem_Damage";
			public static readonly ResourcePath HuntressAttackPrefab = "VFX/Huntress_Attack";
			public static readonly ResourcePath HuntressDamagePrefab = "VFX/Huntress_Damage";
			public static readonly ResourcePath ImperialCavalierAttackPrefab = "VFX/Imperial_Cavalier_Attack";
			public static readonly ResourcePath ImperialCavalierDamagePrefab = "VFX/Imperial_Cavalier_Damage";
			public static readonly ResourcePath ImperialChampionAttackPrefab = "VFX/Imperial_Champion_Attack";
			public static readonly ResourcePath ImperialChampionDamagePrefab = "VFX/Imperial_Champion_Damage";
			public static readonly ResourcePath ImperialGuardsmanAttackPrefab = "VFX/Imperial_Guardsman_Attack";
			public static readonly ResourcePath ImperialGuardsmanDamagePrefab = "VFX/Imperial_Guardsman_Damage";
			public static readonly ResourcePath InsaneMageAttackPrefab = "VFX/Insane_Mage_Attack";
			public static readonly ResourcePath KnightAttackPrefab = "VFX/Knight_Attack";
			public static readonly ResourcePath KnightDamagePrefab = "VFX/Knight_Damage";
			public static readonly ResourcePath MountainDragonAttackPrefab = "VFX/Mountain_Dragon_Attack";
			public static readonly ResourcePath ParticlesAllPrefab = "VFX/Particles_All";
			public static readonly ResourcePath ReaperAttackPrefab = "VFX/Reaper_Attack";
			public static readonly ResourcePath ReaperDamagePrefab = "VFX/Reaper_Damage";
			public static readonly ResourcePath SeaDragonAttackPrefab = "VFX/Sea_Dragon_Attack";
			public static readonly ResourcePath SorceressAttackPrefab = "VFX/Sorceress_Attack";
			public static readonly ResourcePath SorceressDamagePrefab = "VFX/Sorceress_Damage";
			public static readonly ResourcePath SpecterAttackPrefab = "VFX/Specter_Attack";
			public static readonly ResourcePath SpecterDamagePrefab = "VFX/Specter_Damage";
			public static readonly ResourcePath StunAllPrefab = "VFX/Stun_All";
			public static readonly ResourcePath VampireDamagePrefab = "VFX/Vampire_Damage";
			public static readonly ResourcePath WardenAttackPrefab = "VFX/Warden_Attack";
			public static readonly ResourcePath WardenDamagePrefab = "VFX/Warden_Damage";
			public static readonly ResourcePath WarlockAttackPrefab = "VFX/Warlock_Attack";
			public static readonly ResourcePath WarlockDamagePrefab = "VFX/Warlock_Damage";
			public static readonly ResourcePath WerewolfAttackPrefab = "VFX/Werewolf_Attack";
			public static readonly ResourcePath WerewolfDamagePrefab = "VFX/Werewolf_Damage";
			public static readonly ResourcePath ZombieAttackPrefab = "VFX/Zombie_Attack";
			public static readonly ResourcePath ZombieDamagePrefab = "VFX/Zombie_Damage";

		}
		public abstract class View
		{

			public abstract class Battle
			{
				public static readonly ResourcePath BattleCharactersViewPrefab = "View/Battle/BattleCharactersView";
				public static readonly ResourcePath BossBattleCharactersViewPrefab = "View/Battle/BossBattleCharactersView";

			}
			public abstract class Boss
			{
				public static readonly ResourcePath BossViewPrefab = "View/Boss/Boss View";
				public static readonly ResourcePath BossWarningWindowPrefab = "View/Boss/BossWarningWindow";
				public static readonly ResourcePath BossWindowWarningElementPrefab = "View/Boss/BossWindowWarningElement";

			}
			public abstract class Character
			{
				public static readonly ResourcePath CharacterListItemPrefab = "View/Character/CharacterListItem";
				public static readonly ResourcePath CharacterListViewPrefab = "View/Character/CharacterListView";
				public static readonly ResourcePath CharacterViewPrefab = "View/Character/CharacterView";

				public abstract class Extra
				{
					public static readonly ResourcePath DontHavePairPrefab = "View/Character/Extra/DontHavePair";
					public static readonly ResourcePath InventoryPopupPrefab = "View/Character/Extra/InventoryPopup";
					public static readonly ResourcePath NextFightSeriesPrefab = "View/Character/Extra/NextFightSeries";
					public static readonly ResourcePath SelectCharacterPopupPrefab = "View/Character/Extra/SelectCharacterPopup";

				}
			}
			public abstract class Cheat
			{
				public static readonly ResourcePath CheatsWidgetPrefab = "View/Cheat/CheatsWidget";
				public static readonly ResourcePath CheatsWindowPrefab = "View/Cheat/CheatsWindow";

			}
			public abstract class Grid
			{
				public static readonly ResourcePath GridViewPrefab = "View/Grid/GridView";

			}
			public abstract class Help
			{
				public static readonly ResourcePath HintViewPrefab = "View/Help/HintView";

			}
			public abstract class History
			{
				public static readonly ResourcePath BossHistorнItemPlayerViewPrefab = "View/History/BossHistorнItemPlayerView";
				public static readonly ResourcePath BossHistorнItemViewPrefab = "View/History/BossHistorнItemView";
				public static readonly ResourcePath HistoryItemViewPrefab = "View/History/HistoryItemView";

			}
			public abstract class Loading
			{
				public static readonly ResourcePath LoadingViewPrefab = "View/Loading/LoadingView";
				public static readonly ResourcePath LoggedOutViewPrefab = "View/Loading/LoggedOutView";

			}
			public abstract class Monster
			{
				public static readonly ResourcePath MonsterListItemPrefab = "View/Monster/MonsterListItem";
				public static readonly ResourcePath MonsterListViewPrefab = "View/Monster/MonsterListView";
				public static readonly ResourcePath MonsterViewPrefab = "View/Monster/MonsterView";

			}
			public abstract class Other
			{
				public static readonly ResourcePath FightButtonPrefab = "View/Other/Fight Button";
				public static readonly ResourcePath FpsPrefab = "View/Other/Fps";
				public static readonly ResourcePath TokensPrefab = "View/Other/Tokens";

			}
			public abstract class Root
			{
				public static readonly ResourcePath AudioManagerRootPrefab = "View/Root/AudioManagerRoot";
				public static readonly ResourcePath MainBackgroundPrefab = "View/Root/MainBackground";
				public static readonly ResourcePath MainUiCanvasPrefab = "View/Root/MainUiCanvas";
				public static readonly ResourcePath TopPanelPrefab = "View/Root/TopPanel";

			}
			public abstract class Tooltip
			{
				public static readonly ResourcePath TooltipViewPrefab = "View/Tooltip/TooltipView";

			}
		}
		public abstract class Windows
		{
			public static readonly ResourcePath BattleDefeatViewPrefab = "Windows/BattleDefeatView";
			public static readonly ResourcePath BattleHistoryWindowPrefab = "Windows/BattleHistoryWindow";
			public static readonly ResourcePath BattleScreenPrefab = "Windows/BattleScreen";
			public static readonly ResourcePath BattleVictoryViewPrefab = "Windows/BattleVictoryView";
			public static readonly ResourcePath BossBattleHistoryWindowPrefab = "Windows/BossBattleHistoryWindow";
			public static readonly ResourcePath BossBattleResultsViewPrefab = "Windows/BossBattleResultsView";
			public static readonly ResourcePath BossBattleSceneScreenPrefab = "Windows/BossBattleSceneScreen";
			public static readonly ResourcePath BossBattleScreenPrefab = "Windows/BossBattleScreen";
			public static readonly ResourcePath CharacterInfoWindowPrefab = "Windows/CharacterInfoWindow";
			public static readonly ResourcePath CharacterLevelUpWindowPrefab = "Windows/CharacterLevelUpWindow";
			public static readonly ResourcePath FightResultsWindowPrefab = "Windows/Fight Results Window";
			public static readonly ResourcePath InventoryScreenPrefab = "Windows/InventoryScreen";
			public static readonly ResourcePath ItemDescriptionWindowPrefab = "Windows/ItemDescriptionWindow";
			public static readonly ResourcePath LogoutConfirmWindowPrefab = "Windows/LogoutConfirmWindow";
			public static readonly ResourcePath MessagePopupPrefab = "Windows/MessagePopup";
			public static readonly ResourcePath MonsterBattleScreenPrefab = "Windows/MonsterBattleScreen";
			public static readonly ResourcePath NeedBuyCharacterWindowPrefab = "Windows/NeedBuyCharacterWindow";
			public static readonly ResourcePath NotEnoughMoneyWindowPrefab = "Windows/NotEnoughMoneyWindow";
			public static readonly ResourcePath SelectCharacterWindowPrefab = "Windows/Select Character Window";
			public static readonly ResourcePath SettingsWindowPrefab = "Windows/SettingsWindow";
			public static readonly ResourcePath ShopWindowPrefab = "Windows/ShopWindow";
			public static readonly ResourcePath StarterPackScreenPrefab = "Windows/StarterPackScreen";
			public static readonly ResourcePath TransmutationConfirmWindowPrefab = "Windows/TransmutationConfirmWindow";
			public static readonly ResourcePath TransmutationScreenPrefab = "Windows/TransmutationScreen";
			public static readonly ResourcePath WindowsCanvasPrefab = "Windows/Windows Canvas";

		}
		public abstract class StyleSheets
		{
			public static readonly ResourcePath DefaultStyleSheetAsset = "Style Sheets/Default Style Sheet";

		}
		public abstract class TextMeshProResources
		{
			public static readonly ResourcePath ArialAsset = "TextMeshProResources/Arial";
			public static readonly ResourcePath MontserratBoldSDFAsset = "TextMeshProResources/Montserrat-Bold SDF";
			public static readonly ResourcePath MontserratAsset = "TextMeshProResources/Montserrat";

		}
	}
}
