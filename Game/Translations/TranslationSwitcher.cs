﻿using Framework;
using Framework.Client.Translations;
using Framework.Model.Localization;
using Framework.Serialization;

namespace Game.Translations
{
	[Bind]
	public class TranslationSwitcher
	{
		private TranslationDeserializer _translationDeserializer;

		[Inject]
		public TranslationSwitcher(BaseSerializer baseSerializer)
		{
			_translationDeserializer = new TranslationDeserializer(baseSerializer);
		}

		public void SwitchLanguage(Language currentLanguage)
		{
			TranslationFile translationFile = _translationDeserializer.Deserialize(currentLanguage);
			Translation.SetTranslationFile(currentLanguage, translationFile);
			Translation.SetCurrentLanguage(currentLanguage);
		}
	}
}