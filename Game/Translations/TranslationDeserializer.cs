﻿using System;
using Framework.Client.Translations;
using Framework.Collections;
using Framework.Model.Localization;
using Framework.Serialization;
using GameShared;
using UnityEngine;

namespace Game.Translations
{
	public class TranslationDeserializer
	{
		private const string SerializedTranslationFilesPath = "Data/Translation/";

		private readonly BaseSerializer _baseSerializer;

		public TranslationDeserializer(BaseSerializer baseSerializer)
		{
			_baseSerializer = baseSerializer;
		}

		public TranslationFile Deserialize(Language language)
		{
			string pathToSerializedData = SerializedTranslationFilesPath + GetFileNameByLanguage(language);
			byte[] data = Resources.Load<TextAsset>(pathToSerializedData).bytes;
			IBinaryDeserializer binaryDeserializer = new BinaryDeserializer(data);
			TranslationData.Response request = new TranslationData.Response();
			_baseSerializer.Deserialize(binaryDeserializer, out request);
			ConstArray<KeyTranslationPair> keyTranslationPairs = request.TranslationDto.Translations;
			return new TranslationFile(keyTranslationPairs);
		}
		
		private static string GetFileNameByLanguage(Language language)
		{
			switch (language)
			{
				case Language.English:
					return "English";
				case Language.Russian:
					return "Russian";
				case Language.Espanol:
					return "Espanol";
				case Language.Portuguese:
					return "Portuguese";
				case Language.Pinoy:
					return "Pinoy";
				default:
					throw new ArgumentOutOfRangeException(nameof(language), language, null);
			}
		}
	}
}