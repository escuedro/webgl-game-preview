﻿#if UNITY_EDITOR

using System.IO;
using Framework.Client.Translations;
using Framework.Collections;
using Framework.Futures;
using Framework.Model.Localization;
using Framework.Serialization;
using GameShared;
using UnityEditor;
using UnityEngine;

namespace Game.Translations
{
	public class TranslationGenerator
	{
		private string _translationPathsFilePath =
				Path.Combine(Application.dataPath, "Game", "Configs", "TranslationPaths.json");
		private string SerializedTranslationFilesPath =
				Path.Combine(Application.dataPath, "Resources", "Data", "Translation");
		private string ProjectRelativeSerializedFilePath = Path.Combine("Assets", "Resources", "Data", "Translation");

		private BaseSerializer _baseSerializer;

		private int _filesCount;
		private Promise _writeFilePromise;

		public TranslationGenerator(BaseSerializer baseSerializer)
		{
			_baseSerializer = baseSerializer;
		}

		public void Generate()
		{
			string jsonString = File.ReadAllText(_translationPathsFilePath);
			TranslationFilePaths translationFilePaths = JsonUtility.FromJson<TranslationFilePaths>(jsonString);
			_filesCount = translationFilePaths.FilePaths.Length;
			foreach (LanguagePathPair languagePathPair in translationFilePaths.FilePaths)
			{
				KeyTranslationPair[] keyTranslationPairs = GetTranslationPairs(languagePathPair);
				
				ConstArray<KeyTranslationPair> keyTranslations = new ConstArray<KeyTranslationPair>(keyTranslationPairs);
				TranslationDto translationDto = new TranslationDto() {Translations = keyTranslations};
				TranslationData.Request request = new TranslationData.Request() {TranslationDto = translationDto};
				
				BinarySerializer binarySerializer = new BinarySerializer();
				_baseSerializer.Serialize(binarySerializer, request);
				byte[] serializedData = binarySerializer.GetResult(out int bytesCount);

				WriteToFile(serializedData, bytesCount, languagePathPair.Language);
			}
		}

		private static KeyTranslationPair[] GetTranslationPairs(LanguagePathPair languagePathPair)
		{
			string pathToCsvFile = Path.Combine(Application.dataPath, languagePathPair.TranslationFilePath);
			CsvFileLoader csvFileLoader =
					new CsvFileLoader(new CsvFileLoaderSettings(pathToCsvFile, languagePathPair.Language));
			CsvFileReader csvFileReader = new CsvFileReader(new CsvFileReaderSettings(';', 0));
			return csvFileReader.Read(csvFileLoader.Load());
		}

		private void WriteToFile(byte[] serializedData, int bytesCount, Language fileLanguage)
		{
			string filePath = Path.Combine(SerializedTranslationFilesPath, GetFileNameByLanguage(fileLanguage));
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}
			if (!Directory.Exists(SerializedTranslationFilesPath))
			{
				Directory.CreateDirectory(SerializedTranslationFilesPath);
			}
			using (FileStream fileStream = File.Create(filePath))
			{
				fileStream.Write(serializedData, 0, bytesCount);
			}
			string projectRelativeFilePath = Path.Combine(ProjectRelativeSerializedFilePath, GetFileNameByLanguage(fileLanguage));
			AssetDatabase.ImportAsset(projectRelativeFilePath, ImportAssetOptions.Default);
		}

		private string GetFileNameByLanguage(Language language)
		{
			return $"{language.ToString()}.bytes";
		}
	}
}

#endif