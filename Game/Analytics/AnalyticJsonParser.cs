﻿using System.Collections.Generic;
using System.Text;
using Framework.Service;

namespace Game.Analytics
{
	public class AnalyticJsonParser
	{
		private readonly string _apiKey;
		private readonly string _userId;

		private const string ApiKeyName = "api_key";
		private const string EventsName = "events";
		private const string EventTypeName = "event_type";
		private const string UserIdName = "user_id";
		private const string EventPropertiesName = "event_properties";

		public AnalyticJsonParser(string apiKey, string userId)
		{
			_apiKey = apiKey;
			_userId = userId;
		}

		public string Parse(IEnumerable<AnalyticsEventTemp> events)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendLine("{");
			stringBuilder.AppendLine($"{InDoubleQuotes(ApiKeyName)}: {InDoubleQuotes(_apiKey)},");
			stringBuilder.AppendLine($"{InDoubleQuotes(EventsName)}: [");
			foreach (AnalyticsEventTemp analyticsEvent in events)
			{
				stringBuilder.AppendLine("{");
				stringBuilder.AppendLine($"{InDoubleQuotes(UserIdName)}: {InDoubleQuotes(_userId)},");
				stringBuilder.Append($"{InDoubleQuotes(EventTypeName)}: {InDoubleQuotes(analyticsEvent.Name)}");
				int argumentsCount = analyticsEvent.Arguments.Count;
				int counter = 0;
				if (analyticsEvent.Arguments.Count > 0)
				{
					stringBuilder.AppendLine(",");
					stringBuilder.Append($"{InDoubleQuotes(EventPropertiesName)}: ");
					stringBuilder.AppendLine("{");
					foreach (KeyValuePair<string,object> eventArgument in analyticsEvent.Arguments)
					{
						stringBuilder.Append(
								$"{InDoubleQuotes(eventArgument.Key)}: {InDoubleQuotes(eventArgument.Value.ToString())}");
						counter++;
						if (counter != argumentsCount)
						{
							stringBuilder.Append(',');
						}
					}
					stringBuilder.AppendLine("}");
				}
				stringBuilder.AppendLine("},");
			}
			stringBuilder.AppendLine("]");
			stringBuilder.Append("}");
			
			return stringBuilder.ToString();
		}

		private string InDoubleQuotes(string value)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("\"");
			stringBuilder.Append(value);
			stringBuilder.Append("\"");
			return stringBuilder.ToString();
		}
	}
}