﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Framework;
using Framework.Client;
using Framework.Client.Timer;
using Framework.Reactive;
using Framework.Service;
using SAS.Model.Time;
using UnityEngine.Networking;

namespace Game.Analytics
{
	public class AnalyticsProvider : ModelListener
	{
		private string _url;

		private Queue<AnalyticsEventTemp> _events = new Queue<AnalyticsEventTemp>();
		private AnalyticJsonParser _jsonParser;

		private Timer _sendTimer;
		private readonly IEngine _engine;

		public AnalyticsProvider(IUpdateManager updateManager, IEngine engine, string url, string apiKey, string userId)
		{
			_engine = engine;
			_sendTimer = new Timer(updateManager, TimeDuration.CreateBySeconds(5), true);
			_sendTimer.SetOnReset(OnTimerReset);
			_sendTimer.Start();
					
			Subscribe(AnalyticsFront.OnEvent, AddEvent);
			Subscribe(AnalyticsFront.OnProperty, AddProperty);
			
			_url = url;

			_jsonParser = new AnalyticJsonParser(apiKey, userId);
		}

		private void OnTimerReset()
		{
			if (_events.Count > 0)
			{
				SendEvents(_events);
				_events.Clear();
			}
		}

		private void AddEvent(AnalyticsEvent analyticsEvent)
		{
			AnalyticsEventTemp analyticsEventTemp = new AnalyticsEventTemp(analyticsEvent.Name,
					new Dictionary<string, object>(analyticsEvent.Arguments));
			if (analyticsEvent.Immediately)
			{
				SendEvents(new[] {analyticsEventTemp});
			}
			else
			{
				_events.Enqueue(analyticsEventTemp);
			}
		}

		private void AddProperty(AnalyticsProperty analyticsProperty)
		{
			
		}

		private void SendEvents(IEnumerable<AnalyticsEventTemp> events)
		{
			_engine.StartCoroutine(SendEventsCoroutine(events));
		}

		private IEnumerator SendEventsCoroutine(IEnumerable<AnalyticsEventTemp> events)
		{
			string jsonString = _jsonParser.Parse(events);
			UnityWebRequest unityWebRequest = UnityWebRequest.Put(_url, Encoding.UTF8.GetBytes(jsonString));
			unityWebRequest.SetRequestHeader("Content-Type", "application/json");
			unityWebRequest.method = UnityWebRequest.kHttpVerbPOST;
			UnityWebRequestAsyncOperation asyncOperation = unityWebRequest.SendWebRequest();
			yield return asyncOperation;
			if (asyncOperation.webRequest.result != UnityWebRequest.Result.Success)
			{
				Log.Error(asyncOperation.webRequest.error);
			}
		}
	}
}