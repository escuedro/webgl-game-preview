﻿namespace Game.Analytics
{
	public static class InAppEvents
	{
		public static string UserId = "user_id";
		public static string TestEvent = "test_event";
		public static string ButtonClick = "button_click";
		public static string Target = "target";
	}
}