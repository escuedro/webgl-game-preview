﻿using System.Collections.Generic;

namespace Game.Analytics
{
	public readonly struct AnalyticsEventTemp
	{
		public readonly string Name;
		public readonly Dictionary<string, object> Arguments;

		public AnalyticsEventTemp(string name, Dictionary<string, object> arguments)
		{
			Name = name;
			Arguments = arguments;
		}
	}
}