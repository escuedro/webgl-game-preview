using System;
using Framework;
using Framework.Client.View;
using TMPro;
using UnityEngine;

namespace Game
{
	public class LoadingView : MonoBehaviour
	{
		[SerializeField]
		private TMP_Text _loadingText;
		[SerializeField]
		private LinkButton _uiButton;

		private Action _onAuth;
		private string _url;

		public void Show(string text)
		{
			_loadingText.text = text;
			gameObject.SetActive(true);
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}

		public void SetAuthorizeAction(Action onAuthorize, string url, bool firstTry)
		{
			_url = url;
			_onAuth = onAuthorize;
			_uiButton.gameObject.SetActive(true);
			_uiButton.SetOnClick(url, HideButtonAndCallAction);
#if UNITY_EDITOR
			if (firstTry)
			{
				HideButtonAndCallAction();
			}
#endif
		}

		private void HideButtonAndCallAction()
		{
			OpenWindowUtils.OpenLinkInNewTab(_url);
			_uiButton.gameObject.SetActive(false);
			_onAuth.Invoke();
		}
	}
}