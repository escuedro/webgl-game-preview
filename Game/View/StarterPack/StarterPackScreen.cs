using System;
using System.Collections;
using Framework;
using Framework.Audio;
using Framework.Client.Translations;
using Framework.FixedMath;
using Framework.Model;
using Framework.RPC;
using Framework.UI;
using Framework.View;
using Game.Audio;
using Game.Translations;
using Game.Windows;
using GameShared;
using SAS.Model;
using SAS.Model.StarterPack;
using Spine.Unity;
using UnityEngine;

namespace Game.View.StarterPack
{
	public class StarterPackScreen : Screen
	{
		[Inject]
		public ITooltipManager TooltipManager;
		[Inject]
		public IClientRpcController ClientRpcController;
		[Inject]
		public UserCharacters UserCharacters;
		[Inject]
		public IWindowManager WindowManager;
		[Inject]
		public IAudioManager AudioManager;
		[Inject]
		public UserChest UserChest;

		[SerializeField]
		private UIText _tokenPrice;
		[SerializeField]
		private GameObject _tooltip;
		[SerializeField]
		private UIButton _buyButton;
		[SerializeField]
		private CanvasGroup _buyGroup;
		[SerializeField]
		private CanvasGroup _rewardGroup;
		[SerializeField]
		private RectTransform _windowRect;
		[SerializeField]
		private Vector2 _rewardSize;
		[SerializeField]
		private float _fadeTime;
		[SerializeField]
		private float _resizeTime;
		[SerializeField]
		private UIText _title;
		[SerializeField]
		private TranslatedText _titleTranslatedText;
		[SerializeField]
		private UIText _nft;
		[SerializeField]
		private TranslatedText _characterName;
		[SerializeField]
		private TranslatedText _element;
		[SerializeField]
		private CharacterView _characterView;
		[SerializeField]
		private UIButton _getButton;
		[SerializeField]
		private GameObject _eterIcon;

		[SerializeField]
		private SkeletonGraphic _chestGraphics;
		[SerializeField]
		private AnimationReferenceAsset _openAnimation;
		[SerializeField]
		private AnimationReferenceAsset _idleAnimation;

		private static ResourcePath ClosedChest => Resource.UI.Items.ClosedChestPng;
		private static ResourcePath OpenedChest => Resource.UI.Items.OpenChestPng;
		
		private uint _priceInTokens;

		public void Init(IScreenChanger screenChanger)
		{
			TooltipManager.BindTooltip(_tooltip, TranslationKeys.STARTER_PACK_DESCRIPTION);
			TooltipManager.BindTooltip(_eterIcon, TranslationKeys.TOKENS);
			_buyButton.SetOnClick(StartBuy);
			_getButton.SetOnClick(() =>
			{
				Disposed = true;
				screenChanger.Open<StarterPackScreen>().Init(screenChanger);
			});
			_priceInTokens = Statics.GetSingle<StarterPackStatic>().PriceInTokens;
			_tokenPrice.text = _priceInTokens.ToString();
		}

		private void StartBuy()
		{
			if (UserChest.TokensAvailable >= (Fix64)_priceInTokens)
			{
				_buyButton.enabled = false;
				ClientRpcController.Execute(new BuyStarterPackCommand()).Then(OnSuccessBuy, OnFail);
			}
			else
			{
				WindowManager.Open<NotEnoughMoneyWindow>();
			}
		}

		private void OnSuccessBuy()
		{
			_chestGraphics.freeze = false;
			_chestGraphics.AnimationState.SetAnimation(1, _openAnimation, false);
			AudioManager.PlaySound(SoundType.SoundChestOpen);
			AudioManager.PlaySound(SoundType.SoundStarterPack);
			CreateCharacterCommand createCharacterCommand = new CreateCharacterCommand();
			createCharacterCommand.OnResponse<CreateCharacter.Response>().Then(OnSuccessCreateCharacter, OnFail);
			ClientRpcController.Execute(createCharacterCommand).Then(StartAnimate, OnFail);
		}

		private void OnSuccessCreateCharacter(CreateCharacter.Response response)
		{
			SetNewCharacter(UserCharacters.TryGet(response.CharacterDto.Id));
		}

		private void SetNewCharacter([CanBeNull] Character character)
		{
			if (character == null)
			{
				return;
			}
			_characterName.SetTranslationKey(character.TranslationKey);
			TranslationKey elementKey;
			switch (character.Element)
			{
				case Element.Earth:
					elementKey = TranslationKeys.EARTH;
					break;
				case Element.Fire:
					elementKey = TranslationKeys.FIRE;
					break;
				case Element.Air:
					elementKey = TranslationKeys.AIR;
					break;
				case Element.Water:
					elementKey = TranslationKeys.WATER;
					break;
				default:
					elementKey = TranslationKeys.EARTH;
					break;
			}
			switch (character.Rarity)
			{
				case CharacterRarity.Undefined:
					break;
				case CharacterRarity.Common:
					AudioManager.PlaySound(SoundType.SoundRarity1);
					break;
				case CharacterRarity.Uncommon:
					AudioManager.PlaySound(SoundType.SoundRarity2);
					break;
				case CharacterRarity.Rare:
					AudioManager.PlaySound(SoundType.SoundRarity3);
					break;
				case CharacterRarity.Epic:
					AudioManager.PlaySound(SoundType.SoundRarity4);
					break;
				case CharacterRarity.Legendary:
					AudioManager.PlaySound(SoundType.SoundRarity5);
					break;
				case CharacterRarity.Mythical:
					AudioManager.PlaySound(SoundType.SoundRarity6);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			_element.SetTranslationKey(elementKey);
			_characterView.Init(character, TooltipManager);
			string guid = character.Guid.ToString();
			_nft.text = guid;
			TooltipManager.BindTooltip(_nft.gameObject, guid);
		}

		private void StartAnimate()
		{
			Disposed = true;
			StartCoroutine(AnimateReward());
			Log.Info("Great success!");
		}

		private IEnumerator AnimateReward()
		{
			float time = 0;
			float way = 0;
			_buyGroup.blocksRaycasts = false;
			Color titleColor = _title.color;
			while (way < _fadeTime)
			{
				time += Time.deltaTime;
				way = time / _fadeTime;
				_buyGroup.alpha = 1f - way;
				titleColor.a = 1f - way;
				_title.color = titleColor;
				yield return null;
			}

			_buyGroup.alpha = 0;
			titleColor.a = 0;
			_title.color = titleColor;
			Vector2 startSize = _windowRect.sizeDelta;
			time = 0;
			way = 0;
			while (way < _resizeTime)
			{
				time += Time.deltaTime;
				way = time / _fadeTime;
				_windowRect.sizeDelta = Vector2.Lerp(startSize, _rewardSize, way);
				yield return null;
			}
			_windowRect.sizeDelta = _rewardSize;

			_titleTranslatedText.SetTranslationKey(TranslationKeys.REWARD);
			time = 0;
			way = 0;
			while (way < _fadeTime)
			{
				time += Time.deltaTime;
				way = time / _fadeTime;
				titleColor.a = way;
				_rewardGroup.alpha = way;
				_title.color = titleColor;
				yield return null;
			}
			_rewardGroup.blocksRaycasts = true;
			_rewardGroup.alpha = 1;
			titleColor.a = 1;
			_title.color = titleColor;
		}

		private void OnFail(Exception reason)
		{
			_buyButton.enabled = true;
			Log.Exception(reason);
		}
	}
}