using Framework;
using Framework.Futures;
using Framework.RPC;
using Framework.UI;
using UnityEngine;

namespace Game.View.Logout
{
	public class LogoutConfirmWindow : Window, IFutureHandler
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private UIButton _logoutButton;
		[SerializeField]
		private UIButton _closeButton;
		private IWindowAnimation _animation;
		private IClientRpcController _clientRpcController;
		private IPrefabProvider _prefabProvider;

		private void Awake()
		{
			_animation = new FadeInOutAnimation(this, _canvasGroup);
			_closeButton.SetOnClick(Close);
		}

		[Inject]
		public void Inject(IClientRpcController clientRpcController, IPrefabProvider prefabProvider)
		{
			_clientRpcController = clientRpcController;
			_logoutButton.SetOnClick(OnLogoutStart);
			_prefabProvider = prefabProvider;
		}

		private void OnLogoutStart()
		{
			IFuture logoutFuture = _clientRpcController.ClientAuth.TryLogout();
			logoutFuture.Finally(this);
			_logoutButton.enabled = false;
		}

		public override IFuture OnClose()
		{
			return _animation.PlayOut();
		}

		public override IFuture Open()
		{
			return _animation.PlayIn();
		}

		public void HandleFuture(IFuture future)
		{
			_clientRpcController.SetEnabled(false);
			_clientRpcController.ClientAuth.ClearStoredTokens();
			_prefabProvider.Instantiate(Resource.View.Loading.LoggedOutViewPrefab);
		}
	}
}