﻿#if !NOT_UNITY3D

using System;
using System.Collections;
using System.Linq;
using Framework;
using Game.View.Animations;
using Game.View.Animations.VFX;
using UnityEngine;

namespace Game.View
{
	[RequireComponent(typeof(Animator))]
	[RequireComponent(typeof(CharacterBattleEffectHolder))]
	public class ResourceMecanimAnimation : MonoBehaviour
	{
		[SerializeField]
		private Animator _animator;
		public Animator Animator => _animator;

		[SerializeField]
		private CharacterBattleEffectHolder _battleEffectHolder;
		public CharacterBattleEffectHolder BattleEffectHolder => _battleEffectHolder;

		private event Action OnAnimationAttackEvent;

		private VFX CreateEffectInstance(VFX effectPrefab,
				Transform instanceOwner,
				Vector3 position,
				Quaternion rotation)
		{
			VFX vfx = Instantiate(effectPrefab, position, rotation, instanceOwner);
			return vfx;
		}

		public IEnumerator Play(string trigger, Action onAttackedEvent = null)
		{
			OnAnimationAttackEvent = onAttackedEvent;
			_animator.SetTrigger(trigger);

			AnimationClip requiredClip =
					_animator.runtimeAnimatorController.animationClips.FirstOrDefault(clip => clip.name == trigger);
			if (requiredClip == null)
			{
				Log.Info("Clip not found: " + trigger);
				yield return new WaitForEndOfFrame(); //todo: quality gate
				//throw new NullReferenceException();
			}
			else
			{
				yield return new WaitForSeconds(requiredClip.length);
			}
		}

		public IEnumerator PlayAttack(Transform attackedPointTransform, Action onDamageDone)
		{
			// ReSharper disable once HeapView.ClosureAllocation
			string trigger = "attack";

			if (_battleEffectHolder.AttackVfx != null)
			{
				if (_battleEffectHolder.AttackVfx.VFXType == VFXType.Moving)
				{
					OnAnimationAttackActionMoving(attackedPointTransform.position, onDamageDone);
				}
				else
				{
					OnAnimationAttackActionStatic(attackedPointTransform.position, onDamageDone);
				}
			}
			else
			{
				if (_battleEffectHolder.AttackedVfx != null)
				{
					StartCoroutine(ExecuteWithDelay(_battleEffectHolder.AttackedVfxDelay, () =>
					{
						var effectInstance = CreateEffectInstance(_battleEffectHolder.AttackedVfx, transform,
								attackedPointTransform.position,
								_battleEffectHolder.AttackedVfxPoint.rotation);
						Debug.Log("hit");
						effectInstance.Play();
						onDamageDone.Invoke();
					}));
				}
				else
				{
					StartCoroutine(ExecuteWithDelay(_battleEffectHolder.AttackVfxDelay, onDamageDone));
				}
			}

			_animator.SetTrigger(trigger);

			AnimationClip requiredClip = GetClipByName(trigger);
			if (requiredClip != null)
			{
				yield return WaitForClipEnd(requiredClip);
			}
			else
			{
				Log.Info("Clip not found: " + trigger);
				yield return new WaitForEndOfFrame(); //todo: quality gate
				//throw new NullReferenceException();
			}
		}

		private void OnAnimationAttackActionStatic(Vector3 positionTo, Action onDamageDone)
		{
			StartCoroutine(ExecuteWithDelay(_battleEffectHolder.AttackVfxDelay, () =>
			{
				var effect = CreateEffectInstance(_battleEffectHolder.AttackVfx, transform,
						_battleEffectHolder.AttackVfxPoint.position, _battleEffectHolder.AttackVfxPoint.rotation);
				if (_battleEffectHolder.AttackedVfx != null)
				{
					effect.OnVFXHitEvent = () =>
					{
						StartCoroutine(ExecuteWithDelay(_battleEffectHolder.AttackedVfxDelay, () =>
						{
							var effectInstance = CreateEffectInstance(_battleEffectHolder.AttackedVfx, transform,
									positionTo,
									_battleEffectHolder.AttackedVfxPoint.rotation);
							Debug.Log("hit");
							effectInstance.Play();
							onDamageDone.Invoke();
						}));
					};
				}
				else
				{
					onDamageDone.Invoke();
				}
				Debug.Log("attack");
				effect.Play();
			}));
		}

		private void OnAnimationAttackActionMoving(Vector3 positionTo, Action onDamageDone)
		{
			StartCoroutine(ExecuteWithDelay(_battleEffectHolder.AttackVfxDelay, () =>
			{
				var position = _battleEffectHolder.AttackVfxPoint.position;
				MovingEffect effect = (MovingEffect)CreateEffectInstance(_battleEffectHolder.AttackVfx, transform,
						position,
						_battleEffectHolder.AttackVfxPoint.rotation);
				effect.Init(position, positionTo, () =>
				{
					if(_battleEffectHolder.AttackedVfx != null)
					{
						var effectInstance = CreateEffectInstance(_battleEffectHolder.AttackedVfx, transform,
								positionTo,
								_battleEffectHolder.AttackedVfxPoint.rotation);
						effectInstance.Play();
					}
					onDamageDone.Invoke();
				});
				effect.Play();
			}));
		}

		private IEnumerator ExecuteWithDelay(float delay, Action onComplete)
		{
			yield return new WaitForSeconds(delay);
			onComplete.Invoke();
		}

		private IEnumerator WaitForClipEnd(AnimationClip clip)
		{
			yield return new WaitForSeconds(clip.length);
		}

		private AnimationClip GetClipByName(string clipName) =>
				_animator.runtimeAnimatorController.animationClips.FirstOrDefault(clip => clip.name == clipName);

		public void AnimationEvent()
		{
		}

		private void Reset()
		{
			if (!TryGetComponent(out _battleEffectHolder))
			{
				_battleEffectHolder = gameObject.AddComponent<CharacterBattleEffectHolder>();
			}
			if (!TryGetComponent(out _animator))
			{
				_animator = gameObject.AddComponent<Animator>();
			}
		}
	}
}

#endif