﻿namespace Game.View
{
	public interface IScreenChanger
	{
		public TScreen Open<TScreen>() where TScreen : Screen;
	}
}