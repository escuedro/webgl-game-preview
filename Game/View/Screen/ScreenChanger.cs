using System;
using System.Collections;
using System.Collections.Generic;
using Framework;
using Framework.Reactive;
using UnityEngine;

namespace Game.View
{
	public class ScreenChanger : MonoBehaviour, IScreenChanger
	{
		[SerializeField]
		private float _fadeInTime;
		[SerializeField]
		private float _fadeOutTime;

		[Inject]
		public IPrefabProvider PrefabProvider;
		[Inject]
		public IEngine Engine;

		private readonly Dictionary<Type, Screen> _screens = new Dictionary<Type, Screen>();
		private Screen _active;
		private IEnumerator _activeEnumerator;
		private Coroutine _activeCoroutine;

		public Field<Screen> CurrentScreen = new Field<Screen>();

		public TScreen Open<TScreen>() where TScreen : Screen
		{
			return (TScreen)Open(typeof(TScreen));
		}

		private Screen Open(Type screenType)
		{
			if (!_screens.TryGetValue(screenType, out Screen nextScreen))
			{
				nextScreen = (Screen)PrefabProvider.Instantiate(screenType, true);
				HideImmediately(nextScreen);
				_screens.Add(screenType, nextScreen);
			}
			if (nextScreen.IsDisposed)
			{
				if (nextScreen == _active)
				{
					HideImmediately(_active);
					_active = null;
				}
				Destroy(nextScreen.gameObject);
				nextScreen = (Screen)PrefabProvider.Instantiate(screenType, true);
				HideImmediately(nextScreen);
				_screens[screenType] = nextScreen;
			}
			if (_active == nextScreen)
			{
				return nextScreen;
			}
			if (_active == null)
			{
				ForceComplete();
				_active = nextScreen;
				_activeEnumerator = Show(nextScreen);
				_activeCoroutine = Engine.StartCoroutine(_activeEnumerator);
			}
			else
			{
				ForceComplete();
				_activeEnumerator = Swap(nextScreen);
				_activeCoroutine = Engine.StartCoroutine(_activeEnumerator);
			}
			CurrentScreen.Value = nextScreen;
			return nextScreen;
		}

		private void ForceComplete()
		{
			if (_activeEnumerator != null)
			{
				int leftIterations = 100;
				while (_activeEnumerator.MoveNext() && leftIterations-- > 0)
				{
				}
				_activeEnumerator = null;
			}
			if (_activeCoroutine != null)
			{
				Engine.StopCoroutine(_activeCoroutine);
				_activeCoroutine = null;
			}
		}

		private IEnumerator Swap(Screen next)
		{
			yield return Hide(_active);
			_active = next;
			yield return Show(next);
		}

		private IEnumerator Hide(Screen screen)
		{
			float time = 0;
			while (time < _fadeOutTime)
			{
				time += Time.deltaTime;
				float progress = time / _fadeOutTime;
				screen.CanvasGroup.alpha = 1 - progress;
				yield return null;
			}
			HideImmediately(screen);
		}

		private IEnumerator Show(Screen screen)
		{
			float time = 0;
			screen.Canvas.enabled = true;
			try
			{
				screen.OnStartVisible();
			}
			catch (Exception e)
			{
				Log.Exception(e, screen);
			}
			while (time < _fadeInTime)
			{
				time += Time.deltaTime;
				float progress = time / _fadeInTime;
				screen.CanvasGroup.alpha = progress;
				yield return null;
			}
			screen.CanvasGroup.alpha = 1;
			screen.CanvasGroup.blocksRaycasts = true;

			try
			{
				screen.OnCompleteVisible();
			}
			catch (Exception e)
			{
				Log.Exception(e, screen);
			}
		}

		private void HideImmediately(Screen screen)
		{
			screen.Canvas.enabled = false;
			screen.CanvasGroup.blocksRaycasts = false;
			screen.CanvasGroup.alpha = 0;
		}
	}
}