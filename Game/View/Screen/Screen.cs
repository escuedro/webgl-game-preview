using System;
using Framework.View;
using UnityEngine;

namespace Game.View
{
	public abstract class Screen : ViewBehaviour
	{
		[SerializeField]
		private Canvas _canvas;
		[SerializeField]
		private CanvasGroup _canvasGroup;

		public CanvasGroup CanvasGroup => _canvasGroup;
		public Canvas Canvas => _canvas;
		protected bool Disposed = false;

		public bool IsDisposed => Disposed;

		private void Reset()
		{
			TryGetComponent(out _canvas);
			TryGetComponent(out _canvasGroup);
		}

		public virtual void OnStartVisible()
		{
		}
		public virtual void OnCompleteVisible()
		{
		}
	}
}