namespace Game.View
{
	public class PanelGroup
	{
		private PanelButton[] _buttons;

		public PanelGroup(params PanelButton[] buttons)
		{
			_buttons = buttons;
		}

		public void Select(PanelButton panelButton)
		{
			foreach (PanelButton button in _buttons)
			{
				button.SetSelected(button == panelButton);
			}
		}
	}
}