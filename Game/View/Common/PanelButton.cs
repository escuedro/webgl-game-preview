using System;
using Framework;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View
{
	public class PanelButton : MonoBehaviour
	{
		private static readonly Color SelectedColor = new Color(1, 0.7333333f, 0.1137255f);
		private static readonly Color DefaultColor = Color.white;

		[SerializeField]
		private UIButton _button;
		[SerializeField]
		private Image _icon;
		[SerializeField]
		private UIText _text;

		private PanelGroup _panelGroup;
		private Action _onClick;

		public RectTransform RectTransform
		{
			get
			{
				if (_rectTransform == null)
				{
					TryGetComponent(out _rectTransform);
				}
				return _rectTransform;
			}
		}

		private RectTransform _rectTransform;

		public void Init(Action onClick, PanelGroup panelGroup)
		{
			_onClick = onClick;
			_panelGroup = panelGroup;
			_button.SetOnClick(Click);
		}

		private void Click()
		{
			_onClick.Invoke();
			_panelGroup.Select(this);
		}

		public void SetSelected(bool selected)
		{
			_icon.color = selected ? SelectedColor : DefaultColor;
			_text.color = selected ? SelectedColor : DefaultColor;
		}
	}
}