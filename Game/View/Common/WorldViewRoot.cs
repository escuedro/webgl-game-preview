﻿using UnityEngine;

namespace Game.View
{
	public class WorldViewRoot : MonoBehaviour
	{
		public Transform Transform => transform;
	}
}