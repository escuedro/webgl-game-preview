using Framework;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.View
{
	public class ButtonAnimatedText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		[SerializeField]
		private UIText _text;
		[SerializeField]
		private Color _highlightColor = Color.white;
		[SerializeField]
		private Color _normalColor;

		private void Reset()
		{
			_text = GetComponentInChildren<UIText>();
			_normalColor = _text.color;
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			_text.color = _highlightColor;
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			_text.color = _normalColor;
		}
	}
}