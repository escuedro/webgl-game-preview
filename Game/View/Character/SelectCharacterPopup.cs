﻿using System;
using Framework.View;
using SAS.Model;
using UnityEngine;

namespace Game.View
{
	public class SelectCharacterPopup : ViewBehaviour
	{
		[SerializeField]
		private UIButton _selectButton;
		
		public Character Character { get; private set; }

		public void Init(Character character, Action<Character> onSelectClicked)
		{
			Character = character;
			_selectButton.SetOnClick(() => onSelectClicked?.Invoke(character));
		}
	}
}