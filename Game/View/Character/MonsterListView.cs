using System.Collections.Generic;
using Framework;
using Framework.Client.View;
using Framework.View;
using Game.Command;
using GameShared;
using SAS.Model;
using UnityEngine;

namespace Game.View
{
	public class MonsterListView : ViewBehaviour
	{
		[SerializeField]
		private GridViewBehaviour _gridView;
		[SerializeField]
		private UIButton _generateMonstersButton;

		private IController _controller;

		[Inject]
		public void Inject(UserMonsters userMonsters, IPrefabProvider prefabProvider, IController controller)
		{
			_controller = controller;
			bool noMonsters = userMonsters.Monsters == null || userMonsters.Monsters.Count == 0;
			if (noMonsters)
			{
				_controller.Execute(new SetCurrentChosenMonsterCommand(null));
			}
			_generateMonstersButton.gameObject.SetActive(noMonsters);
			_generateMonstersButton.SetOnClick(OnGenerateMonstersButtonClick);
			_gridView.Init<Monster, MonsterListItem>(prefabProvider, userMonsters.Monsters, false,
					(monster, monsterListItem) => monsterListItem.Init(monster, OnMonsterClicked));
			UpdateSubscriptions();
		}

		private void OnGenerateMonstersButtonClick()
		{
			_controller.Execute(new GenerateMonstersCommand(3));
		}

		private void OnMonsterClicked(Monster monster)
		{
			
		}
	}
}