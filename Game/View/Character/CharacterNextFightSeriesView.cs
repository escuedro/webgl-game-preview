﻿using Framework;
using Framework.Client;
using Framework.Client.Timer;
using Framework.Utils;
using Framework.View;
using Game.Service.Visibility;
using SAS.Model;
using SAS.Model.Time;
using UnityEngine;

namespace Game.View
{
	public class CharacterNextFightSeriesView : ViewBehaviour
	{
		[SerializeField]
		private UIText _timerText;

		private Timer _timer;
		private IUpdateManager _updateManager;
		private IVisibilityListener _visibilityListener;
		private Character _character;

		[Inject]
		public void Inject(IUpdateManager updateManager, IVisibilityListener visibilityListener)
		{
			_updateManager = updateManager;
			Subscribe(visibilityListener.OnGameVisible, OnGameVisibleChanged);
		}

		private void OnGameVisibleChanged(bool visible)
		{
			if (visible)
			{
				UpdateTimer();
			}
		}

		public void Init(Character character)
		{
			_character = character;
			Subscribe(character.NextFightSeries, OnChangeNextFightSeries);

			uint secondsLeft = (character.NextFightSeries.Value - TimePoint.Now).Seconds;
			gameObject.SetActive(true);
			_timer = new Timer(_updateManager, TimeDuration.CreateBySeconds(secondsLeft), false);
			_timer.SetOnReset(() => gameObject.SetActive(false));
			Subscribe(_timer.SecondsLeft, OnSecondsChanged);
			_timer.Start();
		}

		private void OnChangeNextFightSeries(TimePoint newTime)
		{
			TimeDuration timerDuration = (newTime - TimePoint.Now);
			_timer.SetSecondsLeft(timerDuration);
		}

		private void UpdateTimer()
		{
			uint secondsLeft = (_character.NextFightSeries.Value - TimePoint.Now).Seconds;
			_timer.SetSecondsLeft(TimeDuration.CreateBySeconds(secondsLeft));
		}

		private void OnSecondsChanged(int secondsLeft)
		{
			_timerText.text = StringUtils.DurationFormat(secondsLeft);
		}
	}
}