﻿using Framework;
using Framework.View;
using SAS.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View
{
	public class CharacterExperienceBarView : ViewBehaviour
	{
		[SerializeField]
		private Slider _slider;
		[SerializeField]
		private UIText _levelText;
		[SerializeField]
		private UIText _requiredExperienceText;

		private Character _character;

		public void Init(Character character)
		{
			_character = character;
			ClearSubscriptions();
			Subscribe(character.Level, OnLevelChanged).Update();
			Subscribe(character.CurrentExperience, OnExperienceChanged).Update();
		}

		private void OnLevelChanged(int currentLevel)
		{
			_levelText.text = currentLevel.ToString();
		}

		private void OnExperienceChanged(long currentExperience)
		{
			long experienceToNextLevel = _character.LevelStatic.ExperienceToNextLevel;
			_requiredExperienceText.text = $"{currentExperience.ToString()}/{experienceToNextLevel.ToString()}";
			float experienceNormalized = (float)currentExperience / experienceToNextLevel;
			_slider.value = experienceNormalized;
		}
	}
}