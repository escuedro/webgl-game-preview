﻿using Framework;
using Framework.Client.Translations;
using Framework.Model;
using Framework.UI;
using Framework.View;
using SAS.Model;
using UnityEngine;

namespace Game.View
{
	public class CharacterView : ViewBehaviour
	{
		[Header("Data")]
		[SerializeField]
		private UIText _idText;
		[SerializeField]
		private TranslatedText _characterName;
		[SerializeField]
		private TranslatedText _characterDescription;
		[Header("Graphics")]
		[SerializeField]
		private ResourceImage _rarityFrameImage;
		[SerializeField]
		private ResourceImage _rarityTitleImage;
		[SerializeField]
		private ResourceImage _rarityDecorImage;
		[SerializeField]
		private ResourceImage _elementFlagImage;
		[SerializeField]
		private ResourceImage _backgroundImage;
		[SerializeField]
		private ResourceImage _cardGraphics;
		[Header("Bar")]
		[SerializeField]
		private CharacterExperienceBarView _experienceBar;
		[Header("Characteristics")]
		[SerializeField]
		private UIText _powerText;
		[SerializeField]
		private UIText _defenceText;
		[SerializeField]
		private UIText _attackText;
		[SerializeField]
		private UIText _luckText;

		private Character _character;

		public void Init(Character character, ITooltipManager tooltipManager)
		{
			if (_character != null)
			{
				ClearSubscriptions(_character.Level);
			}
			_character = character;
			_characterName.SetTranslationKey(character.TranslationKey);
			_characterDescription.SetTranslationKey(character.DescriptionTranslationKey);
			UpdateGraphics(character);
			string guid = character.Guid.ToString();
			_idText.text = character.Guid.ToString();
			tooltipManager.BindTooltip(_idText.gameObject, guid);
			_experienceBar.Init(character);

			CharacterSharedStatic characterSharedStatic = Statics.GetSingle<CharacterSharedStatic>();
			_powerText.text = characterSharedStatic.Power[character.Level.Value-1].ToString();
			_defenceText.text = character.Static.Defence.ToString();
			_attackText.text = character.Static.Attack.ToString();
			_luckText.text = character.Static.Luck.ToString();
		}

		private void UpdateGraphics(Character character)
		{
			int characterRarity = (int)character.Rarity - 1;
			int characterElement = (int)character.Element - 1;
			CharacterSharedStatic characterSharedStatic = Statics.GetSingle<CharacterSharedStatic>();
			_rarityFrameImage.Load(characterSharedStatic.FrameVisualByRarity[characterRarity]);
			_rarityTitleImage.Load(characterSharedStatic.TitleVisualByRarity[characterRarity]);
			_rarityDecorImage.Load(characterSharedStatic.DecorRarityByVisual[characterRarity]);
			_elementFlagImage.Load(characterSharedStatic.FlagVisualByElement[characterElement]);
			_backgroundImage.Load(characterSharedStatic.BackgroundByRarity[characterRarity]);
			_cardGraphics.Load(character.CardVisual);
		}
	}
}