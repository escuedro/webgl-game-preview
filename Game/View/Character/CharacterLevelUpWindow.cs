﻿using Framework.Client.Translations;
using Framework.Futures;
using Framework.UI;
using Game.View.Items;
using SAS.Model;
using UnityEngine;

namespace Game.View
{
	public class CharacterLevelUpWindow : Window<Character, ITooltipManager>
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private UIButton _closeButton;
		[SerializeField]
		private CharacterView _characterView;

		private IWindowAnimation _animation;

		private void Awake()
		{
			_animation = new FadeInOutAnimation(this, _canvasGroup);
			_closeButton.SetOnClick(OnCloseButtonClick);
		}

		private void OnCloseButtonClick()
		{
			Close();
		}

		public void Init(Character character, ITooltipManager tooltipManager)
		{
			_characterView.Init(character, tooltipManager);
		}

		public override IFuture OnClose()
		{
			return _animation.PlayOut();
		}

		protected override IFuture Open(Character character, ITooltipManager tooltipManager)
		{
			Init(character, tooltipManager);
			return _animation.PlayIn();
		}
	}
}