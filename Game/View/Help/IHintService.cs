﻿using UnityEngine;

namespace Game.View.Help
{
	public interface IHintService
	{
		public void BindHint(HintType hintType, RectTransform rectTransform);
		public void ShowHints(params HintType[] hintTypes);
		public void HideHints();
	}
}