﻿namespace Game.View.Help
{
	public enum HintType
	{
		None = 0,
		StarterPack = 1,
		Marketplace = 2,
		Inventory = 3,
		Transmute = 4,
		BattleVsMonster = 5,
		BossAttack = 6
	}
}