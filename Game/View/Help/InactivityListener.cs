﻿using System;
using Framework;
using Framework.Client;
using Framework.Client.Timer;
using Framework.Reactive;
using Game.View.Battle;
using Game.View.Boss;
using SAS.Model.Time;
using UnityEngine;

namespace Game.View.Help
{
	public class InactivityListener : ModelListener, IUpdatable
	{
		private Timer _inactivityInScreenTimer;
		private TimeDuration _timerDuration;

		private bool _isScreenSelected = false;
		private Screen _selectedScreen;
		
		private const float InputDistancePrecision = 0.5f;

		private Vector2 _lastMousePosition;
		private Action<Screen> _onInScreenSelected;
		
		private IUpdateManager _updateManager;
		private bool _needToUpdate;
		
		private readonly Timer _inactivityWithoutScreenTimer;

		public InactivityListener(
				IUpdateManager updateManager,
				ScreenChanger screenChanger,
				float inactivityTimerDuration,
				Action<Screen> onInScreenInactive,
				Action onNoScreenActive)
		{
			_updateManager = updateManager;
			_updateManager.StartUpdate(this, UpdateType.ByUpdate);
			_onInScreenSelected = onInScreenInactive;
			_timerDuration =  TimeDuration.CreateBySeconds(inactivityTimerDuration);
			_inactivityInScreenTimer = new Timer(_updateManager, _timerDuration, false);
			_inactivityInScreenTimer.SetOnReset(OnTimerReset);
			_inactivityInScreenTimer.Start();
			Subscribe(screenChanger.CurrentScreen, OnScreenChanged);

			_inactivityWithoutScreenTimer = new Timer(updateManager, _timerDuration, false);
			_inactivityWithoutScreenTimer.SetOnReset(() => onNoScreenActive?.Invoke());
			_inactivityWithoutScreenTimer.Start();
		}

		private void OnTimerReset()
		{
			if (_isScreenSelected)
			{
				_onInScreenSelected?.Invoke(_selectedScreen);
			}
		}

		public void Update()
		{
			if (_isScreenSelected)
			{
				Vector2 currentMousePosition = Input.mousePosition;
				if (Vector2.Distance(currentMousePosition, _lastMousePosition) > InputDistancePrecision)
				{
					_inactivityInScreenTimer.SetSecondsLeft(_timerDuration);
				}
				_lastMousePosition = currentMousePosition;
			}
		}

		private void OnScreenChanged(Screen screen)
		{
			_selectedScreen = screen;
			if (screen is BossBattleSceneScreen || screen is BattleScreen)
			{
				_isScreenSelected = false;
				return;
			}
			if (screen != null)
			{
				_inactivityWithoutScreenTimer.Stop();
				_inactivityInScreenTimer.SetSecondsLeft(_timerDuration);
				_inactivityInScreenTimer.Start();
				_isScreenSelected = true;
			}
		}
	}
}