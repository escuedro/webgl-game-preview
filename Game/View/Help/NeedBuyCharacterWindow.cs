﻿using Framework;
using Framework.Futures;
using Framework.UI;
using UnityEngine;

namespace Game.View.Help
{
	public class NeedBuyCharacterWindow : Window
	{
		[SerializeField]
		private UIButton _okButton;
		
		private IWindowAnimation _animation;
		private IHintService _hintService;

		[Inject]
		public void Inject(IHintService hintService)
		{
			_hintService = hintService;
		}

		private void Awake()
		{
			_animation = new ScaleAnimation(this, transform, 0.2f);
			_okButton.SetOnClick(Close);
		}

		public override IFuture OnClose()
		{
			_hintService.ShowHints(HintType.StarterPack);
			return _animation.PlayOut();
		}

		public override IFuture Open()
		{
			return _animation.PlayIn();
		}
	}
}