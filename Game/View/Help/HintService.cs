﻿using System.Collections.Generic;
using Framework;
using UnityEngine;

namespace Game.View.Help
{
	[Bind(BindTo = typeof(IHintService))]
	public class HintService : IHintService
	{
		private Dictionary<HintType, RectTransform> _hintTransforms = new Dictionary<HintType, RectTransform>();
		private List<RectTransform> _activeHints = new List<RectTransform>();
		
		private readonly IPrefabProvider _prefabProvider;

		[Inject]
		public HintService(IPrefabProvider prefabProvider)
		{
			_prefabProvider = prefabProvider;
		}

		public void BindHint(HintType hintType, RectTransform rectTransform)
		{
			_hintTransforms.Add(hintType, rectTransform);
		}

		public void ShowHints(HintType[] hintTypes)
		{
			HideHints();
			foreach (HintType hintType in hintTypes)
			{
				_activeHints.Add(InstantiateHintView(hintType));
			}
		}

		public void HideHints()
		{
			foreach (RectTransform activeHint in _activeHints)
			{
				Object.Destroy(activeHint.gameObject);
			}
			_activeHints.Clear();
		}

		private RectTransform InstantiateHintView(HintType hintType)
		{
			RectTransform hintRectTransform = _hintTransforms[hintType];
			_prefabProvider.InstantiateAt(Resource.View.Help.HintViewPrefab, hintRectTransform.transform).
					TryGetComponent(out RectTransform hintRect);
			return hintRect;
		}
	}
}