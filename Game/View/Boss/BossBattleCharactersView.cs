﻿using Framework;
using Framework.Client.View;
using Framework.View;
using SAS.Model;
using Spine.Unity;
using UnityEngine;

namespace Game.View.Boss
{
	public class BossBattleCharactersView : ViewBehaviour
	{
		[SerializeField]
		private GameObject[] _charactersPoints;
		[SerializeField]
		private int[] _sortingLayers;
		[SerializeField]
		private GameObject _enemyPoint;

		private ResourceMecanimAnimation[] _characterMecanimAnimations;
		public ResourceMecanimAnimation[] CharacterMecanimAnimations => _characterMecanimAnimations;

		private ResourceMecanimAnimation _enemyMecanimAnimation;
		public ResourceMecanimAnimation EnemyMecanimAnimation => _enemyMecanimAnimation;

		public void Init(IPrefabProvider prefabProvider, CharacterStatic[] characterStatic, BossStatic monsterStatic)
		{
			_characterMecanimAnimations = new ResourceMecanimAnimation[characterStatic.Length];
			for (int i = 0; i < characterStatic.Length; i++)
			{
				_characterMecanimAnimations[i] = prefabProvider.InstantiateAt(new ResourcePath(characterStatic[i].PrefabResourcePath), _charactersPoints[i].transform).GetComponent<ResourceMecanimAnimation>();
				_characterMecanimAnimations[i].GetComponent<MeshRenderer>().sortingOrder = _sortingLayers[i];
			}
			_enemyMecanimAnimation = prefabProvider.InstantiateAt(new ResourcePath(monsterStatic.PrefabResourcePath), _enemyPoint.transform).GetComponent<ResourceMecanimAnimation>();
		}
	}
}