﻿using System;
using System.Collections.Generic;
using Framework;
using Framework.Audio;
using Framework.Client.View;
using Framework.Collections;
using Framework.Futures;
using Framework.Reactive;
using Framework.RPC;
using Framework.UI;
using Game.Audio;
using Game.Command;
using Game.Model;
using Game.Translations;
using Game.View.Items;
using Game.View.Transmutation;
using GameShared;
using SAS.Model;
using SAS.Model.User;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View.Boss
{
	public class BossBattleScreen : Screen
	{
		[SerializeField]
		private UIButton _battleHistoryButton;
		[SerializeField]
		private ActivatableImage _battleHistoryActivatableImage;
		[SerializeField]
		private Toggle _skipAnimationToggle;
		[SerializeField]
		private ClickableItemView _ticketItemView;
		[Header("Characters")]
		[SerializeField]
		private UIButton[] _selectCharacterButtons;
		[SerializeField]
		private CharacterView[] _characterViews;
		[Header("Bosses")]
		[SerializeField]
		private GridViewBehaviour _bossesRoot;

		private MainUiView _mainUiView;
		private ScreenChanger _screenChanger;
		private IFuture<Character> _characterSelectionFuture;

		private IWindowManager _windowManager;
		private ITooltipManager _tooltipManager;
		private IController _controller;
		private IPrefabProvider _prefabProvider;
		private IAudioManager _audioManager;

		private Dictionary<int, Character> _selectedCharacters = new Dictionary<int, Character>();
		private ConstArray<BossDamageDto> _bossDamageDtos;

		private ClientBosses _clientBosses;
		private IFuture _endBattleFuture;

		private GameObject _mainBackground;
		private UserSettings _userSettings;
		private BossSelectionModel _bossSelectionModel;
		private Item _ticketItem;

		private long _aliveBossId;
		private ConstArray<BossUserRating> _topHundred;
		private BossPlayerRating _playerRating;

		[Inject]
		public void Inject(
				IWindowManager windowManager,
				ITooltipManager tooltipManager,
				IPrefabProvider prefabProvider,
				IController controller,
				IAudioManager audioManager,
				UserItems userItems,
				UserSettings userSettings,
				ClientBosses clientBosses,
				BossSelectionModel bossSelectionModel)
		{
			_windowManager = windowManager;
			_prefabProvider = prefabProvider;
			_tooltipManager = tooltipManager;
			_audioManager = audioManager;
			_controller = controller;
			_clientBosses = clientBosses;
			_userSettings = userSettings;
			_bossSelectionModel = bossSelectionModel;

			_battleHistoryActivatableImage.SetActive(false);
			Subscribe(_clientBosses.Bosses, BossesUpdated);
			_bossesRoot.Init<ClientBoss, BossView>(_prefabProvider, _clientBosses.Bosses, true,
					(boss, bossView) => { bossView.Init(boss, OnBossFightClicked); });

			_ticketItem = userItems.GetItem(ItemType.Ticket);
			_ticketItemView.Init(_ticketItem, windowManager);
			tooltipManager.BindTooltip(_ticketItemView.gameObject, TranslationKeys.TICKET_TOOLTIP);

			for (int i = 0; i < _selectCharacterButtons.Length; i++)
			{
				UIButton selectCharacterButton = _selectCharacterButtons[i];
				int index = i;
				selectCharacterButton.SetOnClick(() => OnCharacterSelectButtonClicked(index));
			}

			_battleHistoryButton.SetOnClick(OnHistoryButtonClick);

			Subscribe(userSettings.SkipBattleAnimation, OnSkipBattleAnimationChanged).Update();
		}

		private void BossesUpdated(ListField<ClientBoss> bosses)
		{
			if (bosses.Count > 0)
			{
				_aliveBossId = _clientBosses.Bosses[0].Id;
			}
			_battleHistoryActivatableImage.SetActive(bosses.Count > 0);
		}

		public override void OnStartVisible()
		{
			_controller.Execute(new GetBossesCommand()).Catch(Log.Exception);
		}

		private void OnSkipBattleAnimationChanged(bool skip)
		{
			_skipAnimationToggle.isOn = skip;
		}

		private void OnHistoryButtonClick()
		{
			if (_clientBosses.Bosses.Count > 0)
			{
				_aliveBossId = _clientBosses.Bosses[0].Id;
			}
			else
			{
				return;
			}
			GetBossRatingCommand getBossRatingCommand = new GetBossRatingCommand((uint)_aliveBossId);
			getBossRatingCommand.OnResponse<GetBossRatings.Response>().Then((response) =>
			{
				_topHundred = response.TopHundred;
				_playerRating = response.UserRating;
				_windowManager.Open<BossBattleHistoryWindow>(_topHundred, _playerRating);
			}, Log.Exception);
			IFuture execute = _controller.Execute(getBossRatingCommand);
			_battleHistoryButton.EnableAfter(execute);
			execute.Then(() => { }, Log.Exception);
		}

		private void OnBossFightClicked(ClientBoss boss)
		{
			KeyArgsPair[] warnings = TryCollectWarnings(boss);
			if (warnings.Length != 0)
			{
				_windowManager.Open<WarningWindow>(warnings);
				return;
			}
			Character[] selectedCharacters = GetSelectedCharacters();
			AttackBossCommand attackBossCommand = new AttackBossCommand(selectedCharacters, boss);
			attackBossCommand.OnResponse<AttackBoss.Response>().Then(OnBossAttackCompleted, Log.Exception);

			_controller.Execute(attackBossCommand).Then(() => { }, Rejected);

			CanvasGroup.blocksRaycasts = false;
		}

		private void Rejected(Exception reason)
		{
			_controller.Execute(new GetBossesCommand()).Catch(Log.Exception);
		}

		private KeyArgsPair[] TryCollectWarnings(ClientBoss boss)
		{
			List<KeyArgsPair> collectedWarnings = new List<KeyArgsPair>();
			ListField<CharacterIndexPair> characters = _bossSelectionModel.Characters;
			if (characters == null || characters.Count < boss.Static.MinCharactersInGroupCriterion)
			{
				collectedWarnings.Add(new KeyArgsPair()
				{
						TranslationKey = TranslationKeys.BOSS_WARNING_WRONG_ELEMENT,
				});
				return collectedWarnings.ToArray();
			}
			if (_ticketItem.Count < boss.Static.TicketsCost)
			{
				collectedWarnings.Add(new KeyArgsPair()
				{
						TranslationKey = TranslationKeys.BOSS_WARNING_NOT_ENOUGH_TICKETS
				});
			}
			if (boss.Element != Element.Undefined)
			{
				bool rightElement = false;
				foreach (CharacterIndexPair characterIndexPair in characters)
				{
					if (characterIndexPair.Character.Element == ElementUtils.GetOppositeElement(boss.Element))
					{
						rightElement = true;
						break;
					}
				}
				if (!rightElement)
				{
					collectedWarnings.Add(new KeyArgsPair()
					{
							TranslationKey = TranslationKeys.BOSS_WARNING_WRONG_ELEMENT
					});
				}
			}
			bool haveCharacterWithRarity = false;
			foreach (CharacterIndexPair characterIndexPair in characters)
			{
				if (characterIndexPair.Character.Rarity >= boss.Static.CharactersRarity)
				{
					haveCharacterWithRarity = true;
					break;
				}
			}
			if (!haveCharacterWithRarity)
			{
				collectedWarnings.Add(new KeyArgsPair()
				{
						TranslationKey = TranslationKeys.BOSS_WARNING_WRONG_RARITY,
						Args = new object[] { ((int)(boss.Static.CharactersRarity - 1)).ToString() }
				});
			}
			return collectedWarnings.ToArray();
		}

		private void OnBossAttackCompleted(AttackBoss.Response result)
		{
			TrySaveBattleSettings();
			Promise endBattlePromise = PromisePool.Pull();
			_endBattleFuture = FuturePool.Pull(endBattlePromise);
			_mainUiView.Canvas.enabled = false;
			_mainBackground.SetActive(false);
			Character[] selectedCharacters = GetSelectedCharacters();
			_screenChanger.Open<BossBattleSceneScreen>().
					Init(endBattlePromise, result, selectedCharacters, _skipAnimationToggle.isOn);
			_endBattleFuture.Then(OnBattleFinished, Log.Exception);
			_controller.Execute(new ResetBossFightCharactersCommand());
			_audioManager.PlaySound(SoundType.SoundBossFightStart);
			Disposed = true;
		}

		private void TrySaveBattleSettings()
		{
			if (_userSettings.SkipBattleAnimation != _skipAnimationToggle.isOn)
			{
				_controller.Execute(new SetBattleSettingsCommand(_skipAnimationToggle.isOn)).
						Then(() => { }, Log.Exception);
			}
		}

		private Character[] GetSelectedCharacters()
		{
			List<Character> selectedCharacters = new List<Character>();
			foreach (KeyValuePair<int, Character> keyValuePair in _selectedCharacters)
			{
				if (keyValuePair.Value != null)
				{
					selectedCharacters.Add(keyValuePair.Value);
				}
			}
			return selectedCharacters.ToArray();
		}

		private void OnBattleFinished()
		{
			_mainUiView.Canvas.enabled = true;
			_mainBackground.SetActive(true);
			_screenChanger.Open<BossBattleScreen>().Init(_mainUiView, _screenChanger, _mainBackground);
		}

		private void OnCharacterSelectButtonClicked(int index)
		{
			Promise<Character> characterSelectionPromise = PromisePool.Pull<Character>();
			_characterSelectionFuture = FuturePool.Pull(characterSelectionPromise);
			_windowManager.Open<CharacterSelectWindow>(characterSelectionPromise,
					new Predicate<Character>(IsCharacterCorrect),
					new Action<Character, CharacterListItem>((c, cli) => { cli.ShowAvailable(c.IsAvailable); }));
			_characterSelectionFuture.Then((character) => OnCharacterSelected(character, index), (e) => { });
		}

		private void OnCharacterSelected(Character character, int index)
		{
			if (!_selectedCharacters.ContainsKey(index))
			{
				_selectedCharacters.Add(index, character);
			}
			else
			{
				_selectedCharacters[index] = character;
			}
			CharacterView characterView = _characterViews[index];
			characterView.gameObject.SetActive(true);
			characterView.Init(character, _tooltipManager);
			_controller.Execute(new SetBossFightCharacterCommand(index, character));
		}

		private bool IsCharacterCorrect(Character character)
		{
			return !_selectedCharacters.ContainsValue(character);
		}

		public void Init(MainUiView mainUiView, ScreenChanger screenChanger, GameObject mainBackground)
		{
			_mainUiView = mainUiView;
			_screenChanger = screenChanger;
			_mainBackground = mainBackground;
		}
	}
}