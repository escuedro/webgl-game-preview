﻿using System;
using Framework;
using Framework.Client.Translations;
using Framework.Model;
using Framework.View;
using SAS.Model;
using SAS.Model.Time;
using UnityEngine;

namespace Game.View.Boss
{
	public class BossView : ViewBehaviour
	{
		[SerializeField]
		private TranslatedText _bossNameText;
		[SerializeField]
		private ResourceImage _bossImage;
		[SerializeField]
		private SlicedFilledImage _healthBarFilledImage;
		[SerializeField]
		private UIText _beginText;
		[SerializeField]
		private UIText _endText;
		[SerializeField]
		private UIText _idText;
		[SerializeField]
		private ResourceImage _flag;
		[Header("Tickets")]
		[SerializeField]
		private UIText _ticketCostText;
		[Header("Fight Button")]
		[SerializeField]
		private UIButton _fightButton;
		[SerializeField]
		private ActivatableImage _activatableButtonImage;
		
		private ClientBoss _boss;

		public void Init(ClientBoss clientBoss, Action<ClientBoss> onFightBossClicked)
		{
			ClearSubscriptions();
			_bossImage.Load(clientBoss.Static.CardVisualPath);
			_boss = clientBoss;
			_bossNameText.SetTranslationKey(new TranslationKey(clientBoss.Static.TranslationKey));
			_idText.text = clientBoss.Id.ToString();
			_flag.gameObject.SetActive(clientBoss.Element != 0);
			if (clientBoss.Element > 0)
			{
				_flag.Load(Statics.GetSingle<CharacterSharedStatic>().FlagVisualByElement[(int)clientBoss.Element - 1]);
			}
			Subscribe(clientBoss.HealthPoints, OnHealthPointsChanged).Update();
			_beginText.text = (clientBoss.DeathTime -
					TimeDuration.CreateBySeconds(clientBoss.Static.LifetimeSeconds)).LocalTime.ToString("dd/MM/yy");
			_endText.text = clientBoss.DeathTime.LocalTime.ToString("dd/MM/yy");
			_ticketCostText.text = clientBoss.Static.TicketsCost.ToString();
			_fightButton.SetOnClick(() => onFightBossClicked?.Invoke(clientBoss));
		}

		private void OnHealthPointsChanged(long healthPoints)
		{
			_healthBarFilledImage.fillAmount = (float)healthPoints / _boss.Static.HealthPoints;
		}
	}
}