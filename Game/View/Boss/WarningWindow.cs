﻿using Framework;
using Framework.Futures;
using Framework.UI;
using UnityEngine;

namespace Game.View.Boss
{
	public class WarningWindow : Window<KeyArgsPair[]>
	{
		[SerializeField]
		private Transform _warningRoot;
		[SerializeField]
		private UIButton _okButton;
		
		private IWindowAnimation _animation;
		
		private IPrefabProvider _prefabProvider;

		private void Awake()
		{
			_animation = new ScaleAnimation(this, transform, 0.3f);
		}

		[Inject]
		public void Inject(IPrefabProvider prefabProvider)
		{
			_prefabProvider = prefabProvider;
			_okButton.SetOnClick(Close);
		}
		public override IFuture OnClose()
		{
			return _animation.PlayOut();
		}

		protected override IFuture Open(KeyArgsPair[] warningTranslationKeys)
		{
			foreach (KeyArgsPair keyArgsPair in warningTranslationKeys)
			{
				_prefabProvider.InstantiateAt<BossWarningWindowElement>(_warningRoot, false).
						SetTranslationKey(keyArgsPair.TranslationKey, keyArgsPair.Args);
			}
			return _animation.PlayIn();
		}
	}

	public struct KeyArgsPair
	{
		public TranslationKey TranslationKey;
		public object[] Args;
	}
}