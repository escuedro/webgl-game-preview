﻿using Framework;
using Framework.Client.Translations;
using UnityEngine;

namespace Game.View.Boss
{
	public class BossWarningWindowElement : MonoBehaviour
	{
		[SerializeField]
		private TranslatedText _translatedText;

		public void SetTranslationKey(TranslationKey translationKey, params object[] args)
		{
			_translatedText.SetTranslationKey(translationKey);
			if (args != null && args.Length > 0)
			{
				_translatedText.InputFormattedData(args);
			}
		}
	}
}