﻿using Framework;
using Framework.Collections;
using Framework.Futures;
using Framework.UI;
using GameShared;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View.Boss
{
	public class BossBattleHistoryWindow : Window<ConstArray<BossUserRating>, BossPlayerRating>
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private VerticalLayoutGroup _rootLayout;
		[SerializeField]
		private UIButton _closeButton;
		[SerializeField]
		private BossRatingItemView _playerItemView;

		private FadeInOutAnimation _animation;
		
		private IPrefabProvider _prefabProvider;
		private ITooltipManager _tooltipManager;

		private void Awake()
		{
			_animation = new FadeInOutAnimation(this, _canvasGroup);
			_closeButton.SetOnClick(Close);
		}

		[Inject]
		public void Inject(ITooltipManager tooltipManager, IPrefabProvider prefabProvider)
		{
			_tooltipManager = tooltipManager;
			_prefabProvider = prefabProvider;
		}

		public override IFuture OnClose()
		{
			return _animation.PlayOut();
		}

		protected override IFuture Open(ConstArray<BossUserRating> bossDamages, BossPlayerRating playerRating)
		{
			int index = 1;
			foreach (BossUserRating bossDamage in bossDamages)
			{
				BossRatingItemView ratingView =
						_prefabProvider.InstantiateAt<BossRatingItemView>(_rootLayout.transform, false);
				ratingView.Init(_tooltipManager, index, bossDamage.UserGuid, bossDamage.Damage, bossDamage.ExpectedEssence);
				index++;
			}
			_playerItemView.Init(_tooltipManager, (int)playerRating.Place + 1, playerRating.UserGuid, playerRating.Damage,
					playerRating.ExpectedEssence);
			return _animation.PlayIn();
		}
	}
}