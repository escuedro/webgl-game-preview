﻿using System;
using Framework;
using UnityEngine;

namespace Game.View.Boss
{
	public class BossBattleResultView : MonoBehaviour
	{
		[SerializeField]
		private UIText _damageDoneText;
		[SerializeField]
		private UIButton _completeButton;

		public void Init(long damageDone, Action onCompleteButtonClicked)
		{
			_damageDoneText.text = damageDone.ToString();
			_completeButton.SetOnClick(() => onCompleteButtonClicked?.Invoke());
		}
	}
}