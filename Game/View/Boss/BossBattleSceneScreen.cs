﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Framework;
using Framework.Collections;
using Framework.Futures;
using Framework.Model;
using Game.View.Animations;
using Game.View.Battle;
using GameShared;
using SAS.Model;
using SAS.Model.Animations;
using UnityEngine;

namespace Game.View.Boss
{
	public class BossBattleSceneScreen : Screen
	{
		[SerializeField]
		private CharacterBattleUIView[] _charactersBattleViews;
		[SerializeField]
		private CharacterBattleUIView _bossBattleUIView;

		private IPrefabProvider _prefabProvider;

		private WorldViewRoot _worldViewRoot;

		private Promise _completePromise;

		private BossBattleCharactersView _battleCharactersView;
		private BossBattleResultView _bossBattleResultsView;
		private AttackBoss.Response _attackResponse;
		private BossStatic _bossStatic;

		[Inject]
		public void Inject(IPrefabProvider prefabProvider, WorldViewRoot worldViewRoot)
		{
			_prefabProvider = prefabProvider;
			_worldViewRoot = worldViewRoot;
		}

		public void Init(Promise completePromise, AttackBoss.Response attackResponse, Character[] attackerCharacters, bool skipAnimation)
		{
			_completePromise = completePromise;
			_attackResponse = attackResponse;
			for (int i = 0; i < _charactersBattleViews.Length; i++)
			{
				_charactersBattleViews[i].gameObject.SetActive(i < attackerCharacters.Length);
			}
			for (int i = 0; i < attackerCharacters.Length; i++)
			{
				Character character = attackerCharacters[i];
				_charactersBattleViews[i].Init(new TranslationKey(character.Static.TranslationKey), character.Static.PortraitResourcePath, 1.0f);
			}
			_bossStatic = Statics.Get<BossStatic>(attackResponse.BossStaticId);
			_bossBattleUIView.Init(new TranslationKey(_bossStatic.TranslationKey), _bossStatic.PortraitResourcePath,1.0f);
			_battleCharactersView = _prefabProvider.InstantiateAt<BossBattleCharactersView>(_worldViewRoot.Transform, false);
			_battleCharactersView.Init(_prefabProvider, attackerCharacters.Select(character => character.Static).ToArray(), _bossStatic);
			if (skipAnimation)
			{
				AnimateBattleImmediately();
			}
			else
			{
				StartCoroutine(AnimateBattle());
			}
		}

		private void AnimateBattleImmediately()
		{
			foreach (CharacterBattleUIView charactersBattleView in _charactersBattleViews)
			{
				charactersBattleView.SetHealthNormalized(0.0f);
			}

			ShowBattleResult();
		}


		private IEnumerator AnimateBattle()
		{
			yield return new WaitForSeconds(0.5f);
			AnimationScenario scenario = new BossAnimationScenarioLose(_battleCharactersView.CharacterMecanimAnimations,
					_charactersBattleViews, _battleCharactersView.EnemyMecanimAnimation,
					_bossBattleUIView, _attackResponse.AttackersDamage, new BossHealthInfo(_bossStatic.HealthPoints, _attackResponse.BossStartHp, _attackResponse.BossEndHp));

			yield return scenario.Play();
			yield return new WaitForSeconds(0.25f);
			ShowBattleResult();
		}

		private void ShowBattleResult()
		{
			_bossBattleResultsView = _prefabProvider.Instantiate<BossBattleResultView>(false);
			long totalDamage = 0;
			foreach (long damage in _attackResponse.AttackersDamage)
			{
				totalDamage += damage;
			}
			_bossBattleResultsView.Init(totalDamage, OnComplete);
		}

		void OnComplete()
		{
			Destroy(_bossBattleResultsView.gameObject);
			Destroy(_battleCharactersView.gameObject);
			_completePromise.Resolve();
			Disposed = true;
		}
	}
}