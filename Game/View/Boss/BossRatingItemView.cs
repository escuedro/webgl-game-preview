﻿using Framework;
using Framework.UI;
using Framework.View;
using UnityEngine;

namespace Game.View.Boss
{
	public class BossRatingItemView : MonoBehaviour
	{
		[SerializeField]
		private UIText _ratingNumber;
		[SerializeField]
		private ResourceImage _ratingImage;
		[SerializeField]
		private UIText _idText;
		[SerializeField]
		private UIText _damageText;
		[SerializeField]
		private UIText _rewardText;
		[Header("Colors")]
		[SerializeField]
		private Color _zeroColor;
		[SerializeField]
		private Color _positiveColor;

		private static ResourcePath BronzeResourcePath => Resource.UI.Boss.MedalbronzePng;
		private static ResourcePath SilverResourcePath => Resource.UI.Boss.MedalsilverPng;
		private static ResourcePath GoldenResourcePath => Resource.UI.Boss.MedalgoldPng;

		public void Init(ITooltipManager tooltipManager, int placeId, string userId, long damage, uint expectedEssence)
		{
			ShowPlaceGraphics(placeId);
			_idText.text = userId;
			_damageText.text = $"{damage.ToString()}";
			_rewardText.text = $"+ {expectedEssence.ToString()}";
			_rewardText.color = damage > 0 ? _positiveColor : _zeroColor;
			tooltipManager.BindTooltip(gameObject, userId);
		}

		private void ShowPlaceGraphics(int placeId)
		{
			bool isFirstPlaces = placeId == 1 || placeId == 2 || placeId == 3;
			_ratingImage.gameObject.SetActive(isFirstPlaces);
			_ratingNumber.gameObject.SetActive(!isFirstPlaces);
			if (placeId == 1)
			{
				_ratingImage.Load(GoldenResourcePath);
			}
			else if (placeId == 2)
			{
				_ratingImage.Load(SilverResourcePath);
			}
			else if (placeId == 3)
			{
				_ratingImage.Load(BronzeResourcePath);
			}
			else
			{
				_ratingNumber.text = placeId.ToString();
			}
		}
	}
}