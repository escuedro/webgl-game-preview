using Framework;
using Framework.Audio;
using Framework.Client;
using Framework.Client.View;
using Framework.Service;
using Framework.UI;
using Framework.View;
using Game.Analytics;
using Game.View.Audio;
using Game.View.Battle;
using Game.View.Boss;
using Game.View.Help;
using Game.View.Inventory;
using Game.View.Logout;
using Game.View.StarterPack;
using Game.View.Transmutation;
using Game.Windows;
using SAS.Model;
using SAS.Model.User;
using UnityEngine;

namespace Game.View
{
	public class MainUiView : ViewBehaviour
	{
		[SerializeField]
		private Canvas _mainCanvas;
		[SerializeField]
		private RectTransform _topPanel;
		[SerializeField]
		private PanelButton _starterPack;
		[SerializeField]
		private LinkButton _marketplace;
		[SerializeField]
		private RectTransform _marketplaceRect;
		[SerializeField]
		private PanelButton _inventory;
		[SerializeField]
		private PanelButton _transmutation;
		[SerializeField]
		private PanelButton _attackMonster;
		[SerializeField]
		private PanelButton _attackBoss;
		[SerializeField]
		private UIButton _settings;
		[SerializeField]
		private UIText _userId;
		[SerializeField]
		private UIButton _logout;
		[SerializeField]
		private TokensView _tokensView;
		[SerializeField]
		private ScreenChanger _screenChanger;

		public Canvas Canvas => _mainCanvas;

		public LoadingView LoadingView => _loadingView;
		private LoadingView _loadingView;
		private IWindowManager _windowManager;
		private IHintService _hintService;

		[Inject]
		public ITooltipManager TooltipManager;

		private IPrefabProvider _prefabProvider;
		private IAudioManager _audioManager;
		private UserCharacters _userCharacters;

		private InactivityListener _inactivityListener;
		private ScreenAudioListener _screenAudioListener;
		private UserSettings _userSettings;

		[Inject]
		public void Startup(IPrefabProvider provider,
				IInjector injector,
				IUpdateManager updateManager,
				IWindowManager windowManager,
				IAudioManager audioManager,
				IHintService hintService,
				UserGuid userGuid,
				UserCharacters userCharacters,
				UserSettings userSettings)
		{
			_windowManager = windowManager;
			_prefabProvider = provider;
			_hintService = hintService;
			_audioManager = audioManager;
			_userSettings = userSettings;

			_userCharacters = userCharacters;

			GameObject mainBackground =
					Instantiate(Resources.Load<Transform>(Resource.View.Root.MainBackgroundPrefab.Value)).gameObject;

			injector.Inject(_tokensView);
			injector.Inject(_screenChanger);
#if !PRODUCTION
			provider.InstantiateAt<FpsView>(_topPanel, false);
			provider.InstantiateAt<CheatsWidget>(_topPanel, true);
#endif
			_loadingView = provider.Instantiate<LoadingView>(false);

			PanelGroup panelGroup = new PanelGroup(_starterPack, _inventory, _transmutation,
					_attackMonster, _attackBoss);
			_settings.SetOnClick(() => _windowManager.Open<SettingsWindow>());
			_marketplace.SetOnClick("https://inanomo.com/en", null);

			_starterPack.Init(() => _screenChanger.Open<StarterPackScreen>().Init(_screenChanger), panelGroup);
			_inventory.Init(() => _screenChanger.Open<InventoryScreen>(), panelGroup);
			_transmutation.Init(() => _screenChanger.Open<TransmutationScreen>(), panelGroup);
			_attackMonster.Init(
					() => _screenChanger.Open<BattleVsMonsterScreen>().Init(this, _screenChanger, mainBackground),
					panelGroup);
			_logout.SetOnClick(() => _windowManager.Open<LogoutConfirmWindow>());
			_attackBoss.Init(() => _screenChanger.Open<BossBattleScreen>().Init(this, _screenChanger, mainBackground),
					panelGroup);
			Subscribe(userGuid.Guid, OnGuidLoad);
			UpdateSubscriptions();

			BindHints();
			
			_screenAudioListener = new ScreenAudioListener(_screenChanger, _audioManager);

			Subscribe(_screenChanger.CurrentScreen, (_) => _hintService.HideHints());

			_inactivityListener =
					new InactivityListener(updateManager, _screenChanger, 10.0f, OnScreenInactive, OnNoScreenActive);
		}

		private void BindHints()
		{
			_hintService.BindHint(HintType.StarterPack, _starterPack.RectTransform);
			_hintService.BindHint(HintType.Marketplace, _marketplaceRect);
			_hintService.BindHint(HintType.Inventory, _inventory.RectTransform);
			_hintService.BindHint(HintType.Transmute, _transmutation.RectTransform);
			_hintService.BindHint(HintType.BattleVsMonster, _attackMonster.RectTransform);
			_hintService.BindHint(HintType.BossAttack, _attackBoss.RectTransform);
		}

		private void OnNoScreenActive()
		{
			if (_userCharacters.Characters.Count > 0)
			{
				_hintService.ShowHints(HintType.StarterPack, HintType.BattleVsMonster, HintType.BossAttack);
			}
		}

		private void OnScreenInactive(Screen screen)
		{
			if (screen is StarterPackScreen)
			{
				_hintService.ShowHints(HintType.BattleVsMonster, HintType.BossAttack);
			}
			if (screen is InventoryScreen)
			{
				_hintService.ShowHints(HintType.StarterPack, HintType.BattleVsMonster, HintType.BossAttack);
			}
			if (screen is TransmutationScreen)
			{
				_hintService.ShowHints(HintType.StarterPack, HintType.BattleVsMonster, HintType.BossAttack);
			}
			if (screen is BattleVsMonsterScreen)
			{
				_hintService.ShowHints(HintType.StarterPack, HintType.BossAttack);
			}
			if (screen is BossBattleScreen)
			{
				_hintService.ShowHints(HintType.StarterPack, HintType.BattleVsMonster);
			}
		}

		private void OnGuidLoad(string guid)
		{
			if (!string.IsNullOrEmpty(guid))
			{
				_userId.text = guid;
				TooltipManager.BindTooltip(_userId.gameObject, guid);
			}
		}
	}
}