﻿using System;
using System.Text;
using Framework;
using Framework.Client;
using Framework.Client.Timer;
using Framework.FixedMath;
using Framework.Reactive;
using Framework.UI;
using Framework.Utils;
using Framework.View;
using Game.Service.Visibility;
using Game.Translations;
using SAS.Model;
using SAS.Model.Time;
using UnityEngine;

namespace Game.View.Battle
{
	public class BattleRewardButton : ViewBehaviour
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private ButtonAnimatedText _animatedText;
		[SerializeField]
		private UIButton _button;
		[Header("Get reward state")]
		[SerializeField]
		private GameObject _getRewardState;
		[Header("Wait state")]
		[SerializeField]
		private GameObject _waitState;
		[SerializeField]
		private UIText _waitTimerText;
		[Header("Common")]
		[SerializeField]
		private UIText _rewardCountText;

		private Timer _rewardTimer;

		private UserChest _userChest;
		private IUpdateManager _updateManager;

		public void Init(IUpdateManager updateManager,
				ITooltipManager tooltipManager,
				IVisibilityListener visibilityListener,
				UserChest userChest,
				Action onButtonClicked)
		{
			_userChest = userChest;
			_updateManager = updateManager;
			_button.SetOnClick(onButtonClicked);
			_getRewardState.gameObject.SetActive(false);
			_waitState.gameObject.SetActive(false);
			Subscribe(userChest.TokenRewards, OnTokenRewardsChanged).Update();
			Subscribe(visibilityListener.OnGameVisible, UpdateTimer);
			tooltipManager.BindTooltip(gameObject, TranslationKeys.REWARD_TOOLTIP);
		}

		private void OnTokenRewardsChanged(ListField<TokenReward> tokenRewards)
		{
			_getRewardState.SetActive(false);
			_waitState.SetActive(false);
			if (tokenRewards.Count == 0 && _userChest.EssenceRewards.Count == 0)
			{
				_getRewardState.SetActive(true);
				_rewardCountText.text = GetRewardText(Fix64.Zero);
				ChangeButtonActiveState(false);
			}
			else if (tokenRewards.Count > 0)
			{
				OnRewardTimerReset();
				return;
			}
			if (_userChest.EssenceRewards.Count > 0)
			{
				if (_userChest.TryGetLastEssenceReward(out EssenceReward essenceReward))
				{
					ClearSubscriptions(essenceReward.Count);
					TimeDuration timerLeft =
							essenceReward.ChestRewardTime.ToTimePoint() + ChestRewardTime.RewardDuration -
							TimePoint.Now;
					if (_rewardTimer != null)
					{
						ClearSubscriptions(_rewardTimer.SecondsLeft);
					}
					_rewardTimer = new Timer(_updateManager, timerLeft, false);
					_rewardTimer.SetSecondsLeft(timerLeft);
					_rewardTimer.Start();
					Subscribe(_rewardTimer.SecondsLeft,
							(left) => _waitTimerText.text = StringUtils.DurationFormat(left)).Update();
					_waitTimerText.gameObject.SetActive(true);
					_waitState.gameObject.SetActive(true);
					ChangeButtonActiveState(false);
				}
			}
		}

		private void UpdateTimer(bool isVisible)
		{
			if (isVisible)
			{
				if (_userChest.TryGetLastEssenceReward(out EssenceReward essenceReward) && _rewardTimer != null)
				{
					TimeDuration timerLeft =
							essenceReward.ChestRewardTime.ToTimePoint() + ChestRewardTime.RewardDuration -
							TimePoint.Now;
					_rewardTimer.SetSecondsLeft(timerLeft);
				}
			}
		}

		private void OnRewardTimerReset()
		{
			Fix64 tokensCount = _userChest.GetAllTokens();
			_rewardCountText.text = GetRewardText(tokensCount);
			_waitState.gameObject.SetActive(false);
			_getRewardState.gameObject.SetActive(true);

			ChangeButtonActiveState(true);
		}

		private void ChangeButtonActiveState(bool isActive)
		{
			_canvasGroup.alpha = isActive ? 1.0f : 0.5f;
			_animatedText.enabled = isActive;
			_button.enabled = isActive;
		}

		private string GetRewardText(Fix64 essenceCount)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(essenceCount.ToString());
			sb.Append(" ETER");
			return sb.ToString();
		}
	}
}