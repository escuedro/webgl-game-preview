using System;
using System.Collections;
using Framework;
using Framework.Audio;
using Framework.Futures;
using Game.Audio;
using Game.View.Animations;
using GameShared;
using SAS.Model;
using SAS.Model.Animations;
using SAS.Model.Battle;
using UnityEngine;

namespace Game.View.Battle
{
	public class BattleScreen : Screen
	{
		[Inject]
		public UserCharacters UserCharacters;
		[Inject]
		public IPrefabProvider PrefabProvider;
		[Inject]
		public IAudioManager AudioManager;
		[Inject]
		public WorldViewRoot WorldViewRoot;

		[SerializeField]
		private Color _mint;
		[SerializeField]
		private Color _orange;
		[SerializeField]
		private Color _red;
		[SerializeField]
		private Color _yellow;

		[SerializeField]
		private CharacterBattleUIView _characterBattleUIView;
		[SerializeField]
		private CharacterBattleUIView _enemyBattleUIView;

		private Promise _completePromise;
		private AttackMonster.Response _result;
		private Character _character;
		private BattleResultView _battleResultView;

		private BattleCharactersView _battleCharactersView;

		public override void OnCompleteVisible()
		{
		}

		public void Init(Promise completePromise, AttackMonster.Response result, bool skipAnimation)
		{
			_completePromise = completePromise;
			_result = result;
			BattleResult battleResult = result.BattleResult;
			_character = UserCharacters.TryGet(battleResult.AttackerCharacterId);
			if (_character == null)
			{
				Log.Exception(new Exception($"Not found character with id {battleResult.AttackerCharacterId}"));
				completePromise.Resolve();
				return;
			}
			Monster monster = new Monster(battleResult.AttackedMonster.StaticId, battleResult.AttackedMonster.Level);
			_characterBattleUIView.Init(_character.TranslationKey, _character.Static.PortraitResourcePath, 1.0f);
			_enemyBattleUIView.Init(monster.TranslationKey, monster.Static.PortraitResourcePath, 1.0f);
			_battleCharactersView = PrefabProvider.Instantiate<BattleCharactersView>(WorldViewRoot.Transform);
			_battleCharactersView.Init(PrefabProvider, _character.Static, monster.Static);
			if (skipAnimation)
			{
				AnimateBattleImmediately();
			}
			else
			{
				StartCoroutine(AnimateBattle());
			}
		}

		private void AnimateBattleImmediately()
		{
			bool win = _result.BattleResult.ResultType == BattleResultType.Win;
			float enemyHealthNormalized = win ? 0f : 0.1f;
			float heroHealthNormalized = win ? 0.1f : 0f;

			_enemyBattleUIView.SetHealthNormalized(enemyHealthNormalized);
			_characterBattleUIView.SetHealthNormalized(heroHealthNormalized);

			ShowBattleResult(win);
		}

		private IEnumerator AnimateBattle()
		{
			bool win = _result.BattleResult.ResultType == BattleResultType.Win;
			AnimationScenario scenario = PickRandomScenario(win);

			yield return new WaitForSeconds(1f);
			yield return scenario.Play();
			yield return new WaitForSeconds(0.25f);
			ShowBattleResult(win);
		}

		private AnimationScenario PickRandomScenario(bool win)
		{
			if (win)
			{
				return UnityEngine.Random.Range(0, 2) switch
				{
						0 => new AnimationScenarioAttackWin(_battleCharactersView.CharacterMecanimAnimation,
								_characterBattleUIView, _battleCharactersView.EnemyMecanimAnimation,
								_enemyBattleUIView),
						1 => new AnimationScenarioAttackWin2(_battleCharactersView.CharacterMecanimAnimation,
								_characterBattleUIView, _battleCharactersView.EnemyMecanimAnimation,
								_enemyBattleUIView),
						_ => throw new ArgumentOutOfRangeException()
				};
			}
			return UnityEngine.Random.Range(0, 2) switch
			{
					0 => new AnimationScenarioAttackLose(_battleCharactersView.CharacterMecanimAnimation,
							_characterBattleUIView, _battleCharactersView.EnemyMecanimAnimation, _enemyBattleUIView),
					1 => new AnimationScenarioAttackLose2(_battleCharactersView.CharacterMecanimAnimation,
							_characterBattleUIView, _battleCharactersView.EnemyMecanimAnimation, _enemyBattleUIView),
					_ => throw new ArgumentOutOfRangeException()
			};
		}


		private void ShowBattleResult(bool win)
		{
			if (win)
			{
				_battleResultView = PrefabProvider.Instantiate<BattleVictoryView>(false);
				AudioManager.PlaySound(SoundType.SoundWin);
			}
			else
			{
				_battleResultView = PrefabProvider.Instantiate<BattleDefeatView>(false);
				AudioManager.PlaySound(SoundType.SoundLose);
			}
			_battleResultView.Init(_result, _character, OnComplete);
		}

		void OnComplete()
		{
			Destroy(_battleResultView.gameObject);
			Destroy(_battleCharactersView.gameObject);
			_completePromise.Resolve();
			Disposed = true;
		}
	}
}