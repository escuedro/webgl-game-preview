﻿using Framework;
using Framework.Client.Translations;
using Framework.View;
using UnityEngine;

namespace Game.View.Battle
{
	public class CharacterBattleUIView : MonoBehaviour
	{
		[SerializeField]
		private TranslatedText _nameTranslatedText;
		[SerializeField]
		private SlicedFilledImage _healthBar;
		[SerializeField]
		private ResourceImage _portraitVisual;
				
		public void Init(TranslationKey nameTranslationKey, ResourcePath portraitPath, float healthNormalizedValue)
		{
			_nameTranslatedText.SetTranslationKey(nameTranslationKey);
			_portraitVisual.Load(portraitPath);
			_healthBar.fillAmount = healthNormalizedValue;
		}

		public void SetHealthNormalized(float normalizedHealth)
		{
			_healthBar.fillAmount = normalizedHealth;
		}

		public void ChangeHealthNormalized(float normalizedHealth)
		{
			_healthBar.fillAmount += normalizedHealth;
		}
	}
}