using System;
using Framework;
using Framework.View;
using GameShared;
using SAS.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View.Battle
{
	public class BattleDefeatView : BattleResultView
	{
		[SerializeField]
		private UIButton _closeButton;
		[SerializeField]
		private UIText _expText;
		[SerializeField]
		private Image _expProgress;


		public override void Init(AttackMonster.Response result, Character character, Action onComplete)
		{
			_closeButton.SetOnClick(onComplete);
			_expText.text = $"<font=\"Arial\">+{result.Experience.ToString()}</font> EXP";
			CharacterLevelSharedStatic levelStatic = character.LevelStatic;
			float progress = Mathf.Clamp01((float)character.CurrentExperience / levelStatic.ExperienceToNextLevel);
			_expProgress.fillAmount = progress;
		}
	}
}