﻿using System;
using Framework;
using Framework.Audio;
using Framework.Client;
using Framework.Client.View;
using Framework.Futures;
using Framework.Reactive;
using Framework.RPC;
using Framework.UI;
using Game.Audio;
using Game.Command;
using Game.Service.Visibility;
using Game.Translations;
using Game.View.Transmutation;
using GameShared;
using SAS.Model;
using SAS.Model.User;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View.Battle
{
	public class BattleVsMonsterScreen : Screen
	{
		[SerializeField]
		private UIButton _generateMonstersButton;
		[SerializeField]
		private GridViewBehaviour _gridView;
		[SerializeField]
		private UIButton _characterSelectButton;
		[Header("Character View")]
		[SerializeField]
		private CharacterView _characterView;
		[SerializeField]
		private Transform _characterViewRoot;
		[Header("Battle History button")]
		[SerializeField]
		private UIButton _battleHistoryButton;
		[Header("Rewards button")]
		[SerializeField]
		private BattleRewardButton _battleRewardButton;
		[Header("Essence")]
		[SerializeField]
		private UIText _essenceView;
		[SerializeField]
		private GameObject _essenceViewRect;
		[Header("Skip animation toggle")]
		[SerializeField]
		private Toggle _skipAnimationToggle;

		private IController _controller;
		private IWindowManager _windowManager;
		private IPrefabProvider _prefabProvider;

		private Character _selectedCharacter;
		private IFuture<Character> _characterSelectionFuture;

		private CharacterNextFightSeriesView _nextFightSeriesView;
		private MainUiView _mainUiView;
		private ScreenChanger _screenChanger;
		private IFuture _endBattleFuture;

		private ITooltipManager _tooltipManager;
		private IAudioManager _audioManager;

		private UserSettings _userSettings;
		private GameObject _mainBackground;

		[Inject]
		public void Inject(
				UserMonsters userMonsters,
				UserChest userChest,
				UserSettings userSettings,
				IPrefabProvider prefabProvider,
				IController controller,
				IWindowManager windowManager,
				ITooltipManager tooltipManager,
				IUpdateManager updateManager,
				IAudioManager audioManager,
				IVisibilityListener visibilityListener)
		{
			_prefabProvider = prefabProvider;
			_controller = controller;
			_windowManager = windowManager;
			_tooltipManager = tooltipManager;
			_audioManager = audioManager;
			_userSettings = userSettings;
			_characterSelectButton.SetOnClick(OnCharacterSelectButtonClicked);
			_generateMonstersButton.SetOnClick(OnGenerateMonstersButtonClick);
			_battleHistoryButton.SetOnClick(OnBattleHistoryButtonClick);
			_gridView.Init<Monster, MonsterListItem>(prefabProvider, userMonsters.Monsters, true,
					(monster, monsterListItem) => monsterListItem.Init(monster, OnMonsterClicked));
			UpdateSubscriptions();
			_controller.Execute(new SetCurrentChosenCharacterCommand(null));

			_battleRewardButton.Init(updateManager, tooltipManager, visibilityListener, userChest, OnGetRewardButtonClicked);
			Subscribe(userChest.EssenceRewards, OnEssenceRewardsChanged).Update();
			Subscribe(userSettings.SkipBattleAnimation, OnSkipBattleAnimationChanged).Update();

			_tooltipManager.BindTooltip(_essenceViewRect, TranslationKeys.ESSENCE_TOOLTIP);
		}

		private void OnSkipBattleAnimationChanged(bool skipAnimation)
		{
			_skipAnimationToggle.isOn = skipAnimation;
		}

		private void OnEssenceRewardsChanged(ListField<EssenceReward> rewards)
		{
			uint essenceCount = 0;
			foreach (EssenceReward essenceReward in rewards)
			{
				essenceCount += essenceReward.Count;
			}
			_essenceView.text = essenceCount.ToString();
		}

		private void OnGetRewardButtonClicked()
		{
			_controller.Execute(new GetAllTokensCommand());
		}

		private void OnBattleHistoryButtonClick()
		{
			_windowManager.Open<HistoryWindow>();
		}

		public void Init(MainUiView mainUiView, ScreenChanger screenChanger, GameObject mainBackground)
		{
			_mainUiView = mainUiView;
			_screenChanger = screenChanger;
			_mainBackground = mainBackground;
		}

		private void OnCharacterSelectButtonClicked()
		{
			Promise<Character> characterSelectionPromise = PromisePool.Pull<Character>();
			_characterSelectionFuture = FuturePool.Pull(characterSelectionPromise);
			_windowManager.Open<CharacterSelectWindow>(characterSelectionPromise,
					new Predicate<Character>(IsCharacterCorrect), new Action<Character, CharacterListItem>((c, cli) =>
					{
						cli.ShowAvailable(c.IsAvailable);
					}));
			_characterSelectionFuture.Then(OnCharacterSelected, (e) => {});
		}

		private bool IsCharacterCorrect(Character character)
		{
			return true;
		}

		private void OnCharacterSelected(Character character)
		{
			if (_selectedCharacter != null)
			{
				ClearSubscriptions(_selectedCharacter.AvailableFightsCount);
			}
			_selectedCharacter = character;
			Subscribe(_selectedCharacter.AvailableFightsCount, OnAvailableFightsChanged);
			_characterView.gameObject.SetActive(character != null);
			_characterView.Init(character, _tooltipManager);
			_controller.Execute(new SetCurrentChosenCharacterCommand(character));
		}

		private void OnAvailableFightsChanged(int availableFights)
		{
			if (availableFights == 0)
			{
				if (_nextFightSeriesView == null)
				{
					_nextFightSeriesView = _prefabProvider.InstantiateAt<CharacterNextFightSeriesView>(_characterViewRoot, true);
				}
				_nextFightSeriesView.Init(_selectedCharacter);
			}
			else
			{
				if (_nextFightSeriesView != null)
				{
					_nextFightSeriesView.gameObject.SetActive(false);
				}
			}
		}

		private void OnGenerateMonstersButtonClick()
		{
			_audioManager.PlaySound(SoundType.SoundRefresh);
			_controller.Execute(new GenerateMonstersCommand(4));
		}

		private void OnMonsterClicked(Monster monster)
		{
			_controller.Execute(new SetCurrentChosenMonsterCommand(monster));
			AttackMonsterCommand attackMonsterCommand = new AttackMonsterCommand(_selectedCharacter, monster);
			attackMonsterCommand.OnResponse<AttackMonster.Response>().Then(OnAttackMonsterCompleted, Log.Exception);
			_controller.Execute(attackMonsterCommand);
			_audioManager.PlaySound(SoundType.SoundFightStart);

			CanvasGroup.blocksRaycasts = false;
		}

		private void OnAttackMonsterCompleted(AttackMonster.Response result)
		{
			TrySaveBattleSettings();
			Promise endBattlePromise = PromisePool.Pull();
			_endBattleFuture = FuturePool.Pull(endBattlePromise);
			_mainUiView.Canvas.enabled = false;
			_mainBackground.SetActive(false);
			_screenChanger.Open<BattleScreen>().Init(endBattlePromise, result, _skipAnimationToggle.isOn);
			_endBattleFuture.Then(OnBattleFinished, Log.Exception);
			Disposed = true;
		}

		private void TrySaveBattleSettings()
		{
			if (_userSettings.SkipBattleAnimation != _skipAnimationToggle.isOn)
			{
				_controller.Execute(new SetBattleSettingsCommand(_skipAnimationToggle.isOn)).
						Then(SuccessBattleSettingsChanged, Log.Exception);
			}
		}

		private void SuccessBattleSettingsChanged()
		{
			Log.Info("Success changed battle settings!");
		}

		private void OnBattleFinished()
		{
			_mainUiView.Canvas.enabled = true;
			_mainBackground.SetActive(true);
			_screenChanger.Open<BattleVsMonsterScreen>().Init(_mainUiView, _screenChanger, _mainBackground);
		}
	}
}