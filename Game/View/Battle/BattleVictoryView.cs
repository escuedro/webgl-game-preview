using System;
using Framework;
using Framework.View;
using GameShared;
using SAS.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View.Battle
{
	public class BattleVictoryView : BattleResultView
	{
		[SerializeField]
		private UIButton _closeButton;
		[SerializeField]
		private UIText _expText;
		[SerializeField]
		private UIText _essenceText;
		[SerializeField]
		private Image _expProgress;
		[SerializeField]
		private GameObject _foliant;
		[SerializeField]
		private GameObject _ticket;
		[SerializeField]
		private GameObject _elixir;


		public override void Init(AttackMonster.Response result, Character character, Action onComplete)
		{
			_closeButton.SetOnClick(onComplete);
			_expText.text = $"<font=\"Arial\">+{result.Experience.ToString()}</font> EXP";
			CharacterLevelSharedStatic levelStatic = character.LevelStatic;
			float progress = Mathf.Clamp01((float)character.CurrentExperience / levelStatic.ExperienceToNextLevel);
			_expProgress.fillAmount = progress;
			_essenceText.text = $"+{result.Essence.ToString()}";
			foreach (ItemPair itemPair in result.ItemReward)
			{
				switch (itemPair.Id)
				{
					case (int)ItemType.Folliant:
						_foliant.SetActive(true);
						break;

					case (int)ItemType.Ticket:
						_ticket.SetActive(true);
						break;
					case (int)ItemType.Elixir:
						_elixir.SetActive(true);
						break;
					default:
						Log.Error($"Unknown reward type {itemPair.ItemStatic.Name}");
						break;
				}
			}
		}
	}
}