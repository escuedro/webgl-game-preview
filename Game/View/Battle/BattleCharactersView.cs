﻿using Framework;
using Framework.View;
using SAS.Model;
using UnityEngine;

namespace Game.View.Battle
{
	public class BattleCharactersView  : ViewBehaviour
	{
		[SerializeField]
		private GameObject _characterPoint;
		[SerializeField]
		private GameObject _enemyPoint;

		private ResourceMecanimAnimation _characterMecanimAnimation;
		public ResourceMecanimAnimation CharacterMecanimAnimation => _characterMecanimAnimation;

		private ResourceMecanimAnimation _enemyMecanimAnimation;
		public ResourceMecanimAnimation EnemyMecanimAnimation => _enemyMecanimAnimation;

		public void Init(IPrefabProvider prefabProvider, CharacterStatic characterStatic, MonsterStatic monsterStatic)
		{
			_characterMecanimAnimation = prefabProvider.InstantiateAt(new ResourcePath(characterStatic.PrefabResourcePath), _characterPoint.transform).GetComponent<ResourceMecanimAnimation>();
			_enemyMecanimAnimation = prefabProvider.InstantiateAt(new ResourcePath(monsterStatic.PrefabResourcePath), _enemyPoint.transform).GetComponent<ResourceMecanimAnimation>();
		}
	}
}