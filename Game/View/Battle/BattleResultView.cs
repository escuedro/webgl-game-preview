using System;
using Framework.View;
using GameShared;
using SAS.Model;

namespace Game.View.Battle
{
	public abstract class BattleResultView : ViewBehaviour
	{
		public abstract void Init(AttackMonster.Response result, Character character, Action onComplete);
	}
}