using UnityEngine;

namespace Game.View.Battle
{
	public class BattleBackgroundScaler : MonoBehaviour
	{
		[SerializeField]
		private RectTransform _canvasRect;
		[SerializeField]
		private RectTransform _rectTransform;
		[SerializeField]
		private float _targetHeigh;
		private float _lastScale = 1;

		public void LateUpdate()
		{
			float scale = Mathf.Max(1, _canvasRect.sizeDelta.y / _targetHeigh);
			if (!Mathf.Approximately(_lastScale, scale))
			{
				_lastScale = scale;
				_rectTransform.localScale = Vector3.one * scale;
			}
		}
	}
}