using System;
using System.Text;
using Framework;
using Framework.Client.Translations;
using SAS.Model;
using SAS.Model.Battle;
using SAS.Model.History;
using SAS.Model.Time;
using UnityEngine;

namespace Game.View
{
	public class HistoryItemView : MonoBehaviour
	{
		[SerializeField]
		private UIText _nftText;
		[SerializeField]
		private TranslatedText _characterText;
		[SerializeField]
		private UIText _levelText;
		[SerializeField]
		private UIText _resultText;
		[SerializeField]
		private UIText _experienceText;
		[SerializeField]
		private UIText _rewardText;
		[SerializeField]
		private UIText _timeText;
		
		[SerializeField]
		private Color32 _winColor;
		[SerializeField]
		private Color32 _loseColor;
		[SerializeField]
		private Color32 _idColor;
		
		private UserCharacters _userCharacters;

		[Inject]
		public void Inject(UserCharacters userCharacters)
		{
			_userCharacters = userCharacters;
		}
		
		public void Init(HistoryData historyData)
		{
			Character character = _userCharacters.TryGet(historyData.CharacterId);
			TimePoint fightTime = TimePoint.CreateBySeconds(historyData.FightTime);
			string day = fightTime.LocalTime.ToString("dd.MM.yyyy");
			string time = fightTime.LocalTime.ToString("HH:mm:ss");
			_nftText.text = GetColored($"#{character.Guid}", _idColor);
			_characterText.SetTranslationKey(character.TranslationKey);
			_levelText.text = historyData.Level.ToString();
			_resultText.text = historyData.Result == BattleResultType.Win ?
					GetColored("Win", _winColor) :
					GetColored("Lose", _loseColor);
			_experienceText.text = historyData.Experience.ToString();
			bool positiveReward = historyData.Essence > 0;
			StringBuilder rewardText = new StringBuilder();
			if (positiveReward)
			{
				rewardText.Append("+");
			}
			rewardText.Append(historyData.Essence.ToString());
			_rewardText.text = GetColored(rewardText.ToString(), positiveReward ? _winColor : _idColor);
			_timeText.text = $"{day} {time}";
		}

		private string GetColored(string text, Color32 color)
		{
			byte[] oneByte = new byte[1];
			oneByte[0] = color.r;
			string r = BitConverter.ToString(oneByte);
			oneByte[0] = color.g;
			string g = BitConverter.ToString(oneByte);
			oneByte[0] = color.b;
			string b = BitConverter.ToString(oneByte);
			return $"<color=#{r}{g}{b}>{text}</color>";
		}
	}
}