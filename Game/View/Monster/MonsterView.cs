﻿using Framework;
using Framework.Client.Translations;
using Framework.View;
using SAS.Model;
using UnityEngine;

namespace Game.View
{
	public class MonsterView : ViewBehaviour
	{
		[SerializeField]
		private TranslatedText _monsterName;
		[SerializeField]
		private UIText _levelText;
		[SerializeField]
		private UIText _winChanceText;
		[SerializeField]
		private UIText _rewardText;
		[SerializeField]
		private UIText _experienceText;
		[SerializeField]
		private UIText _ticketChanceText;
		[SerializeField]
		private ResourceImage _monsterIcon;

		public void Init(Monster monster)
		{
			_monsterName.SetTranslationKey(new TranslationKey(monster.Static.TranslationKey));
			_levelText.text = monster.Level.ToString();
			_winChanceText.text = (monster.WinChance / 1000).ToString();
			_rewardText.text = monster.EssenceForWin.ToString();
			_experienceText.text = monster.ExperienceForWin.ToString();
			_ticketChanceText.text = $"{monster.TicketRewardChance / 1000}%";
			_monsterIcon.Load(monster.Static.CardVisualPath);
		}
	}
}