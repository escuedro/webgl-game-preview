using System;
using Framework;
using Framework.View;
using Game.Model;
using SAS.Model;
using UnityEngine;

namespace Game.View
{
	public class MonsterListItem : ViewBehaviour
	{
		[SerializeField]
		private UIButton _button;
		[SerializeField]
		private ButtonAnimatedText _animatedText;
		[SerializeField]
		private CanvasGroup _buttonCanvasGroup;
		[SerializeField]
		private MonsterView _monsterView;

		private Monster _monster;
		
		private SelectionModel _selectionModel;

		[Inject]
		public void Inject(SelectionModel selectionModel)
		{
			_selectionModel = selectionModel;
			Subscribe(_selectionModel.CurrentChosenCharacter, OnCharacterSelected).Update();
		}

		private void OnCharacterSelected(Character character)
		{
			UpdateButtonGraphics(character != null);
		}

		private void UpdateButtonGraphics(bool isActive)
		{
			_buttonCanvasGroup.alpha = isActive ? 1.0f : 0.7f;
			_animatedText.enabled = isActive;
			_button.enabled = isActive;
		}

		public void Init(Monster monster, Action<Monster> onClick)
		{
			_monster = monster;
			_monsterView.Init(monster);
			_button.SetOnClick(() => onClick?.Invoke(_monster));
		}
	}
}