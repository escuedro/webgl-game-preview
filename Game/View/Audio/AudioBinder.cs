﻿using System;
using Framework;
using Framework.Audio;
using Game.Audio;

namespace Game.View.Audio
{
	public class AudioBinder
	{
		private readonly IResourceBinder _resourceBinder;
		private readonly IAudioManager _audioManager;

		public AudioBinder(IResourceBinder resourceBinder, IAudioManager audioManager)
		{
			_resourceBinder = resourceBinder;
			_audioManager = audioManager;
		}

		public void BindAudio()
		{
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundClickButtonAsset, SoundType.SoundClickButton);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.EGMainThemeAsset, SoundType.EGMainTheme);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.EGAmbientAsset, SoundType.EGAmbient);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.EGBattleThemeAsset, SoundType.EGBattleTheme);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundLoginAsset, SoundType.SoundLogin);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundLoginFailureAsset, SoundType.SoundLoginFailure);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundMergeAsset, SoundType.SoundMerge);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundMergeFailureAsset, SoundType.SoundMergeFailure);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundStarterPackAsset, SoundType.SoundStarterPack);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundPurchaseAsset, SoundType.SoundPurchase);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundChestOpenAsset, SoundType.SoundChestOpen);
			
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundRarity1Asset, SoundType.SoundRarity1);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundRarity2Asset, SoundType.SoundRarity2);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundRarity3Asset, SoundType.SoundRarity3);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundRarity4Asset, SoundType.SoundRarity4);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundRarity5Asset, SoundType.SoundRarity5);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundRarity6Asset, SoundType.SoundRarity6);
			
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundElixirUsedAsset, SoundType.SoundElixirUsed);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundWarningAsset, SoundType.SoundElixirUsed);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundCharacterSelectionAsset, SoundType.SoundCharacterSelection);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundRewardAsset, SoundType.SoundReward);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundFightStartAsset, SoundType.SoundFightStart);
			
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundWinAsset, SoundType.SoundWin);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundLoseAsset, SoundType.SoundLose);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundLvlUpAsset, SoundType.SoundLvlUp);
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundRefreshAsset, SoundType.SoundRefresh);
			
			_resourceBinder.Bind<SoundRegistration>(Resource.Sounds.SoundBossFightStartAsset, SoundType.SoundBossFightStart);
			
			UIButton.OnAnyClick = OnButtonClick;
		}

		private void OnButtonClick(string soundName)
		{
			if (Enum.TryParse(soundName, out SoundType soundType))
			{
				_audioManager.PlaySound(soundType);
			}
			else
			{
				throw new Exception($"No sound with name {soundName}");
			}
		}
	}
}