﻿using Framework.Audio;
using Framework.Reactive;
using Game.Audio;
using Game.View.Battle;
using Game.View.Boss;
using Game.View.Inventory;
using Game.View.Transmutation;

namespace Game.View.Audio
{
	public class ScreenAudioListener : ModelListener
	{
		private ScreenChanger _screenChanger;
		private IAudioManager _audioManager;

		private SoundType _currentBackgroundSoundType;

		public ScreenAudioListener(ScreenChanger screenChanger, IAudioManager audioManager)
		{
			_audioManager = audioManager;
			Subscribe(screenChanger.CurrentScreen, OnScreenChanged).Update();
			_screenChanger = screenChanger;
		}

		private void OnScreenChanged(Screen screen)
		{
			SoundType neededSoundType = SoundType.EGMainTheme;
			bool isBattleMusic = false;
			if (screen is BossBattleSceneScreen || screen is BattleScreen)
			{
				neededSoundType = SoundType.EGBattleTheme;
				isBattleMusic = true;
			}
			else if (screen is InventoryScreen || screen is TransmutationScreen)
			{
				neededSoundType = SoundType.EGAmbient;
			}
			else
			{
				neededSoundType = SoundType.EGMainTheme;
			}
			if (_currentBackgroundSoundType != neededSoundType)
			{
				_audioManager.ChangeBackgroundSound(neededSoundType, isBattleMusic);
				_currentBackgroundSoundType = neededSoundType;
			}
		}
	}
}