using Framework;
using Framework.FixedMath;
using Framework.UI;
using Framework.View;
using Game.Translations;
using SAS.Model;
using TMPro;
using UnityEngine;

namespace Game.View
{
	public class TokensView : ViewBehaviour
	{
		[SerializeField]
		private TMP_Text _tokens;

		[Inject]
		public void Inject(ITooltipManager tooltipManager, UserChest userChest)
		{
			tooltipManager.BindTooltip(gameObject, TranslationKeys.TOKENS);
			Subscribe(userChest.TokensAvailable, UpdateCount);
			UpdateSubscriptions();
		}

		private void UpdateCount(Fix64 tokens)
		{
			_tokens.text = tokens.ToString();
		}
	}
}