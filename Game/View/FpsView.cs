using System.Collections.Generic;
using Framework;
using Framework.View;
using UnityEngine;

namespace Game.View
{
	public class FpsView : ViewBehaviour
	{
		[SerializeField]
		private UIText _fpsText;
		private int _framesFilter = 5;
		private float _fpsTime;
		private readonly Queue<float> timeDeltasQueue = new Queue<float>();

		private void Awake()
		{
			for (int i = 0; i < _framesFilter; i++)
			{
				timeDeltasQueue.Enqueue(0f);
			}
		}

		private void Update()
		{
			_fpsTime -= timeDeltasQueue.Dequeue();
			_fpsTime += Time.deltaTime;
			timeDeltasQueue.Enqueue(Time.deltaTime);
			
			_fpsText.text = $"{((int)(1f/(_fpsTime/_framesFilter))).ToString()}";
		}
	}
}