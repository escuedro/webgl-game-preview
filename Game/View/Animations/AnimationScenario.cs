﻿using System.Collections;

namespace SAS.Model.Animations
{
	public abstract class AnimationScenario
	{
		public abstract IEnumerator Play();
	}
}