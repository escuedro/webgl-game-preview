﻿using Game.View;
using Game.View.Battle;

namespace SAS.Model.Animations
{
	public abstract class DuelAnimationScenario : AnimationScenario
	{
		protected ResourceMecanimAnimation _character;
		protected CharacterBattleUIView _characterBattleUIView;
		protected ResourceMecanimAnimation _monster;
		protected CharacterBattleUIView _enemyBattleUIView;

		public DuelAnimationScenario(ResourceMecanimAnimation character,
				CharacterBattleUIView characterBattleUIView,
				ResourceMecanimAnimation monster,
				CharacterBattleUIView enemyBattleUIView)
		{
			_character = character;
			_characterBattleUIView = characterBattleUIView;
			_monster = monster;
			_enemyBattleUIView = enemyBattleUIView;
		}
	}
}