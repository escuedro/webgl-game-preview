﻿using System.Collections;
using Game.View.Battle;
using SAS.Model.Animations;

namespace Game.View.Animations
{
	public class AnimationScenarioAttackWin2 : DuelAnimationScenario
	{
		public AnimationScenarioAttackWin2(ResourceMecanimAnimation character, CharacterBattleUIView characterBattleUIView, ResourceMecanimAnimation monster, CharacterBattleUIView enemyBattleUIView) : base(character, characterBattleUIView, monster, enemyBattleUIView)
		{
		}

		public override IEnumerator Play()
		{
			AnimationAttackAction attack1 = new AnimationAttackAction(_character, _monster, _enemyBattleUIView, 0.15f);
			AnimationAttackAction attack2 = new AnimationAttackAction(_monster, _character, _characterBattleUIView, 0.15f);
			yield return attack2.Action();
			yield return attack1.Action();
			yield return attack2.Action();
			yield return attack1.Action();
			yield return attack2.Action();
			yield return attack1.WithDamage(0.35f).Action();
			yield return new AnimationStunAction(_monster).Action();
			yield return attack1.WithDamage(0.25f).Action();
			yield return new AnimationStunAction(_monster).Action();
			yield return attack1.Action();
			yield return new AnimationDeathAction(_monster).Action();
		}
	}
}