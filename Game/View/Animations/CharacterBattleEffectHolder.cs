﻿using Framework;
using UnityEngine;

namespace Game.View.Animations
{
	public enum AttackType
	{
		ToBody,
		ToLegs
	}
	public class CharacterBattleEffectHolder : MonoBehaviour
	{
		[SerializeField]
		private AttackType _attackType = AttackType.ToBody;
		public AttackType AttackType=> _attackType;

		[CanBeNull]
		[SerializeField]
		private VFX.VFX _attackVfx;
		public VFX.VFX AttackVfx => _attackVfx;
		public float AttackVfxDelay;

		[SerializeField]
		private Transform _attackVfxPoint;
		public Transform AttackVfxPoint => _attackVfxPoint;

		[CanBeNull]
		[SerializeField]
		private VFX.VFX _attackedVfx;
		public VFX.VFX AttackedVfx => _attackedVfx;
		public float AttackedVfxDelay;

		[SerializeField]
		private Transform _attackedVfxPoint;
		public Transform AttackedVfxPoint => _attackedVfxPoint;

		[CanBeNull]
		[SerializeField]
		private VFX.VFX _stunVfx;
		public VFX.VFX StunVfx => _stunVfx;

		[SerializeField]
		private Transform _stunVfxPoint;
		public Transform StunVfxPoint => _stunVfxPoint;
	}
}