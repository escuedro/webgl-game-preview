﻿using System;
using System.Collections;
using Game.View.Battle;
using UnityEngine;

namespace Game.View.Animations
{
	public class AnimationAttackAction : AnimationAction
	{
		private ResourceMecanimAnimation _attacker;
		private ResourceMecanimAnimation _attacked;
		private CharacterBattleUIView _attackedBattleUIView;

		private float _damage;

		public AnimationAttackAction(ResourceMecanimAnimation attacker,
				ResourceMecanimAnimation attacked,
				CharacterBattleUIView attackedBattleUIView,
				float damage)
		{
			_attacker = attacker;
			_attacked = attacked;
			_attackedBattleUIView = attackedBattleUIView;
			_damage = damage;
		}

		public AnimationAttackAction WithDamage(float damage)
		{
			_damage = damage;
			return this;
		}

		public override IEnumerator Action()
		{
			Transform attackTo;
			switch (_attacker.BattleEffectHolder.AttackType)
			{
				case AttackType.ToBody:
					attackTo = _attacked.BattleEffectHolder.AttackedVfxPoint;
					break;
				case AttackType.ToLegs:
					attackTo = _attacked.transform;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			yield return _attacker.PlayAttack(attackTo, () =>
			{
				_attackedBattleUIView.ChangeHealthNormalized(-_damage);
				//TODO: when take damage animate
			});
		}
	}
}