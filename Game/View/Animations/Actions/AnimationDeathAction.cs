﻿using System.Collections;

namespace Game.View.Animations
{
	public class AnimationDeathAction : AnimationAction
	{
		private ResourceMecanimAnimation _character;

		public AnimationDeathAction(ResourceMecanimAnimation character)
		{
			_character = character;
		}

		public override IEnumerator Action()
		{
			return _character.Play("death");
		}
	}
}