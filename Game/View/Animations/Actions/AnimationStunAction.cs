﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.View.Animations
{
	public class AnimationStunAction : AnimationAction
	{
		private ResourceMecanimAnimation _character;

		public AnimationStunAction(ResourceMecanimAnimation character)
		{
			_character = character;
		}

		public override IEnumerator Action()
		{
			if (_character.BattleEffectHolder.StunVfx != null && _character.BattleEffectHolder.StunVfxPoint != null) //TODO: qualityGate instead
			{
				var stunVfx = Object.Instantiate(_character.BattleEffectHolder.StunVfx,
						_character.BattleEffectHolder.StunVfxPoint.position, Quaternion.identity, _character.transform);
				stunVfx.Play();
				yield return new WaitForSeconds(stunVfx.GetLength());
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}