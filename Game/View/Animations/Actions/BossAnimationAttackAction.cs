﻿using System;
using System.Collections;
using Game.View.Battle;
using UnityEngine;

namespace Game.View.Animations
{
	public class BossAnimationAttackAction : AnimationAction
	{
		private ResourceMecanimAnimation _attacker;
		private ResourceMecanimAnimation _attacked;
		private CharacterBattleUIView[] _attackedBattleUIViews;

		private float _damage;

		public BossAnimationAttackAction(ResourceMecanimAnimation attacker,
				ResourceMecanimAnimation attacked,
				CharacterBattleUIView[] attackedBattleUIViews,
				float damage)
		{
			_attacker = attacker;
			_attacked = attacked;
			_attackedBattleUIViews = attackedBattleUIViews;
			_damage = damage;
		}

		public BossAnimationAttackAction WithDamage(float damage)
		{
			_damage = damage;
			return this;
		}

		public override IEnumerator Action()
		{
			Transform attackTo;
			switch (_attacker.BattleEffectHolder.AttackType)
			{
				case AttackType.ToBody:
					attackTo = _attacked.BattleEffectHolder.AttackedVfxPoint;
					break;
				case AttackType.ToLegs:
					attackTo = _attacked.transform;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			yield return _attacker.PlayAttack(attackTo,() =>
			{
				foreach (var view in _attackedBattleUIViews)
				{
					view.ChangeHealthNormalized(-_damage);
				}
				Debug.Log("damage");
				//_attacked.Play("attack"); //TODO: when take damage animate
			});
		}
	}
}