﻿using System.Collections;

namespace Game.View.Animations
{
	public abstract class AnimationAction
	{
		public abstract IEnumerator Action();
	}
}