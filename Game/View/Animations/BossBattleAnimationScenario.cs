﻿using Framework.Collections;
using Game.View;
using Game.View.Battle;

namespace SAS.Model.Animations
{
	public struct BossHealthInfo
	{
		public BossHealthInfo(long bossHp, long bossStartHp, long bossEndHp)
		{
			BossHp = bossHp;
			BossStartHp = bossStartHp;
			BossEndHp = bossEndHp;
		}
		public readonly long BossHp;
		public readonly long BossStartHp;
		public readonly long BossEndHp;
	}

	public abstract class BossBattleAnimationScenario : AnimationScenario
	{
		protected ResourceMecanimAnimation[] _characters;
		protected CharacterBattleUIView[] _characterBattleUIViews;
		protected ResourceMecanimAnimation _boss;
		protected CharacterBattleUIView _bossBattleUIView;
		protected ConstArray<long> _attackersDamage;
		protected BossHealthInfo _bossHealthInfo;

		public BossBattleAnimationScenario(ResourceMecanimAnimation[] characters,
				CharacterBattleUIView[] characterBattleUIViews,
				ResourceMecanimAnimation boss,
				CharacterBattleUIView bossBattleUIView, ConstArray<long> attackersDamage, BossHealthInfo bossHealthInfo)
		{
			_characters = characters;
			_characterBattleUIViews = characterBattleUIViews;
			_boss = boss;
			_bossBattleUIView = bossBattleUIView;
			_attackersDamage = attackersDamage;
			_bossHealthInfo = bossHealthInfo;
		}
	}
}