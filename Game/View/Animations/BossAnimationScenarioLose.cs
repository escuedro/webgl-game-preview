﻿using System.Collections;
using Framework.Collections;
using Game.View.Battle;
using SAS.Model.Animations;
using UnityEngine;

namespace Game.View.Animations
{
	public class BossAnimationScenarioLose : BossBattleAnimationScenario
	{
		public BossAnimationScenarioLose(ResourceMecanimAnimation[] characters, CharacterBattleUIView[] characterBattleUIViews, ResourceMecanimAnimation boss, CharacterBattleUIView bossBattleUIView, ConstArray<long> attackersDamage, BossHealthInfo bossHealthInfo) : base(characters, characterBattleUIViews, boss, bossBattleUIView, attackersDamage, bossHealthInfo)
		{
		}

		public override IEnumerator Play()
		{
			const int stepsToDie = 3;
			float startHpNormalized = (float)_bossHealthInfo.BossStartHp / _bossHealthInfo.BossHp;
			float endHpNormalized = (float)_bossHealthInfo.BossEndHp / _bossHealthInfo.BossHp;
			_bossBattleUIView.SetHealthNormalized(startHpNormalized);
			BossAnimationAttackAction attackB = new BossAnimationAttackAction(_boss, _characters[0], _characterBattleUIViews, 1/(float)stepsToDie);
			AnimationAttackAction[] characterAttacks = new AnimationAttackAction[_characters.Length];

			for(int i = 0; i < _characters.Length; i++)
			{
				characterAttacks[i] = new AnimationAttackAction(_characters[i], _boss, _bossBattleUIView, (float)_attackersDamage[i]/stepsToDie/_bossHealthInfo.BossHp);
			}

			for (int i = 0; i < stepsToDie;i++)
			{
				foreach (AnimationAttackAction attackAction in characterAttacks)
				{
					yield return attackAction.Action();
				}
				yield return attackB.Action();
			}
			_bossBattleUIView.SetHealthNormalized(endHpNormalized); //TODO: simply remove
		}
	}
}