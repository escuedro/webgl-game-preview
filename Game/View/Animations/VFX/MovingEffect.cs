﻿using System;
using System.Collections;
using UnityEngine;

namespace Game.View.Animations.VFX
{
	public class MovingEffect : VFX
	{
		public override VFXType VFXType => VFXType.Moving;

		private Vector3 _startPoint;
		private Vector3 _endPoint;

		[SerializeField]
		private float _moveTime = 1f;
		[SerializeField]
		private float _timeToStayOnEnd = 1f;

		private Coroutine _moveRoutine;

		public override float GetLength() => _moveTime + _timeToStayOnEnd;

		public void Init(Vector3 startPoint, Vector3 endPoint, Action onComplete)
		{
			_startPoint = startPoint;
			_endPoint = endPoint;
			OnVFXHitEvent = onComplete;
		}

		public override void Play()
		{
			_moveRoutine = StartCoroutine(MoveRoutine());
		}

		private void OnDestroy()
		{
			if(_moveRoutine!=null)
			{
				StopCoroutine(_moveRoutine);
			}
		}

		private IEnumerator MoveRoutine()
		{
			float startTime = Time.time;
			while (startTime + _moveTime > Time.time)
			{
				transform.position = Vector3.Lerp(_startPoint, _endPoint, (Time.time - startTime) / _moveTime);
				yield return null;
			}
			transform.position = _endPoint;
			Debug.Log("OnVFXHitEventInvoke");
			OnVFXHitEvent?.Invoke();
			Destroy(gameObject);
		}
	}
}