﻿using System;
using UnityEngine;

namespace Game.View.Animations.VFX
{
	public enum VFXType
	{
		Static,
		Moving
	}
	public abstract class VFX : MonoBehaviour
	{
		public Action OnVFXHitEvent { get; set; }
		public abstract VFXType VFXType { get; }
		public abstract float GetLength();
		public abstract void Play();
	}
}