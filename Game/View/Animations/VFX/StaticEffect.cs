using Game.View.Animations.VFX;
using UnityEngine;

public class StaticEffect : VFX
{
	public override VFXType VFXType => VFXType.Static;

	[SerializeField]
	private float _length;
	public override float GetLength() => _length;
	//[SerializeField]
	//private float _invokeDelay;
	[SerializeField]
	private float _timeToDisappear = 1;

	public override void Play()
	{
		OnVFXHitEvent?.Invoke();
		Destroy(gameObject, _timeToDisappear);
	}
}
