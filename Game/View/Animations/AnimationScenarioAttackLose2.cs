﻿using System.Collections;
using Game.View.Battle;
using SAS.Model.Animations;

namespace Game.View.Animations
{
	public class AnimationScenarioAttackLose2 : DuelAnimationScenario
	{
		public AnimationScenarioAttackLose2(ResourceMecanimAnimation character, CharacterBattleUIView characterBattleUIView, ResourceMecanimAnimation monster, CharacterBattleUIView enemyBattleUIView) : base(character, characterBattleUIView, monster, enemyBattleUIView)
		{
		}

		public override IEnumerator Play()
		{
			AnimationAttackAction attack1 = new AnimationAttackAction(_character,_monster, _enemyBattleUIView, 0.1f);
			AnimationAttackAction attack2 = new AnimationAttackAction(_monster, _character, _characterBattleUIView, 0.15f);
			yield return attack1.Action();
			yield return attack2.Action();
			yield return attack1.Action();
			yield return attack2.WithDamage(0.2f).Action();
			yield return new AnimationStunAction(_character).Action();
			yield return attack2.WithDamage(0.2f).Action();
			yield return new AnimationStunAction(_character).Action();
			yield return attack2.WithDamage(0.2f).Action();
			yield return new AnimationStunAction(_character).Action();
			yield return attack2.WithDamage(0.3f).Action();
			yield return new AnimationStunAction(_character).Action();
			yield return new AnimationDeathAction(_character).Action();
		}
	}
}