﻿using Framework;
using Framework.Client;
using Framework.Client.Timer;
using Framework.RPC;
using Framework.UI;
using Framework.Utils;
using Framework.View;
using Game.Command;
using Game.Model;
using Game.Translations;
using Game.Windows;
using GameShared;
using SAS.Model;
using SAS.Model.Battle;
using SAS.Model.Time;
using TMPro;
using UnityEngine;

namespace Game.View
{
	public class FightButton : ViewBehaviour
	{
		[SerializeField]
		private UIButton _fightButton;
		[SerializeField]
		private TextMeshProUGUI _fightCountText;
		[SerializeField]
		private TextMeshProUGUI _timerText;
		[SerializeField]
		private CanvasGroup _buttonCanvasGroup;
		
		
		private IController _controller;
		private SelectionModel _selectionModel;
		private IWindowManager _windowManager;

		private bool _isMonsterSelected;
		private bool _isCharacterSelected;
		
		private Character _selectedCharacter;

		private ITimer _nextFightSeriesTimer;

		[Inject]
		public void Inject(
				SelectionModel selectionModel,
				IController controller,
				IWindowManager windowManager,
				IUpdateManager updateManager,
				ITooltipManager tooltipManager)
		{
			_selectionModel = selectionModel;
			_controller = controller;
			_windowManager = windowManager;
			tooltipManager.BindTooltip(gameObject,TranslationKeys.FIGHT_BUTTON);
			Subscribe(selectionModel.CurrentChosenCharacter, OnCharacterSelected).Update();
			Subscribe(selectionModel.CurrentChosenMonster, OnMonsterSelected).Update();
			_fightButton.SetOnClick(OnFightButtonClicked);
			_nextFightSeriesTimer = new Timer(updateManager, new TimeDuration(), false);
			Subscribe(_nextFightSeriesTimer.SecondsLeft, OnTimerChanged).Update();
		}

		private void OnMonsterSelected(SAS.Model.Monster monster)
		{
			_isMonsterSelected = monster != null;
			TryChangeButtonState();
		}

		private void OnCharacterSelected(Character character)
		{
			_isCharacterSelected = character != null;
			TryChangeButtonState();
			if (character != null)
			{
				if (_selectedCharacter != null)
				{
					ClearSubscriptions(_selectedCharacter.AvailableFightsCount);
					ClearSubscriptions(_selectedCharacter.NextFightSeries);
				}
				_selectedCharacter = character;
				Subscribe(character.AvailableFightsCount, OnCharacterFightsCountChanged).Update();
				Subscribe(character.NextFightSeries, OnCharacterFightSeriesChanged).Update();
			}
		}

		private void OnCharacterFightSeriesChanged(TimePoint timePoint)
		{
			_nextFightSeriesTimer.SetOnReset(OnTimerReset);
			TimeDuration estimatedTime = CalculateEstimatedTime(timePoint);
			
			if (estimatedTime.IsZero)
			{
				OnTimerReset();
			}
			else
			{
				_nextFightSeriesTimer.SetSecondsLeft(estimatedTime);
				_nextFightSeriesTimer.Start();
			}
		}

		private void OnTimerReset()
		{
			OnCharacterFightsCountChanged((int)_selectedCharacter.Rarity);
		}

		private void OnTimerChanged(int secondsLeft)
		{
			_timerText.gameObject.SetActive(secondsLeft != 0);
			_timerText.text = StringUtils.DurationFormat(secondsLeft);
		}

		private TimeDuration CalculateEstimatedTime(TimePoint value)
		{
			TimePoint now = TimePoint.Now;
			var result = (value - now).Seconds;
			return TimeDuration.CreateBySeconds(result);
		}

		private void OnCharacterFightsCountChanged(int characterFightsCount)
		{
			if (characterFightsCount == (int)_selectedCharacter.Rarity)
			{
				_timerText.gameObject.SetActive(false);
				_nextFightSeriesTimer.Stop();
			}
			_fightCountText.text = $"{characterFightsCount} / {(int)_selectedCharacter.Rarity}";
		}

		private void TryChangeButtonState()
		{
			gameObject.SetActive(_isCharacterSelected || _isMonsterSelected);
			_fightButton.enabled = _isCharacterSelected && _isMonsterSelected;
			
			if (_isCharacterSelected && _isMonsterSelected)
			{
				_buttonCanvasGroup.alpha = 1;
			}
			else if (_isCharacterSelected || _isMonsterSelected)
			{
				_buttonCanvasGroup.alpha = 0.3f;
			}
		}

		private void OnFightButtonClicked()
		{
			var attackMonsterCommand = new AttackMonsterCommand(_selectionModel.CurrentChosenCharacter,
					_selectionModel.CurrentChosenMonster);
			_controller.Execute(attackMonsterCommand);
			attackMonsterCommand.OnResponse<AttackMonster.Response>().Then(OnFightEnded, Log.Exception);
		}

		private void OnFightEnded(AttackMonster.Response result)
		{
			if (result.BattleResult.ResultType == BattleResultType.Win)
			{
				_controller.Execute(new SetCurrentChosenMonsterCommand(null));
			}
			_windowManager.Open<FightResultsWindow>(result);
		}
	}
}