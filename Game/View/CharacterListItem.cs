using System;
using Framework;
using Framework.UI;
using Framework.View;
using SAS.Model;
using SAS.Model.Time;
using UnityEngine;

namespace Game.View
{
	public class CharacterListItem : ViewBehaviour
	{
		[SerializeField]
		private CharacterView _characterView;
		[SerializeField]
		private UIButton _button;
		[SerializeField]
		private Transform _extraObjectsRoot;

		private Character _character;

		private GameObject _dontHavePairGameObject;
		private CharacterNextFightSeriesView _nextFightSeriesView;
		private IPrefabProvider _prefabProvider;
		private ITooltipManager _tooltipManager;

		[Inject]
		public void Inject(IPrefabProvider prefabProvider, ITooltipManager tooltipManager)
		{
			_prefabProvider = prefabProvider;
			_tooltipManager = tooltipManager;
		}

		public void Init(Character character, Action<Character> onClick)
		{
			_character = character;
			_characterView.Init(character, _tooltipManager);
			_button.SetOnClick(() => onClick?.Invoke(_character));
		}

		public void ShowHavePair(bool havePair)
		{
			if (_dontHavePairGameObject == null)
			{
				_dontHavePairGameObject =
						_prefabProvider.InstantiateAt(Resource.View.Character.Extra.DontHavePairPrefab,
								_extraObjectsRoot);
			}
			_dontHavePairGameObject.SetActive(!havePair);
		}

		public void ShowAvailable(bool isAvailable)
		{
			if (!isAvailable && _nextFightSeriesView == null)
			{
				_nextFightSeriesView = _prefabProvider.InstantiateAt<CharacterNextFightSeriesView>(_extraObjectsRoot,true);
			}
			if (!isAvailable)
			{
				_nextFightSeriesView.Init(_character);
			}
		}
	}
}