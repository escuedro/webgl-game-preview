using System;
using System.Collections.Generic;
using Framework;
using Framework.Extension;
using Framework.Reactive;
using Framework.UI;
using Framework.View;
using Game.Command;
using Game.Windows;
using GameShared;
using SAS.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View
{
	public class CharacterListView : ViewBehaviour
	{
		[SerializeField]
		private RectTransform _content;
		[SerializeField]
		private UIButton _addCharacterButton;
		[Inject]
		public IPrefabProvider PrefabProvider;

		private Dictionary<Character, CharacterListItem> _charactersItems =
			new Dictionary<Character, CharacterListItem>();
		
		private IController _controller;
		private IWindowManager _windowManager;
		private Item _contractItem;

		[Inject]
		public void Inject(UserCharacters userCharacters, UserItems userItems, IWindowManager windowManager, IController controller)
		{
			_controller = controller;
			_windowManager = windowManager;
			_contractItem = userItems.GetItem(ItemType.Contract);
			Subscribe(userCharacters.Characters, Refresh);
			UpdateSubscriptions();
			_addCharacterButton.SetOnClick(AddCharacter);
		}

		private void AddCharacter()
		{
			if (_contractItem.Count == 0)
			{
				_windowManager.Open<MessagePopupWindow>("Contract amount is zero!");
			}
			else
			{
				_controller.Execute(new CreateCharacterCommand()).Then(Success, Log.Exception);
			}
		}

		private void Success()
		{
			Log.Info("Character creation successful");
		}

		private void Refresh(ListField<Character> characters)
		{
			if (characters == null || characters.Count == 0)
			{
				_controller.Execute(new SetCurrentChosenCharacterCommand(null));
			}
			_content.DestroyChildren();
			_charactersItems.Clear();
			foreach (Character character in characters)
			{
				CharacterListItem characterListItem = PrefabProvider.InstantiateAt<CharacterListItem>(_content, true);
				_charactersItems.Add(character, characterListItem);
				characterListItem.Init(character, OnCharacterSelected);
			}
			_content.GetComponent<VerticalLayoutGroup>().CalculateLayoutInputVertical();
		}

		private void OnCharacterSelected(Character character)
		{
			// foreach (KeyValuePair<Character,CharacterListItem> keyValuePair in _charactersItems)
			// {
			// 	if (keyValuePair.Key == character)
			// 	{
			// 		keyValuePair.Value.Select();
			// 	}
			// 	else
			// 	{
			// 		keyValuePair.Value.Deselect();
			// 	}
			// }
			_controller.Execute(new SetCurrentChosenCharacterCommand(character));
		}
	}
}