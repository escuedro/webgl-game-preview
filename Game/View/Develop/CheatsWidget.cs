using Framework;
using Framework.UI;
using Framework.View;
using Game.Translations;
using UnityEngine;

namespace Game.View
{
	public class CheatsWidget : ViewBehaviour
	{
		[SerializeField]
		private UIButton _button;
		private IWindowManager _windowManager;

		[Inject]
		public void Init(IWindowManager windowManager, ITooltipManager tooltipManager)
		{
			_windowManager = windowManager;
			tooltipManager.BindTooltip(gameObject,TranslationKeys.CHEATS);
			_button.SetOnClick(OpenWindow);
		}

		private void OpenWindow()
		{
			_windowManager.Open<CheatsWindow>();
		}
	}
}