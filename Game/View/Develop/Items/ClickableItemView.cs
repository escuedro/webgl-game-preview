﻿using Framework.UI;
using Game.View.Shop;
using SAS.Model;
using UnityEngine;

namespace Game.View.Items
{
	public class ClickableItemView : ItemView
	{
		[SerializeField]
		private UIButton _button;
		[SerializeField]
		private UIButton _buttonPlus;
		private IWindowManager _windowManager;
		private Item _item;

		public void Init(Item item, IWindowManager windowManager)
		{
			base.Init(item);
			_item = item;
			_windowManager = windowManager;
			_button.SetOnClick(OpenShop);
			_buttonPlus.SetOnClick(OpenShop);
		}
		void OpenShop()
		{
			_windowManager.Open<ShopWindow>((ItemType)_item.Static.Id);
		}
	}
}