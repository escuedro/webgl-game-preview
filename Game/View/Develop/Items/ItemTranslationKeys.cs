﻿using System;
using Framework;
using Game.Translations;
using SAS.Model;

namespace Game.View.Items
{
	public class ItemTranslationKeys
	{
		public TranslationKey GetTranslationKeyByItemType(ItemType itemType)
		{
			switch (itemType)
			{
				case ItemType.Contract:
					return TranslationKeys.ITEM_CONTRACT;
				case ItemType.Folliant:
					return TranslationKeys.ITEM_FOLIANT;
				case ItemType.Elixir:
					return TranslationKeys.ITEM_ELIXIR;
				case ItemType.Ticket:
					return TranslationKeys.ITEM_TICKET;
				default:
					throw new ArgumentOutOfRangeException(nameof(itemType), itemType, null);
			}
		}
	}
}