using Framework;
using Framework.Client.Translations;
using Framework.View;
using SAS.Model;
using UnityEngine;

namespace Game.View.Items
{
	public class ItemView : ViewBehaviour
	{
		[SerializeField]
		private UIText _itemAmountText;
		[SerializeField]
		private TranslatedText _translatedText;
		[Header("Graphics")]
		[SerializeField]
		private ResourceImage _iconImage;
		[SerializeField]
		private ResourceImage _backgroundImage;

		public void Init(Item item)
		{
			ItemTranslationKeys itemTranslationKeys = new ItemTranslationKeys();
			Subscribe(item.Count, OnItemCountChanged).Update();
			_translatedText.SetTranslationKey(itemTranslationKeys.GetTranslationKeyByItemType((ItemType)item.Static.Id));

			_iconImage.Load(item.Static.IconVisual);
			_backgroundImage.Load(item.Static.BackgroundVisual);
		}

		private void OnItemCountChanged(uint count)
		{
			_itemAmountText.text = count.ToString();
		}
	}
}
