using Framework.Client.Translations;
using Framework.Futures;
using Framework.UI;
using SAS.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View.Items
{
	public class EssenceDescriptionWindow : Window
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private UIButton _closeButton;

		private IWindowAnimation _animation;

		private void Awake()
		{
			_animation = new FadeInOutAnimation(this, _canvasGroup);
			_closeButton.SetOnClick(OnCloseButtonClick);
		}

		private void OnCloseButtonClick()
		{
			Close();
		}

		public override IFuture OnClose()
		{
			return _animation.PlayOut();
		}

		public override IFuture Open()
		{
			return _animation.PlayIn();
		}
	}
}
