using System;
using Framework.Client.Translations;
using Framework.Client.View;
using Framework.View;
using SAS.Model;
using UnityEngine;

public class InventoryPopup : ViewBehaviour
{
	[SerializeField]
	private UIButton upgradeBtn;
	[SerializeField]
	private TranslatedText upgradeText;

	[SerializeField]
	private LinkButton sellBtn;

	public Character Character { get; private set; }

	public void Init(Character character, Action<Character> onSell, Action<Character> onUpgrade)
	{
		Character = character;
		sellBtn.SetOnClick("https://inanomo.com/", () => onSell.Invoke(character));
		upgradeBtn.SetOnClick(() => onUpgrade.Invoke(character));
		upgradeText.InputFormattedData(((int)character.LevelStatic.TokensToNexLevel).ToString());
	}
}
