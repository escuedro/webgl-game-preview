﻿using System;
using Framework;
using Framework.Audio;
using Framework.Client.View;
using Framework.FixedMath;
using Framework.Reactive;
using Framework.UI;
using Game.Audio;
using Game.Translations;
using Game.View.Items;
using Game.Windows;
using GameShared;
using SAS.Model;
using UnityEngine;
using UnityEngine.UI;
using GridLayout = Framework.Client.View.GridLayout;

namespace Game.View.Inventory
{
	public class InventoryScreen : Screen
	{
		[SerializeField]
		private ScrollRect _scroll;
		[SerializeField]
		private GameObject _essenceTooltip;
		[SerializeField]
		private UIText _essenceCountText;
		[SerializeField]
		private ClickableItemView _elixirView;
		[SerializeField]
		private ClickableItemView _foliantView;
		[SerializeField]
		private ClickableItemView _ticketView;
		[SerializeField]
		private GridLayout _heroesLayout;
		[SerializeField]
		private GridViewBehaviour _heroesView;
		[SerializeField]
		private float _recalculateHeight;

		[SerializeField]
		private float _additionalHeight;
		[SerializeField]
		private RectTransform _rect;

		private IWindowManager _windowManager;
		private IPrefabProvider _prefabProvider;
		private IAudioManager _audioManager;
		private IController _controller;
		private ITooltipManager _tooltipManager;

		private UserCharacters _userCharacters;
		private UserChest _userChest;

		private InventoryPopup _lastPopup;
		public override void OnStartVisible()
		{
			_heroesLayout.Recalculate();
			OnEssenceRewardsChanged(_userChest.EssenceRewards);
		}

		[Inject]
		public void Inject(IWindowManager windowManager,
				UserItems userItems,
				IPrefabProvider prefabProvider,
				IAudioManager audioManager,
				IController controller,
				UserChest userChest,
				UserCharacters userCharacters, ITooltipManager tooltipManager)
		{
			_windowManager = windowManager;
			_controller = controller;
			_userCharacters = userCharacters;
			_userChest = userChest;
			_prefabProvider = prefabProvider;
			_audioManager = audioManager;
			_tooltipManager = tooltipManager;
			Subscribe(_userChest.EssenceRewards, OnEssenceRewardsChanged).Update();
			tooltipManager.BindTooltip(_essenceTooltip,TranslationKeys.ESSENCE_TOOLTIP);
			tooltipManager.BindTooltip(_elixirView.gameObject,TranslationKeys.ELIXIR_TOOLTIP);
			tooltipManager.BindTooltip(_foliantView.gameObject,TranslationKeys.FOLIANT_TOLTIP);
			tooltipManager.BindTooltip(_ticketView.gameObject,TranslationKeys.TICKET_TOOLTIP);
			_elixirView.Init(userItems.GetItem(ItemType.Elixir), _windowManager);
			_foliantView.Init(userItems.GetItem(ItemType.Folliant), _windowManager);
			_ticketView.Init(userItems.GetItem(ItemType.Ticket), _windowManager);
			_heroesLayout.OnRecalculated += OnLayoutRecalculated;
			_heroesView.Init<Character, CharacterListItem>(_prefabProvider, _userCharacters.Characters, true,
					(character, characterListItem) =>
					{
						characterListItem.Init(character,
								clicked => OnCharacterClicked(characterListItem, clicked));
						characterListItem.ShowAvailable(character.IsAvailable);
						ClearSubscriptions(character.NextFightSeries);
						Subscribe(character.AvailableFightsCount,
								(_) => OnFightsCountChanged(characterListItem, character));
					},
					character => character.Availability.Value == CharacterAvailability.Available
			);
		}

		private void OnApplicationFocus(bool hasFocus)
		{
			if (!hasFocus)
			{
				_scroll.StopMovement();
				_scroll.enabled = false;
			}
			else
			{
				_scroll.enabled = true;
			}
		}

		private void OnEssenceRewardsChanged(ListField<EssenceReward> essenceRewards)
		{
			uint essenceCount = 0;
			foreach (EssenceReward reward in essenceRewards)
			{
				essenceCount += reward.Count.Value;
			}
			_essenceCountText.text = essenceCount.ToString();
		}

		private void OnFightsCountChanged(CharacterListItem characterListItem, Character character)
		{
			characterListItem.ShowAvailable(character.IsAvailable);
		}

		private void OnLayoutRecalculated(float newHeight)
		{
			_rect.sizeDelta = new Vector2(_rect.sizeDelta.x, _additionalHeight + newHeight + _recalculateHeight);
			_heroesLayout.Rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, newHeight + _recalculateHeight);
		}

		private void OnCharacterClicked(CharacterListItem item, Character character)
		{
			if (_lastPopup != null)
			{
				if (_lastPopup.Character == character)
				{
					return;
				}
				Destroy(_lastPopup.gameObject);
			}
			_lastPopup = _prefabProvider.InstantiateAt<InventoryPopup>(item.transform, false);
			_lastPopup.Init(character, OnCharacterSell, OnCharacterUpgrade);
			_lastPopup.transform.SetAsFirstSibling();
		}

		private void OnCharacterSell(Character character)
		{
			Destroy(_lastPopup.gameObject);
			//todo: on sell response set in stocks character.Availability.Value = CharacterAvailability.InStocks;
		}

		private void OnCharacterUpgrade(Character character)
		{
			Destroy(_lastPopup.gameObject);
			if (character.Availability == CharacterAvailability.InStocks)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.INVENTORY_WINDOW_WARNING_STOCKS);
			}
			if (character.IsMaximumLevel)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.INVENTORY_WINDOW_WARNING_MAXIMUM_LEVEL);
			}
			CharacterLevelSharedStatic levelStatic = character.LevelStatic;
			if (levelStatic.ExperienceToNextLevel > character.CurrentExperience)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.INVENTORY_WINDOW_WARNING_EXPERIENCE);
			}

			if (_userChest.TokensAvailable < new Fix64((int)levelStatic.TokensToNexLevel))
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.INVENTORY_WINDOW_WARNING_TOKENS);
			}

			_audioManager.PlaySound(SoundType.SoundLvlUp);

			_controller.Execute(new LevelUpCharacterCommand(character)).Then(() => ShowLevelUpInfo(character), Log.Exception);
		}

		private void ShowLevelUpInfo(Character character)
		{
			_windowManager.Open<CharacterLevelUpWindow>(character, _tooltipManager);
		}
	}
}