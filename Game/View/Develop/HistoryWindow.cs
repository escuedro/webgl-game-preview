using System.Collections.Generic;
using Framework;
using Framework.Client.View;
using Framework.Futures;
using Framework.UI;
using SAS.Model;
using SAS.Model.History;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View
{
	public class HistoryWindow : Window
	{
		private FadeInOutAnimation _animation;
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private GridViewBehaviour _gridViewBehaviour;
		[SerializeField]
		private VerticalLayoutGroup _rootLayout;
		[SerializeField]
		private UIButton _closeButton;

		private UserCharacters _userCharacters;

		private IComparer<HistoryItem> _historyDataComparer = new HistoryItemComparer();

		private void Awake()
		{
			_animation = new FadeInOutAnimation(this, _canvasGroup);
			_closeButton.SetOnClick(Close);
		}

		[Inject]
		public void Inject(IPrefabProvider prefabProvider, UserHistory userHistory, UserCharacters userCharacters)
		{
			_userCharacters = userCharacters;
			_gridViewBehaviour.Init<HistoryItem, HistoryItemView>(prefabProvider, userHistory.History, true, OnItemCreated, ShowCondition, _historyDataComparer);
		}

		private bool ShowCondition(HistoryItem historyItem)
		{
			return _userCharacters.TryGet(historyItem.HistoryData.CharacterId) != null;
		}

		private void OnItemCreated(HistoryItem item, HistoryItemView view)
		{
			view.Init(item.HistoryData);
			_rootLayout.CalculateLayoutInputVertical();
		}

		public override IFuture OnClose()
		{
			return _animation.PlayOut();
		}

		public override IFuture Open()
		{
			return _animation.PlayIn();
		}
	}
}