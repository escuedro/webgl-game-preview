using Framework;
using Framework.Futures;
using Framework.UI;
using GameShared;
using TMPro;
using UnityEngine;

namespace Game.View
{
	public class CheatsWindow : Window
	{
		private FadeInOutAnimation _animation;
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private UIButton _addTokensButton;
		[SerializeField]
		private TMP_InputField _countTokensField;
		[SerializeField]
		private UIButton _clearCharactersButton;
		[SerializeField]
		private UIButton _clearBossesButton;
		[SerializeField]
		private UIButton _addBossesButton;
		[SerializeField]
		private UIButton _resetCooldownsButton;

		private void Awake()
		{
			_animation = new FadeInOutAnimation(this, _canvasGroup);
			_addTokensButton.SetOnClick(AddTokens);
			_clearCharactersButton.SetOnClick(ClearCharacters);
			_clearBossesButton.SetOnClick(ClearBosses);
			_addBossesButton.SetOnClick(AddBosses);
			_resetCooldownsButton.SetOnClick(ResetCharactersCooldowns);
		}

		private void ResetCharactersCooldowns()
		{
			Execute(new DebugResetCharacterCooldownsCommand()).Catch(Log.Exception);
		}

		private void AddBosses()
		{
			Execute(new DebugSpawnBossesCommand()).Then(() => { Execute(new GetBossesCommand()).Catch(Log.Exception); },
					Log.Exception);
		}

		private void ClearCharacters()
		{
			Execute(new DebugClearCharactersCommand()).Catch(Log.Exception);
		}

		private void AddTokens()
		{
			long value = 0;
			if (long.TryParse(_countTokensField.text, out value))
			{
				Execute(new DebugAddTokensCommand(value)).Catch(Log.Exception);
			}
		}

		private void ClearBosses()
		{
			Execute(new ClearBossesCommand()).Catch(Log.Exception);
		}

		public override IFuture OnClose()
		{
			return _animation.PlayOut();
		}

		public override IFuture Open()
		{
			return _animation.PlayIn();
		}
	}
}