using Framework;
using Framework.Audio;
using Framework.Client.Translations;
using Framework.FixedMath;
using Framework.Futures;
using Framework.Model;
using Framework.UI;
using Game.Audio;
using Game.View.Items;
using Game.Windows;
using GameShared;
using SAS.Model;
using SAS.Model.Shop;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View.Shop
{
	public class ShopWindow : Window<ItemType>
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private TranslatedText _descriptionText;
		[SerializeField]
		private Slider _countSlider;
		[SerializeField]
		private UIText _currentAmountText;
		[SerializeField]
		private ItemView _currentItemView;
		[SerializeField]
		private UIText _priceText;
		[SerializeField]
		private UIButton _buyButton;
		[SerializeField]
		private UIButton _decreaseButton;
		[SerializeField]
		private UIButton _increaseButton;

		private IWindowAnimation _animation;
		
		private IController _controller;
		private IAudioManager _audioManager;
		private IWindowManager _windowManager;

		private ShopPositionStatic _currentShopPosition;

		private UserChest _userChest;
		private UserItems _userItems;

		private int _currentCount;
		private uint _price;

		private void Awake()
		{
			_animation = new FadeInOutAnimation(this, _canvasGroup, 0.25f);
		}

		[Inject]
		public void Inject(IController controller, IWindowManager windowManager, IAudioManager audioManager, UserChest userChest, UserItems userItems)
		{
			_controller = controller;
			_windowManager = windowManager;
			_audioManager = audioManager;
			_userChest = userChest;
			_userItems = userItems;
			_countSlider.onValueChanged.AddListener(OnCountChanged);
			_buyButton.SetOnClick(OnBuyButtonClicked);
			_decreaseButton.SetOnClick(OnDecreaseButtonClicked);
			_increaseButton.SetOnClick(OnIncreaseButtonClicked);
		}

		private void OnIncreaseButtonClicked()
		{
			_countSlider.value += 1;
		}

		private void OnDecreaseButtonClicked()
		{
			_countSlider.value -= 1;
		}

		private void OnBuyButtonClicked()
		{
			if (_userChest.TokensAvailable.Value < (Fix64)_price)
			{
				_windowManager.Open<NotEnoughMoneyWindow>();
				return;
			}
			_controller.Execute(new BuyShopPositionCommand(_currentShopPosition.Id, (uint)_currentCount)).
					Then(Resolved, Log.Exception);
		}

		private void Resolved()
		{
			_audioManager.PlaySound(SoundType.SoundPurchase);
			Log.Info("Success buy!");
			_windowManager.Close<ShopWindow>();
		}

		private void OnCountChanged(float value)
		{
			_currentCount = (int)value;
			_currentAmountText.text = _currentCount.ToString();
			_price = (uint)(_currentShopPosition.PriceInTokens * _currentCount);
			_priceText.text = _price.ToString();
		}

		public override IFuture OnClose()
		{
			return _animation.PlayOut();
		}

		protected override IFuture Open(ItemType itemType)
		{
			Item item = _userItems.GetItem(itemType);
			_currentShopPosition = Statics.Get<ShopPositionStatic>(item.Static.Id);
			_descriptionText.SetTranslationKey(new TranslationKey(item.Static.DescriptionTranslationKey));
			_currentItemView.Init(item);
			OnCountChanged(_countSlider.value);
			return _animation.PlayIn();
		}
	}
}