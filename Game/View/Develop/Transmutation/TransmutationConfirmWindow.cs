﻿using Framework.Client.Translations;
using Framework.Futures;
using Framework.Model;
using Framework.UI;
using SAS.Model.Battle;
using UnityEngine;

namespace Game.View.Transmutation
{
	public class TransmutationConfirmWindow : Window<Promise<bool>>
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private UIButton _confirmButton;
		[SerializeField]
		private UIButton _closeButton;
		[SerializeField]
		private TranslatedText _confirmDescriptionText;
		
		private IWindowAnimation _animation;
		private Promise<bool> _promise;

		private void Awake()
		{
			_animation = new FadeInOutAnimation(this, _canvasGroup);
			_confirmButton.SetOnClick(OnConfirmButtonClick);
			_closeButton.SetOnClick(OnCloseButtonClick);
		}

		private void OnCloseButtonClick()
		{
			Close();
		}

		private void OnConfirmButtonClick()
		{
			_promise.Resolve(true);
			Close();
		}

		public override IFuture OnClose()
		{
			if (!_promise.IsResolved)
			{
				_promise.Resolve(false);
			}
			return _animation.PlayOut();
		}

		protected override IFuture Open(Promise<bool> promise)
		{
			TransmutationSettings transmutationSettings = Statics.GetSingle<TransmutationSettings>();
			_confirmDescriptionText.InputFormattedData(((float)transmutationSettings.CharacterDeathChance / 10).ToString());
			_promise = promise;
			return _animation.PlayIn();
		}
	}
}