﻿using System;
using System.Runtime.InteropServices;
using Framework;
using Framework.Audio;
using Framework.Client.Translations;
using Framework.Futures;
using Framework.Model;
using Framework.RPC;
using Framework.UI;
using Game.Audio;
using Game.Translations;
using Game.View.Items;
using Game.Windows;
using GameShared;
using SAS.Model;
using SAS.Model.Battle;
using UnityEngine;
using UnityEngine.UI;

namespace Game.View.Transmutation
{
	public class TransmutationScreen : Screen
	{
		[SerializeField]
		private ClickableItemView _elixirView;
		[SerializeField]
		private ClickableItemView _foliantView;
		[SerializeField]
		private UIButton _firstCharacterPosition;
		[SerializeField]
		private UIButton _secondCharacterPosition;
		[SerializeField]
		private CharacterView _firstCharacterView;
		[SerializeField]
		private CharacterView _secondCharacterView;
		[SerializeField]
		private CharacterView _transmutatedCharacterView;
		[SerializeField]
		private Toggle _toggle;
		[Header("States gameobjects")]
		[SerializeField]
		private GameObject _successState;
		[SerializeField]
		private GameObject _failedCharacter1State;
		[SerializeField]
		private GameObject _failedCharacter2State;
		[SerializeField]
		private GameObject _failedTransmutatedCharacter;
		[Header("Button")]
		[SerializeField]
		private UIButton _transmutationButton;
		[Header("Translated description")]
		[SerializeField]
		private TranslatedText _descriptionTranslatedText;

		private IWindowManager _windowManager;
		private IController _controller;
		private IAudioManager _audioManager;
		private IFuture<Character> _firstCharacterSelectFuture;
		private IFuture<Character> _secondCharacterSelectFuture;
		private IFuture<bool> _confirmationFuture;

		private Character _firstCharacter;
		private Character _secondCharacter;
		private UserCharacters _userCharacters;

		private Item _elixirItem;
		private Item _folliantItem;
		private ITooltipManager _tooltipManager;

		[Inject]
		public void Inject(
				IWindowManager windowManager,
				IController controller,
				ITooltipManager tooltipManager,
				IAudioManager audioManager,
				UserItems userItems,
				UserCharacters userCharacters)
		{
			_windowManager = windowManager;
			_controller = controller;
			_tooltipManager = tooltipManager;
			_audioManager = audioManager;
			_userCharacters = userCharacters;
			_elixirItem = userItems.GetItem(ItemType.Elixir);
			_folliantItem = userItems.GetItem(ItemType.Folliant);
			_elixirView.Init(_elixirItem, windowManager);
			_tooltipManager.BindTooltip(_elixirView.gameObject, TranslationKeys.ELIXIR_TOOLTIP);
			_tooltipManager.BindTooltip(_toggle.gameObject, TranslationKeys.ELIXIR_TOOLTIP);
			_foliantView.Init(_folliantItem, windowManager);
			_tooltipManager.BindTooltip(_foliantView.gameObject, TranslationKeys.FOLIANT_TOLTIP);
			_toggle.onValueChanged.AddListener(OnToggleValueChanged);
			_firstCharacterPosition.SetOnClick(OnFirstCharacterPositionClicked);
			_secondCharacterPosition.SetOnClick(OnSecondCharacterPositionClicked);
			_transmutationButton.SetOnClick(OnTransmutationButtonClicked);

			LoadDescription();
		}

		private void OnToggleValueChanged(bool isOn)
		{
			if (_elixirItem.Count == 0)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.TRANSMUTE_WINDOW_WARNING_NOT_ENOUGH_ELIXIRS);
				_toggle.isOn = false;
			}
		}

		private void LoadDescription()
		{
			TransmutationSettings transmutationSettings = Statics.GetSingle<TransmutationSettings>();
			_descriptionTranslatedText.InputFormattedData($"{(float)transmutationSettings.CharacterDeathChance / 1000}%".ToString());
		}

		private void OnTransmutationButtonClicked()
		{
			if (_folliantItem.Count <= 0)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.TRANSMUTE_WINDOW_WARNING_NOT_ENOUGH_FOLIANTS);
				return;
			}
			if (_firstCharacter == null || _secondCharacter == null)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.TRANSMUTE_WINDOW_WARNING_NOT_ENOUGH_CHARACTERS);
				return;
			}
			if (_toggle.isOn && _elixirItem.Count == 0)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.TRANSMUTE_WINDOW_WARNING_NOT_ENOUGH_ELIXIRS);
				return;
			}
			bool useElixir = _toggle.isOn;
			if (useElixir)
			{
				_audioManager.PlaySound(SoundType.SoundElixirUsed);
			}
			if (!useElixir)
			{
				Promise<bool> confirmationPromise = PromisePool.Pull<bool>();
				_confirmationFuture = FuturePool.Pull(confirmationPromise);
				_windowManager.Open<TransmutationConfirmWindow>(confirmationPromise);
				_confirmationFuture.Then(OnConfirm, Log.Exception);
			}
			else
			{
				Transmutate();
			}
		}

		private void Transmutate()
		{
			TransmutateCommand transmutateCommand =
					new TransmutateCommand(_toggle.isOn, _firstCharacter.Id, _secondCharacter.Id);
			transmutateCommand.OnResponse<Transmutate.Response>().Then(OnTransmutationResponseReceived, Log.Exception);
			_transmutationButton.EnableAfter(_controller.Execute(transmutateCommand));
		}

		private void OnConfirm(bool confirmed)
		{
			if (confirmed)
			{
				Transmutate();
			}
		}

		private void OnTransmutationResponseReceived(Transmutate.Response result)
		{
			if (result.Success)
			{
				_audioManager.PlaySound(SoundType.SoundMerge);
				_transmutatedCharacterView.gameObject.SetActive(true);
				_transmutatedCharacterView.Init(_userCharacters.TryGet(result.Character.Id), _tooltipManager);
				_successState.SetActive(true);
				switch (result.Character.Rarity)
				{
					case CharacterRarity.Undefined:
						break;
					case CharacterRarity.Common:
						_audioManager.PlaySound(SoundType.SoundRarity1);
						break;
					case CharacterRarity.Uncommon:
						_audioManager.PlaySound(SoundType.SoundRarity2);
						break;
					case CharacterRarity.Rare:
						_audioManager.PlaySound(SoundType.SoundRarity3);
						break;
					case CharacterRarity.Epic:
						_audioManager.PlaySound(SoundType.SoundRarity4);
						break;
					case CharacterRarity.Legendary:
						_audioManager.PlaySound(SoundType.SoundRarity5);
						break;
					case CharacterRarity.Mythical:
						_audioManager.PlaySound(SoundType.SoundRarity6);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			if (!result.Success)
			{
				_audioManager.PlaySound(SoundType.SoundMergeFailure);
				_failedTransmutatedCharacter.SetActive(true);
			}
			if (result.RemoveCharacter1)
			{
				_failedCharacter1State.SetActive(true);
				_firstCharacter = null;
			}
			if (result.RemoveCharacter2)
			{
				_failedCharacter2State.SetActive(true);
				_secondCharacter = null;
			}
			Disposed = true;
		}

		private void OnFirstCharacterPositionClicked()
		{
			Promise<Character> characterPromise = PromisePool.Pull<Character>();
			_firstCharacterSelectFuture = FuturePool.Pull(characterPromise);
			_windowManager.Open<CharacterSelectWindow>(characterPromise,
					new Predicate<Character>(IsFirstCharacterCorrect),
					new Action<Character, CharacterListItem>(OnCharacterItemInitialized));
			_firstCharacterSelectFuture.Then(OnFirstCharacterChosen, (e) => { });
		}

		private void OnCharacterItemInitialized(Character character, CharacterListItem characterListItem)
		{
			characterListItem.ShowHavePair(_userCharacters.HasPair(character));
		}

		private bool IsFirstCharacterCorrect(Character character)
		{
			return IsCharactersCorrect(_secondCharacter, character);
		}

		private bool IsSecondCharacterCorrect(Character character)
		{
			return IsCharactersCorrect(_firstCharacter, character);
		}

		private bool IsCharactersCorrect(Character firstCharacter, Character secondCharacter)
		{
			if (firstCharacter == null)
			{
				if (secondCharacter.IsMaximumRarity || secondCharacter.Availability.Value == CharacterAvailability.InStocks)
				{
					return false;
				}
				return true;
			}
			if (firstCharacter == secondCharacter)
			{
				return false;
			}
			bool sameStatic = firstCharacter.Static.Id == secondCharacter.Static.Id;
			bool sameRarity = firstCharacter.Rarity == secondCharacter.Rarity;
			if (!sameStatic || !sameRarity)
			{
				return false;
			}

			return true;
		}

		private void OnFirstCharacterChosen(Character firstCharacter)
		{
			_firstCharacter = firstCharacter;
			_firstCharacterView.Init(firstCharacter, _tooltipManager);
			_firstCharacterView.gameObject.SetActive(true);
		}

		private void OnSecondCharacterChosen(Character secondCharacter)
		{
			_secondCharacter = secondCharacter;
			_secondCharacterView.Init(secondCharacter, _tooltipManager);
			_secondCharacterView.gameObject.SetActive(true);
		}

		private void OnSecondCharacterPositionClicked()
		{
			Promise<Character> characterPromise = PromisePool.Pull<Character>();
			_secondCharacterSelectFuture = FuturePool.Pull(characterPromise);
			_windowManager.Open<CharacterSelectWindow>(characterPromise,
					new Predicate<Character>(IsSecondCharacterCorrect),
					new Action<Character, CharacterListItem>(OnCharacterItemInitialized));
			_secondCharacterSelectFuture.Then(OnSecondCharacterChosen, (e) => { });
		}
	}
}