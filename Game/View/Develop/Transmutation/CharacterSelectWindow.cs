﻿using System;
using Framework;
using Framework.Audio;
using Framework.Client.View;
using Framework.Futures;
using Framework.UI;
using Game.Audio;
using Game.Translations;
using SAS.Model;
using UnityEngine;

namespace Game.View.Transmutation
{
	public class CharacterSelectWindow : Window<Promise<Character>, Predicate<Character>, Action<Character, CharacterListItem>>
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private GridViewBehaviour _gridViewBehaviour;
		[SerializeField]
		private GameObject _info;

		private IWindowAnimation _windowAnimation;
		private Promise<Character> _characterFuture;

		private IAudioManager _audioManager;
		private IPrefabProvider _prefabProvider;
		private UserCharacters _userCharacters;
		private SelectCharacterPopup _lastPopup;
		
		private ITooltipManager _tooltipManager;

		[Inject]
		public void Inject(IPrefabProvider prefabProvider, IAudioManager audioManager, ITooltipManager tooltipManager, UserCharacters userCharacters)
		{
			_prefabProvider = prefabProvider;
			_audioManager = audioManager;
			_tooltipManager = tooltipManager;
			_userCharacters = userCharacters;
			
			_tooltipManager.BindTooltip(_info, TranslationKeys.SELECTION_WINDOW_TOOLTIP);
		}

		private void OnCharacterClicked(Character character, CharacterListItem characterListItem)
		{
			if (_lastPopup != null)
			{
				if (_lastPopup.Character == character)
				{
					return;
				}
				Destroy(_lastPopup.gameObject);
			}
			_lastPopup = _prefabProvider.InstantiateAt<SelectCharacterPopup>(characterListItem.transform, false);
			_lastPopup.transform.SetAsFirstSibling();
			_lastPopup.Init(character, OnCharacterSelected);
			_audioManager.PlaySound(SoundType.SoundCharacterSelection);
		}

		private void OnCharacterSelected(Character character)
		{
			_characterFuture.Resolve(character);
			Close();
		}

		private void Awake()
		{
			_windowAnimation = new FadeInOutAnimation(this, _canvasGroup, 0.1f);
		}

		public override IFuture OnClose()
		{
			if (!_characterFuture.IsResolved)
			{
				_characterFuture.Reject(new Exception("Window was closed"));
			}
			return _windowAnimation.PlayOut();
		}

		protected override IFuture Open(
				Promise<Character> future,
				Predicate<Character> showCondition,
				Action<Character, CharacterListItem> onCharacterItemInited)
		{
			_characterFuture = future;
			_gridViewBehaviour.Init<Character, CharacterListItem>(_prefabProvider, _userCharacters.Characters, true,
					(character, characterListItem) =>
					{
						characterListItem.Init(character, (character) => OnCharacterClicked(character, characterListItem));
						onCharacterItemInited?.Invoke(character, characterListItem);
					},
					showCondition);
			return _windowAnimation.PlayIn();
		}
	}
}