﻿using Framework;
using Framework.Client.Translations;
using Framework.Futures;
using Framework.UI;
using UnityEngine;

namespace Game.Windows
{
	public class MessagePopupWindow : Window<TranslationKey>
	{
		[SerializeField]
		private TranslatedText _messageText;
		[SerializeField]
		private UIButton _confirmButton;

		private IWindowAnimation _animation;

		private void Awake()
		{
			_animation = new ScaleAnimation(this, transform, 0.3f);
			_confirmButton.SetOnClick(Close);
		}

		public override IFuture OnClose()
		{
			return _animation.PlayOut();
		}

		protected override IFuture Open(TranslationKey message)
		{
			_messageText.SetTranslationKey(message);
			return _animation.PlayIn();
		}
	}
}