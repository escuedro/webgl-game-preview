﻿using Framework.Futures;
using Framework.UI;
using GameShared;
using SAS.Model.Battle;
using TMPro;
using UnityEngine;

namespace Game.Windows
{
	public class FightResultsWindow : Window<AttackMonster.Response>
	{
		[SerializeField]
		private TextMeshProUGUI _battleResultText;
		[SerializeField]
		private TextMeshProUGUI _experienceText;
		[SerializeField]
		private UIButton _okButton;

		private IWindowAnimation _windowAnimation;

		private void Awake()
		{
			_windowAnimation = new ScaleAnimation(this, transform, 0.25f);
			_okButton.SetOnClick(Close);
		}

		public override IFuture OnClose()
		{
			return _windowAnimation.PlayOut();
		}

		protected override IFuture Open(AttackMonster.Response argument)
		{
			bool win = argument.BattleResult.ResultType == BattleResultType.Win;
			if (win)
			{
				_battleResultText.text = "You win the fight!";
			}
			else
			{
				_battleResultText.text = "You lose the fight!";
			}

			_experienceText.text = $"You got {argument.Experience} EXP";
			return _windowAnimation.PlayIn();
		}
	}
}