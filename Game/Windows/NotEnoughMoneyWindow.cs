using Framework.Client.View;
using Framework.Futures;
using Framework.UI;
using UnityEngine;

namespace Game.Windows
{
	public class NotEnoughMoneyWindow : Window
	{
		[SerializeField]
		private UIButton _closeButton;
		[SerializeField]
		private LinkButton _openMarketplace;

		private IWindowAnimation _windowAnimation;

		private void Awake()
		{
			_windowAnimation = new ScaleAnimation(this, transform, 0.25f);
			_closeButton.SetOnClick(Close);
			_openMarketplace.SetOnClick("https://inanomo.com/", null);
		}

		public override IFuture OnClose()
		{
			return _windowAnimation.PlayOut();
		}

		public override IFuture Open()
		{
			return _windowAnimation.PlayIn();
		}
	}
}