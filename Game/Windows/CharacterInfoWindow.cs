﻿using Framework;
using Framework.FixedMath;
using Framework.Futures;
using Framework.RPC;
using Framework.UI;
using Game.Translations;
using GameShared;
using SAS.Model;
using TMPro;
using UnityEngine;

namespace Game.Windows
{
	public class CharacterInfoWindow : Window<Character>
	{
		[SerializeField]
		private UIText _idText;
		[SerializeField]
		private UIText _staticText;
		[SerializeField]
		private UIText _levelText;
		[SerializeField]
		private UIText _experienceText;
		[SerializeField]
		private UIText _elementText;
		[SerializeField]
		private UIText _rarityText;
		[SerializeField]
		private UIText _availabilityText;
		[SerializeField]
		private UIButton _closeButton;
		[SerializeField]
		private UIButton _levelUpButton;
		private IWindowAnimation _windowAnimation;
		private IController _controller;
		private IWindowManager _windowManager;
		private UserChest _userChest;
		private Character _character;

		[Inject]
		public void Inject(IController controller, IWindowManager windowManager, UserChest userChest)
		{
			_controller = controller;
			_windowManager = windowManager;
			_userChest = userChest;
		}

		private void Awake()
		{
			_windowAnimation = new ScaleAnimation(this, transform, 0.25f);
			_closeButton.SetOnClick(Close);
			_levelUpButton.SetOnClick(OnLevelUpClicked);
		}

		private void OnLevelUpClicked()
		{
			if (_character.Availability == CharacterAvailability.InStocks)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.INVENTORY_WINDOW_WARNING_STOCKS);
			}
			if (_character.IsMaximumLevel)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.INVENTORY_WINDOW_WARNING_MAXIMUM_LEVEL);
			}
			CharacterLevelSharedStatic levelStatic = _character.LevelStatic;
			if (levelStatic.ExperienceToNextLevel > _character.CurrentExperience)
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.INVENTORY_WINDOW_WARNING_EXPERIENCE);
			}

			if (_userChest.TokensAvailable < new Fix64((int)levelStatic.TokensToNexLevel))
			{
				_windowManager.Open<MessagePopupWindow>(TranslationKeys.INVENTORY_WINDOW_WARNING_TOKENS);
			}

			var levelUpCommand = new LevelUpCharacterCommand(_character);
			_controller.Execute(levelUpCommand);
			levelUpCommand.OnResponse<EmptyRpc.Response>().Then(ShowInfo, Log.Exception);
		}

		public override IFuture OnClose()
		{
			return _windowAnimation.PlayOut();
		}

		protected override IFuture Open(Character character)
		{
			_character = character;
			ShowInfo();
			return _windowAnimation.PlayIn();
		}

		private void ShowInfo()
		{
			_idText.text = "ID: " + _character.Id + " " + _character.Guid;
			_levelText.text = "Level: " + _character.Level;
			_experienceText.text = "Exp: " + _character.CurrentExperience;
			_elementText.text = "Element: " + _character.Element;
			_rarityText.text = "Rarity: " + _character.Rarity;
			_availabilityText.text = "Availability: " + _character.Availability.Value;
		}
	}
}