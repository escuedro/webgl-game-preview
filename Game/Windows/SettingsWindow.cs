using System;
using Framework;
using Framework.Audio;
using Framework.Futures;
using Framework.Model.Localization;
using Framework.UI;
using Game.Command;
using Game.Model;
using Game.Translations;
using GameShared;
using SAS.Model.User;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Windows
{
	public class SettingsWindow : Window
	{
		private FadeInOutAnimation _animation;
		[SerializeField]
		private CanvasGroup _canvasGroup;

		[SerializeField]
		private UIText _languageText;
		[SerializeField]
		private UIButton _nextLanguage;
		[SerializeField]
		private UIButton _previousLanguage;
		[SerializeField]
		private Slider _soundsSlider;
		[SerializeField]
		private UIText _soundsValue;
		[SerializeField]
		private UIButton _closeButton;

		[Inject]
		public LocalSettings LocalSettings;

		private TranslationSwitcher _translationSwitcher;
		private int _maxLanguage;
		private Language _currentLanguage;
		
		private IAudioManager _audioManager;

		[Inject]
		public void Prepare(IAudioManager audioManager, TranslationSwitcher translationSwitcher)
		{
			_audioManager = audioManager;
			_animation = new FadeInOutAnimation(this, _canvasGroup);
			_soundsSlider.onValueChanged.AddListener(OnSoundSlider);
			_soundsSlider.value = LocalSettings.SoundsVolume;
			_previousLanguage.SetOnClick(PreviousLanguage);
			_nextLanguage.SetOnClick(NextLanguage);
			_closeButton.SetOnClick(Close);

			_translationSwitcher = translationSwitcher;

			_maxLanguage = Enum.GetNames(typeof(Language)).Length - 1;
			_languageText.text = LocalSettings.Language.Value.ToPrintName();
			UpdateSubscriptions();
		}

		private void PreviousLanguage()
		{
			ChangeLanguage(-1);
		}

		private void NextLanguage()
		{
			ChangeLanguage(1);
		}

		private void ChangeLanguage(int next)
		{
			int currentLanguage = (int)_currentLanguage;
			currentLanguage += next;
			if (currentLanguage > _maxLanguage)
			{
				currentLanguage = 1;
			}
			else if (currentLanguage <= 0)
			{
				currentLanguage = _maxLanguage;
			}
			Language newLanguage = (Language)currentLanguage;

			_languageText.text = newLanguage.ToPrintName();
			_translationSwitcher.SwitchLanguage(newLanguage);
			_currentLanguage = newLanguage;
		}

		private void OnSoundSlider(float progress)
		{
			int soundVolume = (int)progress;
			_audioManager.SetVolume(progress / 100f);
			_soundsValue.text = soundVolume.ToString();
		}

		public override IFuture OnClose()
		{
			int soundsVolume = (int)_soundsSlider.value;
			if (LocalSettings.Language.Value != _currentLanguage || LocalSettings.SoundsVolume.Value != soundsVolume)
			{
				Execute(new SetSoundSettingsCommand(_currentLanguage, soundsVolume)).
						Then(() => Log.Info("SetSettings success"), Log.Exception);
				Execute(new SetLocalSettingsVolume(soundsVolume, _currentLanguage));
			}
			return _animation.PlayOut();
		}

		public override IFuture Open()
		{
			_currentLanguage = LocalSettings.Language;
			return _animation.PlayIn();
		}
	}
}