﻿using Framework;
using Game.Model;
using SAS.Model;

namespace Game.Command
{
	public class SetBossFightCharacterCommand : SyncCommand
	{
		private int _index;
		private Character _character;

		public SetBossFightCharacterCommand(int index, Character character)
		{
			_index = index;
			_character = character;
		}

		public override void Execute()
		{
			BossSelectionModel bossSelectionModel = Get<BossSelectionModel>();
			foreach (CharacterIndexPair characterIndexPair in bossSelectionModel.Characters)
			{
				if (characterIndexPair.Index == _index)
				{
					bossSelectionModel.Characters.Remove(characterIndexPair);
					bossSelectionModel.Characters.Add(new CharacterIndexPair() { Character = _character, Index = _index});
					return;
				}
			}
			bossSelectionModel.Characters.Add(new CharacterIndexPair() {Character = _character, Index = _index});
		}
	}
}