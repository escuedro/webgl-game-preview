﻿using Framework;
using Game.Model;

namespace Game.Command
{
	public class ResetBossFightCharactersCommand : SyncCommand
	{
		public override void Execute()
		{
			Get<BossSelectionModel>().Characters.Clear();
		}
	}
}