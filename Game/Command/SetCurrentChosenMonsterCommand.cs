﻿using Framework;
using Game.Model;
using SAS.Model;

namespace Game.Command
{
    public class SetCurrentChosenMonsterCommand : SyncCommand
    {
        private Monster _monster;

        public SetCurrentChosenMonsterCommand(Monster monster)
        {
            _monster = monster;
        }

        public override void Execute()
        {
            SelectionModel selectionModel = Get<SelectionModel>();
            selectionModel.CurrentChosenMonster.Value = _monster;
        }
    }
}