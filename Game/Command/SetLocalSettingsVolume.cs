﻿using Framework;
using Framework.Model.Localization;
using Game.Model;

namespace Game.Command
{
	public class SetLocalSettingsVolume : SyncCommand
	{
		private readonly int _newValue;
		private readonly Language _language;

		public SetLocalSettingsVolume(int newValue, Language language)
		{
			_newValue = newValue;
			_language = language;
		}

		public override void Execute()
		{
			LocalSettings localSettings = Get<LocalSettings>();
			localSettings.SoundsVolume.Value = _newValue;
			localSettings.Language.Value = _language;
		}
	}
}