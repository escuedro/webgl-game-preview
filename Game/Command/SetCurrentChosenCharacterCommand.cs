﻿using Framework;
using Game.Model;
using SAS.Model;

namespace Game.Command
{
    public class SetCurrentChosenCharacterCommand : SyncCommand
    {
        private Character _character;

        public SetCurrentChosenCharacterCommand(Character character)
        {
            _character = character;
        }

        public override void Execute()
        {
            SelectionModel selectionModel = Get<SelectionModel>();
            selectionModel.CurrentChosenCharacter.Value = _character;
        }
    }
}