using System;
using Framework;
using Framework.Audio;
using Framework.Client;
using Framework.RPC;
using Framework.Service;
using Framework.UI;
using Game.Analytics;
using Game.Audio;
using Game.Command;
using Game.Service.PeriodicJob;
using Game.Translations;
using Game.View;
using Game.View.Help;
using GameShared;
using SAS.Model;
using SAS.Model.Time;
using SAS.Model.User;

namespace Game
{
	[Bind]
	public class ClientStartup
	{
		[Inject]
		public IPrefabProvider PrefabProvider;
		[Inject]
		public IClientRpcController RpcController;
		[Inject]
		public IResolver Resolver;
		[Inject]
		public IController Controller;

		private LoadingView _loadingView;
		private bool _isListening;
		private IAudioManager _audioManager;

		public void Start()
		{
			MainUiView mainUiView = PrefabProvider.Instantiate<MainUiView>(true);
			_loadingView = mainUiView.LoadingView;
			_loadingView.Show("Trying to restore session");
			RpcController.Execute(new GetDynamicCommand()).Then(OnGetDynamic, OnNoStoredToken);
			_audioManager = Resolver.Resolve<IAudioManager>();
			_audioManager.SetVolume(0.5f);
		}

		private void OnNoStoredToken(Exception reason)
		{
			_loadingView.Show("Please start login");
			Log.Info(reason.Message);
			_loadingView.SetAuthorizeAction(StartAuthorize, RpcController.ClientAuth.AuthUrl, true);
		}

		private void OnReceivedToken()
		{
			_audioManager.PlaySound(SoundType.SoundLogin);
			_loadingView.Show("Loading game");
			RpcController.Execute(new GetDynamicCommand()).Then(OnGetDynamic, OnFailGetDynamic);
		}

		private void StartAuthorize()
		{
			if (!_isListening)
			{
				_loadingView.Show("Authorization in progress. Check browser");

				_isListening = true;
				RpcController.ClientAuth.Authorize().Then(OnReceivedToken, OnFailAuthorize);
			}
		}

		private void OnGetDynamic()
		{
			Resolver.Resolve<ITaskScheduler>().Schedule(Resolver.Resolve<RequestChestDynamicJob>(),
					TimeDuration.CreateBySeconds(10));

			InitializeTranslations();
			InitializeAnalytics();
			SetVolume();

			if (Resolver.Resolve<UserCharacters>().Characters.Count == 0)
			{
				ShowCharactersNeededWindow();
			}

			_loadingView.Hide();
			Log.Info("success load dynamic");
		}

		private void InitializeAnalytics()
		{
			AnalyticsProvider analyticsProvider = new AnalyticsProvider(
					Resolver.Resolve<IUpdateManager>(), Resolver.Resolve<IEngine>(),
					"https://api.amplitude.com/2/httpapi",
					"30c1a8d1d6bdc0c9c08363e1243759bc", Resolver.Resolve<UserGuid>().Guid.Value);
		}

		private void SetVolume()
		{
			UserSettings userSettings = Resolver.Resolve<UserSettings>();
			int soundsVolume = userSettings.SoundsVolume;
			Controller.Execute(new SetLocalSettingsVolume(soundsVolume, userSettings.Language));
			_audioManager.SetVolume(soundsVolume / 100f);
		}

		private void ShowCharactersNeededWindow()
		{
			Resolver.Resolve<IWindowManager>().Open<NeedBuyCharacterWindow>();
		}

		private void InitializeTranslations()
		{
			UserSettings userSettings = Resolver.Resolve<UserSettings>();
			Resolver.Resolve<TranslationSwitcher>().SwitchLanguage(userSettings.Language);
		}

		void OnFailAuthorize(Exception exception)
		{
			_loadingView.Show($"Authorization fail. {exception.Message}");
			_loadingView.SetAuthorizeAction(StartAuthorize, RpcController.ClientAuth.AuthUrl, false);
			_isListening = false;
			Log.Fatal(exception);
			_audioManager.PlaySound(SoundType.SoundLoginFailure);
		}

		void OnFailGetDynamic(Exception exception)
		{
			_loadingView.Show($"Loading game fail. {exception.Message}");
			Log.Fatal(exception);
		}
	}
}