﻿using Framework.Reactive;

namespace Game.Service.Visibility
{
	public interface IVisibilityListener
	{
		public Event<bool> OnGameVisible { get; }
	}
}