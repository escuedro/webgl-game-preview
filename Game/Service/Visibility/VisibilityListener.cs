﻿using System.Runtime.InteropServices;
using Framework.Reactive;
using UnityEngine;

namespace Game.Service.Visibility
{
	public class VisibilityListener : MonoBehaviour, IVisibilityListener
	{
		[DllImport("__Internal")]
		private static extern void registerVisibilityChangeEvent();

		public Event<bool> OnGameVisible { get; } = new Event<bool>();

		private void Start()
		{
#if !UNITY_EDITOR
			registerVisibilityChangeEvent();
#endif
		}
 
		public void OnVisibilityChange(string visibilityState)
		{
			bool visible = visibilityState == "visible";
			OnGameVisible.Dispatch(visible);
		}
	}
}