using Framework.Client.View;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Service
{
    public class HorizontalLayoutView : LayoutView
    {
        [SerializeField]
        private HorizontalLayoutGroup _horizontalLayoutGroup;

        public override void Recalculate()
        {
            _horizontalLayoutGroup.CalculateLayoutInputHorizontal();
        }
    }
}
