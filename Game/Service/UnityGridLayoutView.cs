﻿using Framework.Client.View;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Service
{
	public class UnityGridLayoutView : LayoutView
	{
		[SerializeField]
		private GridLayoutGroup _gridLayout;
		
		public override void Recalculate()
		{
			_gridLayout.CalculateLayoutInputHorizontal();
			_gridLayout.CalculateLayoutInputVertical();
		}
	}
}