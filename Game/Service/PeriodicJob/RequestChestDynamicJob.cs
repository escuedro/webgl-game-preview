using System;
using Framework;
using Framework.RPC;
using GameShared;
using SAS.Model;
using SAS.Model.Time;

namespace Game.Service.PeriodicJob
{
	[Bind]
	public class RequestChestDynamicJob : IJob
	{
		[Inject]
		public UserChest UserChest;
		[Inject]
		public IClientRpcController RpcController;

		public void Action()
		{
			bool dirtry = false;
			foreach (EssenceReward essenceReward in UserChest.EssenceRewards)
			{
				TimePoint rewardTime = essenceReward.ChestRewardTime.ToTimePoint() + TimeDuration.Hour;
				if (!rewardTime.IsFuture)
				{
					dirtry = true;
					break;
				}
			}

			if (dirtry)
			{
				RpcController.Execute(new GetUserChestCommand()).Then(OnSuccess, OnFail);
			}
		}

		private void OnSuccess()
		{
			Log.Trace("Success update user chest");
		}

		private void OnFail(Exception reason)
		{
			Log.Exception(reason);
		}
	}
}