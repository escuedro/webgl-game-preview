using Framework.Client.View;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Service
{
	public class VerticalLayoutView : LayoutView
	{
		[SerializeField]
		private VerticalLayoutGroup _verticalLayout;
		[SerializeField]
		private ContentSizeFitter _contentSizeFitter;

		public override void Recalculate()
		{
			_verticalLayout.CalculateLayoutInputVertical();
			_contentSizeFitter.SetLayoutVertical();
		}
	}
}
