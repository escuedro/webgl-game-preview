using System;
using Framework;
using Framework.RPC;
using Framework.Serialization;
using GameShared;
using UnityEngine;

namespace Game
{
	public class RpcTester : MonoBehaviour
	{
		[Inject]
		public IClientRpcController RpcController;

		[Inject]
		public BaseSerializer BaseSerializer;

		[SerializeField]
		private int _testValue;

		[SerializeField]
		private bool _getDynamic;
		[SerializeField]
		private bool _createCharacter;
		[SerializeField]
		private bool _generateMonsters;

		private void Start()
		{
			for (int i = 0; i < _testValue; i++)
			{
				RpcController.Execute(new HelloCommand(i)).Then(OnSuccess, OnFail);
			}

			if (_getDynamic)
			{
				RpcController.Execute(new GetDynamicCommand()).Then(OnSuccess, OnFail);
			}
			if (_createCharacter)
			{
				RpcController.Execute(new CreateCharacterCommand()).Then(OnSuccess, OnFail);
			}

			if (_generateMonsters)
			{
				RpcController.Execute(new GenerateMonstersCommand(3)).Then(OnSuccess, OnFail);
			}
		}

		private void OnFail(Exception reason)
		{
			Log.Info($"RpcTester fail execute: {reason}");
		}

		private void OnSuccess()
		{
			Log.Info("RpcTester success");
		}
	}
}